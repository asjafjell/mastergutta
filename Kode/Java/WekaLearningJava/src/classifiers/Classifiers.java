package classifiers;

import classifiers.settings.AClassifierSettings;
import classifiers.settings.AnnSettings;
import classifiers.settings.IBkSettings;
import classifiers.settings.SvmSettings;

/**
 * Created by Erlend on 02.04.2014.
 */
public class Classifiers {

    public static AClassifier create(ClassifierType type){
        switch(type){
            case Ibk:
                return new IBk();
            case Svm:
                return new Svm();
            case Ann:
                return new Ann();
        }
        return null;
    }

    public static AClassifierSettings createSettings(ClassifierType type) {
        switch(type){
            case Ibk:
                return IBkSettings.getDefaultSettings();
            case Svm:
                return SvmSettings.defaultValues();
            case Ann:
                return AnnSettings.getDefaultSettings();
        }
        return null;
    }
}
