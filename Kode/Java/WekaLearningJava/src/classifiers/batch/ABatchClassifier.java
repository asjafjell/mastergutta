package classifiers.batch;

import utilities.Data;
import utilities.StopWatch;
import weka.classifiers.Evaluation;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by Erlend on 12.03.14.
 */
public abstract class ABatchClassifier {

    public static String GlobalCodeSuffix;

    ArrayList<String> fileNameBases = new ArrayList<String>();

    public static void clean(BatchFolder folder, String... codes) {
        File dir = new File(Data.getWekaFile(folder.name()));
        File[] listOfFiles = dir.listFiles();
        for(String code : codes){
            for (int i = 0; i < listOfFiles.length; i++){
                if(listOfFiles[i].getPath().endsWith(getEvalFile("", code)))
                    listOfFiles[i].delete();
            }
        }
    }

    public enum BatchFolder{
        finalDataSet,
        parameterExperimentation
    }

    public ABatchClassifier(BatchFolder folder){
        this(Data.getWekaFile(folder.name()));
    }

    public ABatchClassifier(String folderPath) {
        fileNameBases = getFiles(folderPath);
    }

    public ABatchClassifier(ArrayList<String> files) {
        fileNameBases = files;
    }

    public static ArrayList<String> getFiles(BatchFolder folder){
        return getFiles(Data.getWekaFile(folder.name()), new String[0]);
    }

    public static ArrayList<String> getFiles(BatchFolder folder, String[] onlyTheseIDs){
        return getFiles(Data.getWekaFile(folder.name()), onlyTheseIDs);
    }

    public static ArrayList<String> getFiles(String folderPath){
        return getFiles(folderPath, new String[0]);
    }

    public static ArrayList<String> getFiles(String folderPath, String[] onlyTheseIDs) {
        File folder = new File(folderPath);
        File[] listOfFiles = folder.listFiles();

        if(listOfFiles==null){
            System.out.println("Something fishy with: " + folderPath);
        }

        List<String> onlyThese = Arrays.asList(onlyTheseIDs);
        ArrayList<String> files = new ArrayList<String>();
        for(int i =0; i < 5000; i++){
            if(hasFile(listOfFiles, i) && (onlyTheseIDs.length == 0 || onlyThese.contains(pad(i))))
                files.add(folderPath + "\\" + pad(i));
        }

        return files;
    }

    private static boolean hasFile(File[] listOfFiles, int num) {
        boolean foundTrain = false;
        boolean foundTest = false;

        for(int i = 0; i < listOfFiles.length; i++){
            if(listOfFiles[i].getName().endsWith(pad(num) + "train.csv"))
                foundTrain = true;
            if(listOfFiles[i].getName().endsWith(pad(num) + "test.csv"))
                foundTest = true;
        }

        return foundTrain && foundTest;
    }

    private static String pad(int i){
        if(i<10) return "00" + i;
        if(i<100) return "0"+i;
        return ""+i;
    }

    public void run() throws Exception {

        StopWatch process = new StopWatch("Starting batch run '" + getCode() + "' on " + fileNameBases.size() + " files.");

        for(String file : fileNameBases){

            String trainFile = file + "train.csv";
            String testFile = file + "test.csv";

            /*if(new File(getEvalFile(file)).exists()){
                System.out.println("  > Skipped file " + trainFile);
                continue;
            }*/

            StopWatch s = new StopWatch("  > Starting file " + trainFile);

            Instances train = new ConverterUtils.DataSource(trainFile).getDataSet();
            Instances test = new ConverterUtils.DataSource(testFile).getDataSet();
            setClass(train, test, "Delay");

            Evaluation evaluation = runCase(train, test);

            String res = "Classifier: " + getClassifierName() + "\n";
            res += "Settings: " + getSettingsString() + "\n";
            res += "\n";
            res += "Date: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\n";
            res += "Run time: " + s.elapsed() + "ms\n";
            res += "\n";
            res += "Training rows: " + train.numInstances() + "\n";
            res += "Testing rows: " + test.numInstances() + "\n";
            res += "\n";
            res += "Error rate: " + evaluation.errorRate() + "\n";
            res += "Mean absolute error: " + evaluation.meanAbsoluteError() + "\n";
            res += "Correlation coefficient: " + evaluation.correlationCoefficient() + "\n";
            res += "Root mean squared error: " + evaluation.rootMeanSquaredError() + "\n";
            res += "Relative absolute error: " + evaluation.relativeAbsoluteError() + "\n";
            res += "Root relative squared error: " + evaluation.rootRelativeSquaredError() + "\n";

            save(file, res);

            s.log("  > Finished in X ms.");
        }

        process.log("Finished batch in X ms.");
    }

    /**
     * The settings string used for this classifier. Is saved to the results file.
     * @return
     */
    protected abstract String getSettingsString();

    /**
     * The name of this classifier. Is saved to the results file.
     * @return
     */
    protected abstract String getClassifierName();

    private void save(String number, String contents){

        String path = getEvalFile(number);

        new File(path).delete();

        Writer writer = null;
        try {
            writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(path), "utf-8"));
            writer.write(contents);
        } catch (IOException ex) {
        } finally {
            try {writer.close();} catch (Exception ex) {}
        }
    }

    private String getEvalFile(String number) {
        return getEvalFile(number, getCode());
    }

    private static String getEvalFile(String number, String code) {
        return number + "eval." + code + ".txt";
    }

    /**
     * The code for this batch run. This code is used when saving the results file: "{set number}eval.{code}.txt".
     * For example "001eval.ann.txt".
     * @return
     */
    protected abstract String getCode();

    /**
     * Train a classifier with the training set, evaluate it with the test set, and return the Evaluation object.
     * A results file will be saved with the results in this object.
     * @param train
     * @param test
     * @return
     * @throws Exception
     */
    protected abstract Evaluation runCase(Instances train, Instances test) throws Exception;

    private void setClass(Instances train, Instances test, String attribute) {
        train.setClass(train.attribute(attribute));
        test.setClass(test.attribute(attribute));
    }

}
