package classifiers.batch;

import classifiers.IBk;
import classifiers.settings.IBkSettings;
import weka.classifiers.Evaluation;
import weka.core.Instances;

import java.util.ArrayList;

/**
 * Created by Aleksander on 13.03.14.
 */
public class IBkBatchClassifier extends ABatchClassifier {

    private IBkSettings settings;

    public IBkBatchClassifier(BatchFolder folder){
        super(folder);
        initialize();
    }

    private void initialize() {
        IBk.debug = false;
        settings = IBkSettings.getBestSettings(0);
        //settings = IBkSettings.getDefaultSettings();
    }

    public IBkBatchClassifier(ArrayList<String> files) {
        super(files);
        initialize();
    }

    @Override
    protected String getSettingsString() {
        return settings.toOptionsString();
    }

    @Override
    protected String getClassifierName() {
        return "Instance Based Learning";
    }

    @Override
    protected String getCode() {
        return "ibk" + GlobalCodeSuffix;
    }

    @Override
    protected Evaluation runCase(Instances train, Instances test) throws Exception {
        settings = IBkSettings.getBestSettings(train.numInstances());
        //settings = IBkSettings.getDefaultSettings();
        IBk iBk = new IBk(settings);
        iBk.train(train);
        return iBk.evaluate(test);
    }
}

