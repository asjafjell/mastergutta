package classifiers.batch;

import classifiers.Ann;
import classifiers.settings.AnnSettings;
import weka.classifiers.Evaluation;
import weka.core.Instances;

import java.util.ArrayList;

/**
 * Created by Erlend on 13.03.14.
 */
public class AnnBatchClassifier extends ABatchClassifier {

    private String _settings;

    public AnnBatchClassifier(BatchFolder folder) {
        super(folder);
    }

    public AnnBatchClassifier(ArrayList<String> files) {
        super(files);
    }

    @Override
    protected String getSettingsString() {
        return _settings;
    }

    @Override
    protected String getClassifierName() {
        return "Artificial Neural Network";
    }

    @Override
    protected String getCode() {
        return "ann" + GlobalCodeSuffix;
    }

    @Override
    protected Evaluation runCase(Instances train, Instances test) throws Exception {
        Ann ann = new Ann(AnnSettings.getBestSettings());
        //Ann ann = new Ann(AnnSettings.getDefaultSettings());
        ann.train(train);
        return ann.evaluate(test);
    }
}
