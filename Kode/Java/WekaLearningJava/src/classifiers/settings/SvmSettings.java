package classifiers.settings;

import java.util.HashMap;

/**
 * Created by Erlend on 05.03.14.
 */
public class SvmSettings extends AClassifierSettings {
    public double C = 1.0;

    public Kernels Kernel = Kernels.PolyKernel;

    public int Kernel_CacheSize = 0;
    public double Kernel_Gamma = 0.01;
    public double Kernel_Exponent = 2.0;

    public double Kernel_Puk_Sigma = 1.0;
    public double Kernel_Puk_Omega = 1.0;

    public enum Kernels{
        PolyKernel,
        NormalizedPolyKernel,
        RBFKernel,
        PukKernel
    }

    private HashMap<Kernels, String> kernelIDs = new HashMap<Kernels, String>();

    public SvmSettings(){
        kernelIDs.put(Kernels.PolyKernel, "weka.classifiers.functions.supportVector.PolyKernel");
        kernelIDs.put(Kernels.NormalizedPolyKernel, "weka.classifiers.functions.supportVector.NormalizedPolyKernel");
        kernelIDs.put(Kernels.RBFKernel, "weka.classifiers.functions.supportVector.RBFKernel");
        kernelIDs.put(Kernels.PukKernel, "weka.classifiers.functions.supportVector.Puk");
    }

    @Override
    public String toOptionsString() {
        return String.format("-C %f -N 0 -I %s -K %s", C, getRegOptimizerOptions(), getKernelOptions()).replace(",",".");
    }

    public static SvmSettings defaultValues() {
        return new SvmSettings();
    }

    public static SvmSettings getBestValues() {
        SvmSettings s = new SvmSettings();
        s.C = 5.717;
        s.Kernel_Gamma = 0.0256;
        s.Kernel = Kernels.RBFKernel;
        return s;
    }

    public String getKernelOptions() {
        switch(Kernel){
            default:
            case PolyKernel:
            case NormalizedPolyKernel:
                return String.format("\"%s -C %d -E %f\"", kernelIDs.get(Kernel), Kernel_CacheSize, Kernel_Exponent);
            case RBFKernel:
                return String.format("\"%s -C %d -G %f\"", kernelIDs.get(Kernel), Kernel_CacheSize, Kernel_Gamma);
            case PukKernel:
                return String.format("\"%s -C %d -O %f -S %f\"", kernelIDs.get(Kernel), Kernel_CacheSize,Kernel_Puk_Omega, Kernel_Puk_Sigma);
        }
    }

    public String getRegOptimizerOptions() {
        return "\"weka.classifiers.functions.supportVector.RegSMOImproved -L 0.001 -W 1 -P 1.0E-12 -T 0.001 -V\"";
    }
}
