package classifiers.settings;

/**
 * Created by Aleksander on 19.03.2014.
 */
public class AnnSettings extends AClassifierSettings {


    public Double LearningRate;
    public Double Momentum;
    public int Epochs;
    public String Neurons;

    public AnnSettings(Double learningRate, Double momentum, int epochs, String neurons){
        LearningRate = learningRate;
        Momentum = momentum;
        Epochs = epochs;
        Neurons = neurons;
    }

    @Override
    public String toOptionsString() {
        return String.format("-L %s -M %s -N %s -V 0 -S 0 -E 20 -H %s", LearningRate, Momentum, Epochs, Neurons);
    }

    public static AnnSettings getDefaultSettings(){
        return new AnnSettings(0.3,0.2,500,"a");
    }

    public static AnnSettings getBestSettings(){
        return new AnnSettings(0.1,0.2,250,"49,49,49");
    }
}
