package classifiers;

/**
 * Created by Erlend on 02.04.2014.
 */
public enum ClassifierType {
    Ibk,
    Svm,
    Ann
}
