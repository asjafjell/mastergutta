package classifiers;

import classifiers.settings.AnnSettings;
import weka.classifiers.Classifier;
import weka.classifiers.functions.MultilayerPerceptron;

/**
 * Created by Aleksander on 26.02.14.
 */
public class Ann extends AClassifier {

    //http://weka.sourceforge.net/doc.dev/weka/classifiers/functions/MultilayerPerceptron.html

    public Ann(){
        this(AnnSettings.getDefaultSettings());
    }

    public Ann(AnnSettings settings){
        super(settings);
    }

    public String getName() {
        return "ann";
    }

    @Override
    public Classifier createClassifier() {
       return new MultilayerPerceptron();
    }
}
