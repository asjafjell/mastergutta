package classifiers;

import classifiers.settings.SvmSettings;
import utilities.Log;
import weka.classifiers.Classifier;
import weka.classifiers.evaluation.Prediction;
import weka.classifiers.functions.SMOreg;
import weka.core.Instance;

/**
 * Created by Erlend on 27.02.14.
 */
public class Svm extends AClassifier {
    public Svm(SvmSettings settings)
    {
        super(settings);
    }

    public Svm(){
        super(SvmSettings.defaultValues());
    }

    public Classifier createClassifier() {
        return new SMOreg();
    }

    public String getName() {
        return "svm";
    }

    public void  printPredictions() throws Exception {
        Prediction[] predictions = getPredictions();
        for(int i = 0; i < predictions.length; i++){
            Prediction pred = predictions[i];
            Instance instance = getTestInstances().instance(i);
            System.out.printf("Time: %s, actual value: %f,8, predicted value: %f,8%n", instance.value(2) + "-" + instance.value(0) + "-" + instance.value(1), pred.actual(), pred.predicted());
        }
    }

    public void printResults(){
        Log.slog(evalToString());
    }
}
