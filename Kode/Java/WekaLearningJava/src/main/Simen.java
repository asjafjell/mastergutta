package main;

import classifiers.Ann;
import dataset.DataSet;
import utilities.Data;
import weka.classifiers.Evaluation;
import weka.classifiers.evaluation.Prediction;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import javax.xml.transform.sax.SAXSource;
import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simen on 06.03.14.
 */
public class Simen {

    public static void main(String[] args) {

        String trainFile = Data.getDataDirectory() + "16010907-16010394_17.02.2014-20.02.2014.csv";
        String testFile = Data.getDataDirectory() + "16010907-16010394_21.02.2014-21.02.2014.csv";
        ArrayList<String> predictionResults = new ArrayList<String>();
        try{
            BufferedWriter bw = new BufferedWriter(new FileWriter(new File(testFile.replace(".csv", "_ann.csv"))));
            BufferedReader br = new BufferedReader(new FileReader(testFile));
//
//            DataSet ds = new DataSet(trainFile,testFile);
//
//            List<String> rmAttrs = new ArrayList<String>();
//            rmAttrs.add("TripID");
//            rmAttrs.add("Departure");
//            ds.removeAttributesForever(rmAttrs);
//
//            List<String> stops = new ArrayList<String>();
//            stops.add("Stop_1");
//            stops.add("Stop_2");
//            stops.add("Stop_3");
//            stops.add("Stop_4");
//            stops.add("Stop_5");
//            stops.add("Stop_6");
//            stops.add("Stop_7");
//            stops.add("Stop_8");
//            stops.add("Stop_9");
//            stops.add("Stop_10");
//            stops.add("Stop_11");
//            stops.add("Stop_12");
//            stops.add("Stop_13");
//            stops.add("Stop_14");
//            stops.add("Stop_15");
//            stops.add("Stop_16");

//            while(ds.getAttributes().size() > 2){
            int attributesToRemove = 14;
            while (attributesToRemove >= 0){
                Instances trainingSet = new ConverterUtils.DataSource(trainFile).getDataSet();
                Instances testSet = new ConverterUtils.DataSource(testFile).getDataSet();
                trainingSet.setClassIndex(trainingSet.numAttributes() - 1);
                testSet.setClassIndex(testSet.numAttributes() - 1);
                trainingSet.deleteAttributeAt(0);
                trainingSet.deleteAttributeAt(0);
                testSet.deleteAttributeAt(0);
                testSet.deleteAttributeAt(0);
                int removed = 0;
                while(removed < attributesToRemove){
                    trainingSet.deleteAttributeAt(trainingSet.numAttributes() - 2);
                    testSet.deleteAttributeAt(testSet.numAttributes() - 2);
                    removed++;
                }
                Ann ann = new Ann();
//                for(int i = 0; i < trainingSet.numAttributes(); i++){
//                    System.out.println(trainingSet.attribute(i).toString());
//                    System.out.println(testSet.attribute(i).toString());
//                }
//                ds.setClassAttribute("Stop_16");
//                trainingSet.setClass(trainingSet.attribute("Stop_16"));
//                System.out.println("trainingSet classindex " + trainingSet.classIndex());
//                System.out.println("testSet classindex " + testSet.classIndex());
                //ann.train(ds.getTrainingData());
                ann.train(trainingSet);
                Evaluation e = ann.evaluate(testSet);
                //Evaluation e = ann.evaluate(ds.getTestData());

                Prediction[] preds = ann.getPredictions();
                for(int i = 0; i < preds.length; i++){
                    Prediction p = preds[i];
                    double actual = p.actual();
                    double predicted = p.predicted();
                    System.out.println(actual + " " + predicted + " " + Math.abs(actual - predicted));
                    int predRound = Math.round((float)predicted);
                    if(predictionResults.size() == i){
                        predictionResults.add(String.valueOf(predRound));
                    }
                    else{
                        predictionResults.set(i,predictionResults.get(i) + "," + String.valueOf(predRound));
                    }
                }


                System.out.print(e.toSummaryString());
                System.out.println();

//                List<String> rmAttr = new ArrayList<String>();
//                String currAttr = stops.get(stops.size() - 2);
//                rmAttr.add(currAttr);
//                ds.removeAttributesForever(rmAttr);
//                stops.remove(stops.size()-2);

                //testSet.deleteAttributeAt(testSet.numAttributes() - 2);
                //trainingSet.deleteAttributeAt(trainingSet.numAttributes() - 2);
                attributesToRemove--;
            }
            for(int i = 0; i < predictionResults.size(); i++){
                bw.write(predictionResults.get(i));
                bw.newLine();
            }
            bw.close();
        }
        catch(Exception ex){
            ex.printStackTrace();
        }


//        Ann ann = new Ann();
//
//
//        DataSet dataSet = new DataSet(file, 90, false,false);
//        dataSet.setClassAttribute("Delay");
//
//        List<String> attrsToRemove = new ArrayList<String>(){
//            {
//                add("PassageID");
//                add("WeatherID");
//                add("Trip");
//                add("Vehicle");
//                add("Shift");
//            }
//        };
//
//        dataSet.applyMyAttributeFilter(attrsToRemove);
//
//
//        FindBestParameters bestParameters = new FindBestParameters(ann, dataSet, "Delay", "Ann");
//
//        try {
//            ann.train(dataSet.getTrainingData());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        try {
//            ann.evaluate(dataSet.getTestData());
//        } catch (Exception e) {
//            e.printStackTrace();
//        }


    }
}
