package dataset;

import classifiers.AClassifier;
import utilities.*;
import weka.classifiers.Evaluation;
import weka.core.Attribute;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.*;

/**
 * Created by aleksandersjafjell on 3/1/14.
 */
public class FindBestParameters  {

    AClassifier _classifier;
    DataSet _dataSet;
    String _classAttribute;
    List<String> _ignoreAttributes = new ArrayList<String>();
    String _classifierName;

    public FindBestParameters(AClassifier classifier, DataSet dataSet, String classAttribute, String classifierName){
        _classifier = classifier;
        _dataSet = dataSet;
        _classAttribute = classAttribute;
        _dataSet.setClassAttribute(classAttribute);
        _ignoreAttributes.add(classAttribute);
        _classifierName = classifierName;

    }

    public void generatePowerSet(int splitCount){
        String powerSetFilePath = _dataSet.getSourceFolder().equals("") ? "" : _dataSet.getSourceFolder() + File.separator;
        powerSetFilePath += _dataSet.getId() + "powerset.txt";
        SetUtilities.writePowerSet(powerSetFilePath, generatePowerSet(), _dataSet.getId());
        if(splitCount > 1)
            FileUtilities.splitFile(powerSetFilePath, splitCount, _dataSet.getSourceFolder(),"powerset");
    }

    public void run() throws Exception {
        String powerSetFilePath = "";// _dataSet.getSourceFolder().equals("") ? "" : _dataSet.getSourceFolder() + File.separator;
        //powerSetFilePath += Data.getWekaFile("attributes","minipowerset") + "\\" + _dataSet.getId() + "powerset.txt";
        powerSetFilePath += Data.getWekaFile("attributes","minipowerset") + "\\powerset.txt";

        File file = new File(powerSetFilePath);
        String s  = file.getAbsolutePath();

        runPowerSetFile(s, -1);
    }

    /**
     * Runs Best-parameter tests for the file as input.
     * @param powerSetFile The file with powersets to runPowerSet
     * @param fileNumber If the file is unsplitted, the filenumber will denote the part of the file that is sent into the method. The
     *                   output file will be named accordingly. If set to <= 0, it is indicated that the file is unsplitted. This
     *                   does only affect the naming of the output file.
     */
    private void runPowerSetFile(String powerSetFile, int fileNumber) throws Exception {
        List<List<String>> powerSet = SetUtilities.readPowerSet(powerSetFile);

        String fileName = _dataSet.getId() + "result" + _classifierName + ".txt";
        String resultFile = fileName; //_dataSet.getSourceFolder() == "" ? fileName : _dataSet.getSourceFolder() + File.pathSeparator + fileName;

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(resultFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        StopWatch2 stopWatch2 = new StopWatch2();
        int count = 0;
        for(List<String> set : powerSet){
            count++;
            if(count % 1 == 0) {
                writer.flush();
                if (count == 0) {
                    continue;
                } else {
                    long timeElapsed = stopWatch2.duration();
                    long avgTime = timeElapsed / count;
                    long timeRemaining = avgTime * (powerSet.size() - count);
                    System.out.print("\rFile: " + _dataSet.getId() + " PowerSet(" + count + "/" + powerSet.size() + "). " +
                            "Remaining: " +  TimeUtilities.getDurationBreakdown(timeRemaining) +
                            "(" +  String.format("%1$,.4f", (double) count / powerSet.size()*100) + "%)");
                }
            }
            /**
             * For each of the power sets, calculate the evaluation object
             * and write results to file.
             */
            Evaluation eval = findError(set);
            String s;
            try {
                s = set.toString() + ";" + eval.meanAbsoluteError()+ ";" + eval.correlationCoefficient()  + ";" + eval.relativeAbsoluteError() + ";" + eval.rootMeanSquaredError() + ";" + eval.rootRelativeSquaredError();
                writer.println(s);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        writer.close();
        System.out.println();
    }

    private Set<Set<String>> generatePowerSet() {
        Set<String> set = new HashSet<String>();

        for(Attribute attribute : _dataSet.getAttributes()){
            if(_ignoreAttributes.contains(attribute.name()))
                continue;
            set.add(attribute.name());
        }

        return SetUtilities.generatePowerSet(set);
    }

    private Evaluation findError(List<String> attributesToRemove) throws Exception {
        _dataSet.reset();
        _dataSet.applyMyAttributeFilter(attributesToRemove);
        _classifier.train(_dataSet.getTrainingData());
        return _classifier.evaluate(_dataSet.getTestData());
    }

    private String listToString(List<Attribute> attributes){
        return Arrays.toString(attributes.toArray());

    }

    /**
     *  Sets the parameters that will not be removed when finding the best
     * parameters. Note that the class attribute will always be ignored.
     * @param ignoreAttributes The attributes to be ignored.
     */
    public void setIgnoreAttributes(List<String> ignoreAttributes){
        _ignoreAttributes = ignoreAttributes;
    }

}
