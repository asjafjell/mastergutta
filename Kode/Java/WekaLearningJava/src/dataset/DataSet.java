package dataset;

import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;

/**
 * Created by aleksandersjafjell on 3/1/14.
 */
public class DataSet {

    private Instances _originalTrainingData;
    private Instances _originalTestData;
    private Instances _trainingData;
    private Instances _testData;
    private String _classAttribute;
    String _sourceFolder;
    String _id = "No ID set";


    /**
     * Initializes the data set with a predefined training and evaluate set.
     * @param testSetPath
     * @param trainingSetPath
     */
    public DataSet(String trainingSetPath, String testSetPath) throws IOException {
        try {
            _originalTrainingData = new ConverterUtils.DataSource(trainingSetPath).getDataSet();
            _originalTestData = new ConverterUtils.DataSource(testSetPath).getDataSet();

        } catch (Exception e) {
            e.printStackTrace();
        }

        _trainingData = new Instances(_originalTrainingData);
        _testData = new Instances(_originalTestData);
        setSourceFolder(trainingSetPath);
    }

    public void removeAttributesForever(List<String> removeForeverAttributes){
        MyAttributeFilter filter = new MyAttributeFilter(removeForeverAttributes);
        _trainingData = filter.useFilter(_trainingData);
        _testData = filter.useFilter(_testData);
        _originalTrainingData = filter.useFilter(_originalTrainingData);
        _originalTestData = filter.useFilter(_originalTestData);
    }

    /**
     * Resets the training and evaluate data to original set.
     */
    public void reset(){
        _trainingData = new Instances(_originalTrainingData);
        _testData = new Instances(_originalTestData);
        setClassAttribute(_classAttribute);
    }

    public void applyMyAttributeFilter(List<String> attributesToRemove){
        MyAttributeFilter filter = new MyAttributeFilter(attributesToRemove);
        _trainingData = filter.useFilter(_trainingData);
        _testData = filter.useFilter(_testData);
    }

    public void applyMyAttributeFilter(Set<String> attributesToRemove){
        List<String> attributes = new ArrayList<String>();

        for(String attribute : attributesToRemove)
            attributes.add(attribute);

        applyMyAttributeFilter(attributes);
    }

    public Instances getTrainingData() {
        return _trainingData;
    }

    public Instances getTestData() {
        return _testData;
    }

    public void setClassAttribute(String attribute){
        if(_trainingData != null || _testData != null){
            _trainingData.setClass(_trainingData.attribute(attribute));
            _testData.setClass(_testData.attribute(attribute));
        }
        _classAttribute = attribute;
    }

    public String getClassAttribute(){
        if(_trainingData.classAttribute().name().equals(_classAttribute) && _testData.classAttribute().name().equals(_classAttribute)){
            return _classAttribute;
        }else{
            throw new InternalError("ERROR: Getting the class attribute in dataset, but it is not set on the data set itself.");
        }
    }

    public int numOfOriginalAttributes(){
        return _originalTrainingData.numAttributes();
    }

    public int numOfAttributes(){
        return _trainingData.numAttributes();
    }

    /**
     * Gets all attributes that the evaluate and training set currently has.
     * @return The list of attributes.
     */
    public List<Attribute> getAttributes(){
        Enumeration enumeration = _trainingData.enumerateAttributes();
        List<Attribute> allAttributes = new ArrayList<Attribute>();
        while(enumeration.hasMoreElements()){
            allAttributes.add((Attribute) enumeration.nextElement());
        }
        return allAttributes;
    }

    private void setSourceFolder(String fileInSourceFolder){
        String sourceFolder = new File(fileInSourceFolder).getParent();
        _sourceFolder = sourceFolder == null ? "" : sourceFolder;
    }

    public String getSourceFolder(){
        return _sourceFolder;
    }

    public String getId(){
        return _id;
    }

    public void setId(String id){
        _id = id;
    }
}
