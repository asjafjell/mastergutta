package dataset;

import classifiers.AClassifier;
import classifiers.ClassifierType;
import classifiers.Classifiers;
import classifiers.settings.AClassifierSettings;
import classifiers.settings.FromStringSettings;
import main.Erlend;
import utilities.*;
import weka.core.Attribute;
import weka.core.Instances;
import weka.core.converters.ConverterUtils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Erlend on 02.04.2014.
 */
public class FindBestAttributes {

    public static void createJobFiles(String[] setIDs, String[] neverRemoveParameters, ClassifierType classifierType, AClassifierSettings settings) throws Exception {
        String path = getJobLocation();
        List<String> ignoreParameters = Arrays.asList(neverRemoveParameters);

        for(String setID : setIDs) {
            //test
            File dataFile = new File(Data.getDataset(setID, true));
            System.out.println(" > DatasetPath:l " + Data.getDataset(setID, true) + " File exists: " + dataFile.exists());
            //end test


            Instances set = new ConverterUtils.DataSource(Data.getDataset(setID, true)).getDataSet();
            Enumeration enumeration = set.enumerateAttributes();
            Set<String> attributes = new HashSet<String>();
            while(enumeration.hasMoreElements()) {
                String attribute = ((Attribute) enumeration.nextElement()).name();
                if (!ignoreParameters.contains(attribute))
                    attributes.add(attribute);
            }

            Set<Set<String>> powerSets = SetUtilities.generatePowerSet(attributes);

            int fid = 0;
            for(Set<String> job : powerSets){
                String attributesToRemove = "";
                for (String a : job)
                    attributesToRemove += (attributesToRemove.length() == 0 ? "" : ";") + a;

                File file = new File(path + classifierType.name() + "_" + setID + "_" + fid++ + ".job");
                BufferedWriter output = new BufferedWriter(new FileWriter(file, true));
                output.write(setID);
                output.newLine();
                output.write(Integer.toString(classifierType.ordinal()));
                output.newLine();
                output.write(settings.toOptionsString());
                output.newLine();
                output.write(attributesToRemove);
                output.close();
            }
        }
    }

    public static void removeExistingJobs(){
        File[] jobs = getJobs();
        for(File f : jobs)
            f.delete();
    }

    public static void runJobs(int threadCount) throws InterruptedException {
        ExecutorService service = Executors.newFixedThreadPool(threadCount);
        System.out.println(" > Trying to run jobs...");
        String logFile = "/home/ec2-user/data/weka/log/bestAttribute_Amazon.txt";
        System.out.println(" > Loging to " + logFile);
        final Log log = Log.createPath(logFile);
        //final Log log = Log.createNamed("bestAttribute_" + Data.getUser().name() + ".txt");

        final File folder = new File(Data.getWekaFile("log", "jobs"));
        System.out.println(" > Job folder at " + folder.getAbsolutePath());
        System.out.println(folder.getAbsolutePath());

        for (int i = 0; i < threadCount; i++) {
            final int id = i;
            service.submit(new Runnable() {
                public void run() {
                    while(true){
                        File current = null;
                        try {
                            synchronized (folder){
                                File[] jobs = getJobs();
                                if (jobs == null || jobs.length == 0) return;

                                File job = jobs[0];
                                current = new File(job.getAbsolutePath().replace(".job", "." + id + "_curr"));
                                job.renameTo(current);
                            }

                            String[] cmds = Erlend.readFile(current.getAbsolutePath()).replace("\r\n", "\n").split("\n");
                            ClassifierType classifier = ClassifierType.values()[Integer.parseInt(cmds[1])];
                            String settings = cmds[2];
                            String removeAttributes = cmds.length < 4 ? "" : cmds[3];
                            String dsID = cmds[0];

                            ArrayList<String> attributesToRemove = new ArrayList<String>();
                            for(String a : removeAttributes.split(";")) attributesToRemove.add(a);

                            DataSet ds = getDataset(dsID);
                            ds.applyMyAttributeFilter(attributesToRemove);

                            AClassifier cls = Classifiers.create(classifier);
                            cls.setSettings(new FromStringSettings(settings));

                            StopWatch stopWatch = new StopWatch();
                            cls.train(ds.getTrainingData());
                            String res = settings + "\n" + classifier.name() + "\n" + removeAttributes + "\n" + dsID + "\n";
                            res += stopWatch.get("Training finished in X ms.\n");
                            cls.evaluate(ds.getTestData());
                            res += cls.evalToString();
                            res += stopWatch.get("\nTesting finished in X ms.\n\n");

                            log.log(res);

                            if(!current.delete())
                                System.out.println("Failed to delete file.");
                        } catch (Exception e) {
                            if(current!=null)
                                System.out.println(current.getAbsolutePath());
                            e.printStackTrace();
                            continue;
                        }
                    }
                }
            });
        }

        service.shutdown();
        service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
    }

    private static DataSet getDataset(String dsID) throws IOException {
        String trainFile = Data.getDataset(dsID, true);
        String testFile = Data.getDataset(dsID, false);

        DataSet ds = new DataSet(trainFile, testFile);
        ds.setClassAttribute("Delay");
        return ds;
    }

    private static String getJobLocation(){
        return Data.getWekaFile("jobs", "bestAttributes") + File.separator;
    }

    private static File[] getJobs(){
        return FileTools.endsWith(new File(getJobLocation()).listFiles(), ".job");
    }
}
