package dataset;

import classifiers.Ann;
import classifiers.batch.ABatchClassifier;
import classifiers.settings.AnnSettings;
import utilities.Log;
import utilities.StopWatch;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * Created by Aleksander on 24.03.2014.
 */
public class ParamExperimentation {

    public static final String[] representativeSelection = new String[]{"004","034","064","094","124","154","186","207","236","242","254","259","267","287","310","330","349","370","373","385","388","404"};


    static String[] hiddenNeurons = new String[]{
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "30", "50", "100",
            "a",
            "5,1", "5,2", "5,3", "5,4", "5,5", "5,6", "5,7", "5,8", "5,9", "5,10",
            "10,1", "10,2", "10,3", "10,4", "10,5", "10,6", "10,7", "10,8", "10,9", "10,10",
            "a,1", "a,2", "a,3", "a,4", "a,5", "a,6", "a,7", "a,8", "a,9", "a,10", "a,20", "a,30", "a,50", "a,100",
            "1,1,1", "2,2,2", "3,3,3", "4,4,4", "5,5,5", "6,6,6", "7,7,7", "8,8,8", "9,9,9", "10,10,10",
            "20,10", "30,10", "40,10", "50,10", "60,10", "70,10", "80,10", "90,10", "100,10",
            "10,10,10", "11,11,11", "12,12,12", "13,13,13", "14,14,14", "15,15,15", "16,16,16", "17,17,17", "18,18,18", "19,19,19", "20,20,20", "21,21,21",
            "22,22,22", "23,23,23", "24,24,24", "25,25,25", "26,26,26", "27,27,27", "28,28,28", "29,29,29", "30,30,30", "31,31,31", "32,32,32", "33,33,33", "34,34,34", "35,35,35", "36,36,36", "37,37,37", "38,38,38", "39,39,39", "40,40,40", "41,41,41", "42,42,42", "43,43,43", "44,44,44", "45,45,45", "46,46,46", "47,47,47", "48,48,48", "49,49,49", "50,50,50", "51,51,51", "52,52,52", "53,53,53", "54,54,54", "55,55,55", "56,56,56", "57,57,57", "58,58,58", "59,59,59", "60,60,60", "61,61,61", "62,62,62", "63,63,63", "64,64,64", "65,65,65", "66,66,66", "67,67,67", "68,68,68", "69,69,69", "70,70,70", "71,71,71", "72,72,72", "73,73,73", "74,74,74", "75,75,75", "76,76,76", "77,77,77", "78,78,78", "79,79,79", "80,80,80", "81,81,81", "82,82,82", "83,83,83", "84,84,84", "85,85,85", "86,86,86", "87,87,87", "88,88,88", "89,89,89", "90,90,90", "91,91,91", "92,92,92", "93,93,93", "94,94,94", "95,95,95", "96,96,96", "97,97,97", "98,98,98", "99,99,99", "100,100,100", "10,5,2", "15,7,3", "20,10,4", "25,12,5", "30,15,6", "35,17,7", "40,20,8", "45,22,9", "50,25,10", "55,27,11", "60,30,12", "65,32,13", "70,35,14", "75,37,15", "80,40,16", "85,42,17", "90,45,18", "95,47,19", "100,50,20"
    };

    public static void AnnHiddenLayersTesting() {
        ExecutorService service = Executors.newFixedThreadPool(3);
        final Log log = Log.createTimeStamped();
        String[] ids = representativeSelection;


        List<String> files = ABatchClassifier.getFiles(ABatchClassifier.BatchFolder.finalDataSet, ids);
        System.out.println("Fetching files from " + ABatchClassifier.BatchFolder.finalDataSet);

        for (String fileName : files) {
            for (final String hiddenNeuron : hiddenNeurons) {
                final String file = fileName;
                final String neuron = hiddenNeuron;

                service.submit(new Runnable() {
                    public void run() {
                        try {
                            AnnSettings settings = AnnSettings.getDefaultSettings();
                            settings.Neurons = hiddenNeuron;
                            settings.Epochs = 500;

                            String trainFile = file + "train.csv";
                            String testFile =  file + "test.csv";

                            DataSet dataSet = new DataSet(trainFile, testFile);
                            dataSet.setClassAttribute("Delay");


                            Ann ann = new Ann(settings);

                            StopWatch stopWatch = new StopWatch();
                            ann.train(dataSet.getTrainingData());
                            ann.evaluate(dataSet.getTestData());
                            String res = "Neurons: " + hiddenNeuron + " , LearningRate: " + settings.LearningRate + " , Momentum: " + settings.Momentum + ", Epochs: " + settings.Epochs + ", file '" + file + "'.\n";
                            res += stopWatch.get("Training finished in X ms.\n");
                            res += ann.evalToString();
                            res += stopWatch.get("\nTesting finished in X ms.\n\n");

                            log.log(res);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
            }

        }

        service.shutdown();
        try {
            service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }

    public static void AnnLearningRateAndMomentumTesting() {
        ExecutorService service = Executors.newFixedThreadPool(3);
        final Log log = Log.createPath("./AnnLearningRateAndMomentumTesting2.txt");
        String[] ids = representativeSelection;

        List<String> files = ABatchClassifier.getFiles(ABatchClassifier.BatchFolder.finalDataSet, ids);
        final String[] neurons = new String[]{ /*"75,37,15","57,57,57"};*/"3,3,3","10,5,2","49,49,49" };

        for (String fileName : files) {
            for (final String neuronStr : neurons) {
                for (double lr = 0; lr <= 1; lr += 0.1) {
                    final String file = fileName;
                    final double learningRate = lr;

                    for (double m = 0; m <= 1; m += 0.1) {
                        final double momentum = m;
                        service.submit(new Runnable() {
                            public void run() {
                                try {
                                    AnnSettings settings = AnnSettings.getDefaultSettings();
                                    settings.LearningRate = learningRate;
                                    settings.Momentum = momentum;
                                    settings.Neurons = neuronStr;
                                    settings.Epochs = 500;

                                    String trainFile =  file + "train.csv";
                                    String testFile =  file + "test.csv";

                                    DataSet dataSet = new DataSet(trainFile, testFile);
                                    dataSet.setClassAttribute("Delay");

                                    Ann ann = new Ann(settings);

                                    StopWatch stopWatch = new StopWatch();
                                    ann.train(dataSet.getTrainingData());
                                    ann.evaluate(dataSet.getTestData());
                                    String res = "Neurons: " + neuronStr + " , LearningRate: " + settings.LearningRate + " , Momentum: " + settings.Momentum + ", Epochs: " + settings.Epochs + ", file '" + file + "'.\n";
                                    res += stopWatch.get("Training finished in X ms.\n");
                                    res += ann.evalToString();
                                    res += stopWatch.get("\nTesting finished in X ms.\n\n");

                                    log.log(res);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }

        }

        service.shutdown();
        try {
            service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void AnnEpochsTesting() {
        ExecutorService service = Executors.newFixedThreadPool(7);
        final Log log = Log.createPath("./AnnEpochsTesting3 -- 1-1100.txt");
        String[] ids = representativeSelection;

        List<String> files = ABatchClassifier.getFiles(ABatchClassifier.BatchFolder.finalDataSet, ids);

        //RUN 1: With all neurons
        //final String[] neurons = new String[]{"75,37,15", "57,57,57", "3,3,3", "10,5,2", "49,49,49"};
        //final int[] epochsList = new int[]{1,10,25,50,100,150,200,250,300,350,400,450,500,600,700,800,900,1000};

        //RUN 2: With 2 smallest networks, higher epochs
        //final String[] neurons = new String[]{"3,3,3", "10,5,2"};
        //final int[] epochsList = new int[]{1100,1200,1300,1400,1500,2000,3000,5000};

        //RUN 2: With 3 all networks with best
        final String[] neurons = new String[]{"75,37,15", "57,57,57", "3,3,3", "10,5,2", "49,49,49"};
        final int[] epochsList = new int[]{1,50,100,150,200,250,300,350,400,450,500,550,600,650,700,750,800,850,900,950,1000,1050,1100,1150};

        for (String fileName : files) {
            for (final String neuronStr : neurons) {
                for (int e : epochsList) {
                    final String file = fileName;
                    final int epochs = e;
                    service.submit(new Runnable() {
                        public void run() {
                            try {
                                AnnSettings settings = AnnSettings.getDefaultSettings();
                                settings.Neurons = neuronStr;
                                settings.Epochs = epochs;

                                if(neuronStr.equals("75,37,15")){
                                    settings.LearningRate = 0.1;
                                    settings.Momentum = 0.0;
                                }else if(neuronStr.equals("57,57,57")){
                                    settings.LearningRate = 0.0;
                                    settings.Momentum = 0.0;
                                }else if(neuronStr.equals("3,3,3")){
                                    settings.LearningRate = 0.1;
                                    settings.Momentum = 0.7;
                                }else if(neuronStr.equals("10,5,2")){
                                    settings.LearningRate = 0.1;
                                    settings.Momentum = 0.7;
                                }else if(neuronStr.equals("49,49,49")){
                                    settings.LearningRate = 0.1;
                                    settings.Momentum = 0.2;
                                };


                                String trainFile = file + "train.csv";
                                String testFile = file + "test.csv";

                                DataSet dataSet = new DataSet(trainFile, testFile);
                                dataSet.setClassAttribute("Delay");

                                Ann ann = new Ann(settings);

                                StopWatch stopWatch = new StopWatch();
                                ann.train(dataSet.getTrainingData());
                                ann.evaluate(dataSet.getTestData());
                                String res = "Neurons: " + neuronStr + " , LearningRate: " + settings.LearningRate + " , Momentum: " + settings.Momentum + ", Epochs: " + settings.Epochs + ", file '" + file + "'.\n";
                                res += stopWatch.get("Training finished in X ms.\n");
                                res += ann.evalToString();
                                res += stopWatch.get("\nTesting finished in X ms.\n\n");

                                log.log(res);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    });

                }
            }

        }

        service.shutdown();
        try {
            service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static void KnnParameterTesting() {
        ExecutorService service = Executors.newFixedThreadPool(3);
        final Log log = Log.createPath("./Log.txt");
        String[] ids = representativeSelection;

        List<String> files = ABatchClassifier.getFiles(ABatchClassifier.BatchFolder.finalDataSet, ids);
        final String[] neurons = new String[]{ "10,5,2","53,53,53","3,3,3","30,15,6","15,7,3" };

        for (String fileName : files) {
            for (final String neuronStr : neurons) {
                for (double lr = 0; lr <= 1; lr += 0.1) {
                    final String file = fileName;
                    final double learningRate = lr;

                    for (double m = 0; m <= 1; m += 0.1) {
                        final double momentum = m;
                        service.submit(new Runnable() {
                            public void run() {
                                try {
                                    AnnSettings settings = AnnSettings.getDefaultSettings();
                                    settings.LearningRate = learningRate;
                                    settings.Momentum = momentum;
                                    settings.Neurons = neuronStr;
                                    settings.Epochs = 500;

                                    String trainFile = file + "train.csv";
                                    String testFile = file + "test.csv";

                                    DataSet dataSet = new DataSet(trainFile, testFile);
                                    dataSet.setClassAttribute("Delay");

                                    Ann ann = new Ann(settings);

                                    StopWatch stopWatch = new StopWatch();
                                    ann.train(dataSet.getTrainingData());
                                    ann.evaluate(dataSet.getTestData());
                                    String res = "Neurons: " + neuronStr + " , LearningRate: " + settings.LearningRate + " , Momentum: " + settings.Momentum + ", Epochs: " + settings.Epochs + ", file '" + file + "'.\n";
                                    res += stopWatch.get("Training finished in X ms.\n");
                                    res += ann.evalToString();
                                    res += stopWatch.get("\nTesting finished in X ms.\n\n");

                                    log.log(res);

                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }
                        });
                    }
                }
            }

        }

        service.shutdown();
        try {
            service.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }


}
