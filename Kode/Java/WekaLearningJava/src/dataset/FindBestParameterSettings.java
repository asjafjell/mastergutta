package dataset;

import utilities.FileUtilities;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * Created by Aleksander on 19.03.2014.
 */
public class FindBestParameterSettings {

    private String _path;

    public FindBestParameterSettings(String path) throws FileNotFoundException {
        _path = path;

        File file = new File(path);
        if(!file.exists()) throw new FileNotFoundException();
    }

    public String[] getAttributeArray(SettingsAttribute attribute) throws IOException {
        String[] data = getAttribute(attribute).split(";");

        if(data.length == 1 && data[0].equals("")){
            return new String[0];
        }else{
            return data;
        }

    }

    public String getAttribute(SettingsAttribute attribute) throws IOException {
        String line =  FileUtilities.readLines(_path)[attribute.index()];
        String[] contents = line.split(attribute.name() + ":");

        if(contents.length <= 1){
            throw new IllegalArgumentException("The line does not contain data for " + attribute.name());
        }

        return contents[1].trim();

    }


}
