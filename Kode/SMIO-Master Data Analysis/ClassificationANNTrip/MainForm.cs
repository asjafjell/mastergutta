﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AForge.Neuro;
using AForge.Neuro.Learning;
using ClassificationANNTrip.Classifiers;
using RealTimeGrapher;
using RealTimeGrapher.Helpers;
using Utilities.AtB.Trips;
using Utilities.Database;
using Utilities.Extensions;

namespace ClassificationANNTrip
{
    public partial class MainForm : Form
    {
        private readonly SMIODataContext _db;

        private List<TripItem> _trainingSet;
        private List<TripItem> _testSet;

        private AClassifier _classifier;

        private bool _readingSettings;

        private StringBuilder log = new StringBuilder();
        private bool _logChanged;

        public MainForm()
        {
            InitializeComponent();

            _db = new SMIODataContext();

            dtpStart.ValueChanged += (sender, args) => SaveSettings();
            dtpEnd.ValueChanged += (sender, args) => SaveSettings();
            txtLines.TextChanged += (sender, args) => SaveSettings();
            comClassifier.SelectedIndexChanged += (sender, args) => SaveSettings();
            comTestSet.SelectedIndexChanged += (sender, args) => SaveSettings();

            ReadSettings();

            SetWorking(false);
        }

        private void ReadSettings()
        {
            _readingSettings = true;
            var s = Properties.Settings.Default;
            comClassifier.SelectedIndex = s.Classifier;
            comTestSet.SelectedIndex = s.TestSet;
            dtpStart.Value = s.DataStart;
            dtpEnd.Value = s.DataEnd;
            txtLines.Text = s.Lines;
            _classifier.ReadSettings(s.ClassifierSettings);
            _readingSettings = false;
        }

        private void SaveSettings()
        {
            if (_readingSettings) return;
            var s = Properties.Settings.Default;
            s.Classifier = comClassifier.SelectedIndex;
            s.TestSet = comTestSet.SelectedIndex;
            s.DataStart = dtpStart.Value;
            s.DataEnd = dtpEnd.Value;
            s.Lines = txtLines.Text;
            s.ClassifierSettings = _classifier.SaveSettings(s.ClassifierSettings);
            s.Save();
        }

        private void SetStatus(Statuses status, string text = "")
        {
            if (InvokeRequired)
            {
                Invoke(new MethodInvoker(() => SetStatus(status, text)));
                return;
            }

            SetWorking((int)status > 99);
            lblStatus.Text = text;
        }

        private void Log(string msg)
        {
            lock (log)
            {
                log.AppendLine(msg);
                _logChanged = true;
            }
        }

        private void SetWorking(bool working)
        {
            if (InvokeRequired)
            {
                new MethodInvoker(() => SetWorking(working)).Invoke();
                return;
            }

            btnTrain.Enabled = !working;
            btnTest.Enabled = !working && _classifier.ReadyForTesting;
            btnPlotError.Enabled = !working;
            grpDataSettings.Enabled = !working;
            pnlSettingsHolder.Enabled = !working;
            btnBatch.Enabled = !working;
            txtBatch.ReadOnly = working;
            btnCancel.Enabled = working;
            prgWorking.Style = working ? ProgressBarStyle.Marquee : ProgressBarStyle.Blocks;
            lblStatus.Text = "Idle";

            RefreshLog();
        }

        private void btnPickLines_Click(object sender, EventArgs e)
        {
            frmList<string>.SelectItems(new[] { "Line" },
                                        _db.Lines.Select(p => p.ID).ToList().OrderBy(int.Parse).ToList(),
                                        s => new[] { s },
                                        s => { txtLines.Text = string.Join(", ", s); });
        }

        private async void btnTest_Click(object sender, EventArgs e)
        {
            try
            {
                var averageError = await _classifier.Test();
                Log("           " + averageError.ToString().Replace(", ", Environment.NewLine + "           "));
            }
            catch (Exception ex)
            {
                MessageBox.Show("Testing failed: " + ex.Message);
            }
        }

        private async void btnTrain_Click(object sender, EventArgs e)
        {
            var selIndex = comTestSet.SelectedIndex;
            try
            {
                if (!_classifier.ReadyForTraining)
                {
                    await Task.Run(() => FetchData(selIndex, 10));
                    _classifier.SetData(_trainingSet, _testSet);
                }
                _classifier.Train();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Training failed: " + ex.Message);
            }
        }

        private void FetchData(int testType, int percentageTest)
        {
            SetStatus(Statuses.Training, "Fetching data ...");

            var dataSet = _db.Trips.
                              Where(p => p.LineID == txtLines.Text).
                              Between(dtpStart.Value, dtpEnd.Value).
                              OrderBy(p => p.ScheduledDeparture).
                              ToList().
                              Where(p => !new[] {p.Delay, p.DelayLastTrip, p.DelayForThisTripLastWeek, p.AverageDelayForLastFiveTripsLastWeek, p.AverageDelayLastFiveTrips}.Any(c => c == int.MinValue)).
                              Where(p => !new[] {p.Delay, p.DelayLastTrip, p.DelayForThisTripLastWeek, p.AverageDelayForLastFiveTripsLastWeek, p.AverageDelayLastFiveTrips}.Any(c => Math.Abs(c) > 1500)).
                              ToList();

            var perc = (float)percentageTest / 100f;
            switch (testType)
            {
                case 0:
                    //First n%
                    _trainingSet = dataSet.Take((int) (dataSet.Count*perc)).ToList();
                    _testSet = dataSet.Skip(_trainingSet.Count()).ToList();
                    break;
                case 1:
                    //Every nth item.
                    const int s = 10;
                    _trainingSet = dataSet.Where((p, i) => i % s != 0).ToList();
                    _testSet = dataSet.Where((p, i) => i % s == 0).ToList();
                    break;
                case 2:
                    //Random n%
                    var rnd = new Random(DateTime.Now.Millisecond);
                    dataSet = dataSet.OrderBy(p => rnd.Next()).ToList();
                    _trainingSet = dataSet.Take((int)(dataSet.Count * perc)).OrderBy(p => p.ScheduledDeparture).ToList();
                    _testSet = dataSet.Skip(_trainingSet.Count()).OrderBy(p => p.ScheduledDeparture).ToList();
                    break;
            }

            SetStatus(Statuses.Idle);
        }
        
        bool _cancelOperation = false;
        private void btnCancel_Click(object sender, EventArgs e)
        {
            _cancelOperation = true;
            if (_currentBatchClassifier != null)
                _currentBatchClassifier.Abort();
            _classifier.Abort();
        }

        private void comClassifier_SelectedIndexChanged(object sender, EventArgs e)
        {
            _classifier = CreateSelectedClassifier();
            pnlSettingsHolder.Controls.Clear();
            pnlSettingsHolder.Controls.Add(_classifier.Editor);
            _classifier.Editor.Location = new Point(0, 0);
            _classifier.Editor.Width = pnlSettingsHolder.Width;
            _classifier.Editor.Anchor = AnchorStyles.Left | AnchorStyles.Right | AnchorStyles.Top;
            pnlSettingsHolder.Height = _classifier.Editor.Height;
            grpOutput.Top = pnlSettingsHolder.Top + pnlSettingsHolder.Height + 5;
            grpOutput.Height = Height - grpOutput.Top - 50;
            
            _classifier.ReadSettings(Properties.Settings.Default.ClassifierSettings);

            _classifier.EventLogged += Log;
            _classifier.StatusChanged += SetStatus;
            _classifier.SettingsChanged += SaveSettings;
        }

        private AClassifier CreateSelectedClassifier()
        {
            switch (comClassifier.SelectedIndex)
            {
                case 0:
                    return new AnnClassifier();
                case 1:
                    return new AnnClassifier();
            }
            return null;
        }

        private void dtpStart_ValueChanged(object sender, EventArgs e)
        {
            _classifier.ResetData();
        }

        private void timUpdateLog_Tick(object sender, EventArgs e)
        {
            if (chkUpdate.Checked)
            {
                RefreshLog();
            }

            lblError.Text = _classifier != null ? _classifier.CurrentError.ToString("n5") : "";
        }

        private void RefreshLog()
        {
            if (_logChanged == false) return;
            lock (log)
            {
                txtOutput.Text = log.ToString();
                txtOutput.SelectionStart = txtOutput.Text.Length;
                txtOutput.ScrollToCaret();
                _logChanged = false;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            log.Clear();
            txtOutput.Text = "";
        }

        private void chkUpdate_CheckedChanged(object sender, EventArgs e)
        {
            if (chkUpdate.Checked) RefreshLog();
        }

        private void btnBatch_Click(object sender, EventArgs e)
        {
            RunBatch(txtBatch.Lines.Where(p => !p.StartsWith("#") && !string.IsNullOrEmpty(p)).ToArray());
        }

        private AClassifier _currentBatchClassifier = null;
        private async void RunBatch(string[] configs)
        {
            SetStatus(Statuses.Training, "Generating classifiers ...");

            var classifiers = configs.Select(p => CreateSelectedClassifier()).ToList();
            var ci = 0;
            try
            {
                for (ci = 0; ci < configs.Length; ci++)
                    classifiers[ci].Editor.SetData(configs[ci].Split(';'));
            }
            catch (Exception ex)
            {
                Log(string.Format("Failed to create classifier from '{0}': {1}", configs[ci], ex.Message));
                Log("Aborting.");
                SetStatus(Statuses.Idle);
                return;
            }

            Log("Created " + classifiers.Count + " classifiers.");

            var selIndex = comTestSet.SelectedIndex;
            await Task.Run(() => FetchData(selIndex, 10));

            _cancelOperation = false;
            await Task.Run(async () =>
                {
                    var i = 0;
                    foreach (var c in classifiers)
                    {
                        _currentBatchClassifier = c;
                        SetStatus(Statuses.Training, string.Format("Running batch, {0}/{1} ...", ++i, classifiers.Count));

                        var swTrain = new Stopwatch();
                        var swTest = new Stopwatch();

                        swTrain.Start();

                        c.SetData(_trainingSet, _testSet);

                        await c.Train();

                        swTrain.Stop();
                        swTest.Start();

                        var error = await c.Test();

                        swTest.Stop();

                        Log(string.Format("  > {0} => {1}", configs[i - 1], Environment.NewLine + "          " + error.ToString().Replace(", ", Environment.NewLine + "          ")));
                        Log(string.Format("      > {0}", c.Summary));
                        Log(string.Format("      > Time usage (train/test): {0:n2}/{1:n2}", swTrain.ElapsedMilliseconds, swTest.ElapsedMilliseconds));
                        _currentBatchClassifier = null;
                        if (_cancelOperation)
                        {
                            Log("Operation aborted by user.");
                            break;
                        }
                    }
                });

            SetStatus(Statuses.Idle, "Done ...");
        }

        private void chkBatch_CheckedChanged(object sender, EventArgs e)
        {
            btnBatch.Top = btnTrain.Top;
            pnlBatch.Location = pnlSettingsHolder.Location;
            pnlBatch.Size = pnlSettingsHolder.Size;
            pnlBatch.BringToFront();

            if (string.IsNullOrEmpty(txtBatch.Text))
            {
                txtBatch.Text = string.Format("# One configuration per line.{1}# Each line contains all settings{1}# for this classifier, separated{1}# by a ';'.{1}{0}", string.Join(";", _classifier.Editor.GetData()), Environment.NewLine);
            }

            pnlBatch.Visible = btnBatch.Visible = chkBatch.Checked;
            btnTrain.Visible = btnTest.Visible = !chkBatch.Checked;
        }

        private void exportToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportAndRunApplication(new string[0]);
            this.Close();
        }

        private static void ExportAndRunApplication(string[] parameters)
        {
            var path = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
            var target = System.IO.Path.Combine(path, "..", "Run");
            var i = 0;
            while (System.IO.Directory.Exists(target))
            {
                try
                {
                    System.IO.Directory.Delete(target, true);
                }
                catch (Exception)
                {
                    target = System.IO.Path.Combine(path, "..", "Run") + "_" + i++;
                }
            }
            System.IO.Directory.CreateDirectory(target);
            System.IO.Directory.GetFiles(path, "*.*").ToList().ForEach(p => System.IO.File.Copy(p, System.IO.Path.Combine(target, System.IO.Path.GetFileName(p))));
            System.Diagnostics.Process.Start(System.IO.Path.Combine(target, System.IO.Path.GetFileName(System.Reflection.Assembly.GetExecutingAssembly().Location)), parameters.Length > 0 ? "" : "export");
        }

        private void exportDataToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var data = this.GetInputValues().ToList();
            Clipboard.SetText(string.Join(Environment.NewLine, data.Select(p => p.Item1 + " = " + p.Item2)));
        }

        private void comTestSet_SelectedIndexChanged(object sender, EventArgs e)
        {
            _classifier.ResetData();
        }

        private void btnCopyLog_Click(object sender, EventArgs e)
        {
            Clipboard.SetText(txtOutput.Text);
        }

        private void btnPlotError_Click(object sender, EventArgs e)
        {
            if (_classifier == null) return;
            _classifier.PlotError(this);
        }
    }
}
