﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ClassificationANNTrip.Controls
{
    public partial class SettingsSvm : AClassifierSettings
    {
        public int[] NeuronCounts
        {
            get { return txtNodes.Text.Split(',').Select(int.Parse).Concat(new[] {1}).ToArray(); }
            set { txtNodes.Text = string.Join(", ", value); }
        }

        public double SigmoidAlphaValue
        {
            get { return double.Parse(txtSigmoidAlpha.Text); }
            set { txtSigmoidAlpha.Text = value.ToString(); }
        }

        public double LearningErrorLimit
        {
            get { return double.Parse(txtErrorLimit.Text); }
            set { txtErrorLimit.Text = value.ToString(); }
        }

        public double LearningRate
        {
            get { return double.Parse(txtLearningRate.Text); }
            set { txtLearningRate.Text = value.ToString(); }
        }

        public double Momentum
        {
            get { return double.Parse(txtMomentum.Text); }
            set { txtMomentum.Text = value.ToString(); }
        }

        public SettingsSvm()
        {
            InitializeComponent();
        }

        private void btnReset_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Reset();
        }

        public override void Reset()
        {
            txtNodes.Text = "5";
            txtSigmoidAlpha.Text = "2";
            txtLearningRate.Text = "0,1";
            txtErrorLimit.Text = "0,01";
            txtMomentum.Text = "0,0";
        }

        protected override void ValidateData()
        {
            throw new NotImplementedException();
        }
    }
}
