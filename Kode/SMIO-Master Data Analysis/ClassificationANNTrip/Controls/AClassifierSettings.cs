﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Utilities.Extensions;

namespace ClassificationANNTrip.Controls
{
    public abstract class AClassifierSettings : UserControl
    {
        public event Action SettingsChanged;

        protected virtual void OnSettingsChanged()
        {
            var handler = SettingsChanged;
            if (handler != null) handler();
        }

        public AClassifierSettings()
        {
            Load += (sender, args) => SetEvents();
        }

        private void SetEvents()
        {
            this.GetTextBoxes().ToList().ForEach(p => p.TextChanged += (o, eventArgs) => OnSettingsChanged());
        }

        public abstract void Reset();

        public string[] GetData()
        {
            return this.GetTextBoxes().Select(p => p.Text).ToArray();
        }

        public void SetData(string[] settings)
        {
            var txts = this.GetTextBoxes().ToArray();
            for (var i = 0; i < txts.Length; i++)
            {
                txts[i].Text = settings[i];
            }

            ValidateData();
        }

        protected abstract void ValidateData();
    }
}
