﻿namespace ClassificationANNTrip.Controls
{
    partial class SettingsAnn
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpAnnSettings = new System.Windows.Forms.GroupBox();
            this.btnReset = new System.Windows.Forms.LinkLabel();
            this.txtMomentum = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtErrorLimit = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLearningRate = new System.Windows.Forms.TextBox();
            this.txtSigmoidAlpha = new System.Windows.Forms.TextBox();
            this.lblNodes = new System.Windows.Forms.Label();
            this.lblSigmoidAlpha = new System.Windows.Forms.Label();
            this.txtNodes = new System.Windows.Forms.TextBox();
            this.grpAnnSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpAnnSettings
            // 
            this.grpAnnSettings.Controls.Add(this.btnReset);
            this.grpAnnSettings.Controls.Add(this.txtMomentum);
            this.grpAnnSettings.Controls.Add(this.label5);
            this.grpAnnSettings.Controls.Add(this.txtErrorLimit);
            this.grpAnnSettings.Controls.Add(this.label3);
            this.grpAnnSettings.Controls.Add(this.label4);
            this.grpAnnSettings.Controls.Add(this.txtLearningRate);
            this.grpAnnSettings.Controls.Add(this.txtSigmoidAlpha);
            this.grpAnnSettings.Controls.Add(this.lblNodes);
            this.grpAnnSettings.Controls.Add(this.lblSigmoidAlpha);
            this.grpAnnSettings.Controls.Add(this.txtNodes);
            this.grpAnnSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.grpAnnSettings.Location = new System.Drawing.Point(0, 0);
            this.grpAnnSettings.Name = "grpAnnSettings";
            this.grpAnnSettings.Size = new System.Drawing.Size(409, 174);
            this.grpAnnSettings.TabIndex = 20;
            this.grpAnnSettings.TabStop = false;
            this.grpAnnSettings.Text = "ANN Settings";
            // 
            // btnReset
            // 
            this.btnReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnReset.AutoSize = true;
            this.btnReset.Location = new System.Drawing.Point(368, 154);
            this.btnReset.Name = "btnReset";
            this.btnReset.Size = new System.Drawing.Size(35, 13);
            this.btnReset.TabIndex = 21;
            this.btnReset.TabStop = true;
            this.btnReset.Text = "Reset";
            this.btnReset.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.btnReset_LinkClicked);
            // 
            // txtMomentum
            // 
            this.txtMomentum.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtMomentum.Location = new System.Drawing.Point(86, 127);
            this.txtMomentum.Name = "txtMomentum";
            this.txtMomentum.Size = new System.Drawing.Size(317, 20);
            this.txtMomentum.TabIndex = 32;
            this.txtMomentum.Text = "0,0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 130);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(62, 13);
            this.label5.TabIndex = 31;
            this.label5.Text = "Momentum:";
            // 
            // txtErrorLimit
            // 
            this.txtErrorLimit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtErrorLimit.Location = new System.Drawing.Point(86, 101);
            this.txtErrorLimit.Name = "txtErrorLimit";
            this.txtErrorLimit.Size = new System.Drawing.Size(317, 20);
            this.txtErrorLimit.TabIndex = 30;
            this.txtErrorLimit.Text = "0,01";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 77);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(72, 13);
            this.label3.TabIndex = 27;
            this.label3.Text = "Learning rate:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 104);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 29;
            this.label4.Text = "Error limit:";
            // 
            // txtLearningRate
            // 
            this.txtLearningRate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLearningRate.Location = new System.Drawing.Point(86, 74);
            this.txtLearningRate.Name = "txtLearningRate";
            this.txtLearningRate.Size = new System.Drawing.Size(317, 20);
            this.txtLearningRate.TabIndex = 28;
            this.txtLearningRate.Text = "0,1";
            // 
            // txtSigmoidAlpha
            // 
            this.txtSigmoidAlpha.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSigmoidAlpha.Location = new System.Drawing.Point(86, 48);
            this.txtSigmoidAlpha.Name = "txtSigmoidAlpha";
            this.txtSigmoidAlpha.Size = new System.Drawing.Size(317, 20);
            this.txtSigmoidAlpha.TabIndex = 26;
            this.txtSigmoidAlpha.Text = "2";
            // 
            // lblNodes
            // 
            this.lblNodes.AutoSize = true;
            this.lblNodes.Location = new System.Drawing.Point(6, 24);
            this.lblNodes.Name = "lblNodes";
            this.lblNodes.Size = new System.Drawing.Size(41, 13);
            this.lblNodes.TabIndex = 23;
            this.lblNodes.Text = "Nodes:";
            // 
            // lblSigmoidAlpha
            // 
            this.lblSigmoidAlpha.AutoSize = true;
            this.lblSigmoidAlpha.Location = new System.Drawing.Point(6, 51);
            this.lblSigmoidAlpha.Name = "lblSigmoidAlpha";
            this.lblSigmoidAlpha.Size = new System.Drawing.Size(46, 13);
            this.lblSigmoidAlpha.TabIndex = 25;
            this.lblSigmoidAlpha.Text = "SigAlph:";
            // 
            // txtNodes
            // 
            this.txtNodes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtNodes.Location = new System.Drawing.Point(86, 21);
            this.txtNodes.Name = "txtNodes";
            this.txtNodes.Size = new System.Drawing.Size(317, 20);
            this.txtNodes.TabIndex = 24;
            this.txtNodes.Text = "5";
            // 
            // SettingsAnn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.grpAnnSettings);
            this.Name = "SettingsAnn";
            this.Size = new System.Drawing.Size(409, 174);
            this.grpAnnSettings.ResumeLayout(false);
            this.grpAnnSettings.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grpAnnSettings;
        private System.Windows.Forms.TextBox txtMomentum;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtErrorLimit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtLearningRate;
        private System.Windows.Forms.TextBox txtSigmoidAlpha;
        private System.Windows.Forms.Label lblNodes;
        private System.Windows.Forms.Label lblSigmoidAlpha;
        private System.Windows.Forms.TextBox txtNodes;
        private System.Windows.Forms.LinkLabel btnReset;
    }
}
