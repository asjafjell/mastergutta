﻿namespace ClassificationANNTrip
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.prgWorking = new System.Windows.Forms.ProgressBar();
            this.btnPickLines = new System.Windows.Forms.Button();
            this.lblLines = new System.Windows.Forms.Label();
            this.txtLines = new System.Windows.Forms.TextBox();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.lblStatus = new System.Windows.Forms.Label();
            this.txtOutput = new System.Windows.Forms.TextBox();
            this.grpOutput = new System.Windows.Forms.GroupBox();
            this.chkBatch = new System.Windows.Forms.CheckBox();
            this.btnBatch = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.chkUpdate = new System.Windows.Forms.CheckBox();
            this.btnTrain = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnTest = new System.Windows.Forms.Button();
            this.grpDataSettings = new System.Windows.Forms.GroupBox();
            this.comTestSet = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.comClassifier = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.pnlSettingsHolder = new System.Windows.Forms.Panel();
            this.timUpdateLog = new System.Windows.Forms.Timer(this.components);
            this.pnlBatch = new System.Windows.Forms.Panel();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtBatch = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportDataToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.btnCopyLog = new System.Windows.Forms.Button();
            this.lblError = new System.Windows.Forms.Label();
            this.btnPlotError = new System.Windows.Forms.Button();
            this.grpOutput.SuspendLayout();
            this.grpDataSettings.SuspendLayout();
            this.pnlBatch.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // prgWorking
            // 
            this.prgWorking.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.prgWorking.Location = new System.Drawing.Point(6, 63);
            this.prgWorking.Name = "prgWorking";
            this.prgWorking.Size = new System.Drawing.Size(357, 23);
            this.prgWorking.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
            this.prgWorking.TabIndex = 2;
            // 
            // btnPickLines
            // 
            this.btnPickLines.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPickLines.Location = new System.Drawing.Point(335, 75);
            this.btnPickLines.Name = "btnPickLines";
            this.btnPickLines.Size = new System.Drawing.Size(28, 21);
            this.btnPickLines.TabIndex = 18;
            this.btnPickLines.Text = "...";
            this.btnPickLines.UseVisualStyleBackColor = true;
            this.btnPickLines.Click += new System.EventHandler(this.btnPickLines_Click);
            // 
            // lblLines
            // 
            this.lblLines.AutoSize = true;
            this.lblLines.Location = new System.Drawing.Point(6, 80);
            this.lblLines.Name = "lblLines";
            this.lblLines.Size = new System.Drawing.Size(41, 13);
            this.lblLines.TabIndex = 17;
            this.lblLines.Text = "Line(s):";
            // 
            // txtLines
            // 
            this.txtLines.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtLines.Location = new System.Drawing.Point(86, 76);
            this.txtLines.Name = "txtLines";
            this.txtLines.Size = new System.Drawing.Size(243, 20);
            this.txtLines.TabIndex = 16;
            this.txtLines.Text = "22";
            // 
            // dtpEnd
            // 
            this.dtpEnd.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpEnd.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(86, 49);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(277, 20);
            this.dtpEnd.TabIndex = 15;
            this.dtpEnd.Value = new System.DateTime(2014, 2, 3, 16, 0, 0, 0);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 14;
            this.label2.Text = "End:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 13;
            this.label1.Text = "Start:";
            // 
            // dtpStart
            // 
            this.dtpStart.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpStart.CustomFormat = "dd.MM.yyyy HH:mm";
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(86, 21);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(277, 20);
            this.dtpStart.TabIndex = 12;
            this.dtpStart.Value = new System.DateTime(2014, 1, 1, 0, 0, 0, 0);
            this.dtpStart.ValueChanged += new System.EventHandler(this.dtpStart_ValueChanged);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(6, 47);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(24, 13);
            this.lblStatus.TabIndex = 20;
            this.lblStatus.Text = "Idle";
            // 
            // txtOutput
            // 
            this.txtOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtOutput.Location = new System.Drawing.Point(6, 92);
            this.txtOutput.Multiline = true;
            this.txtOutput.Name = "txtOutput";
            this.txtOutput.ReadOnly = true;
            this.txtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtOutput.Size = new System.Drawing.Size(357, 107);
            this.txtOutput.TabIndex = 21;
            // 
            // grpOutput
            // 
            this.grpOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpOutput.Controls.Add(this.btnPlotError);
            this.grpOutput.Controls.Add(this.lblError);
            this.grpOutput.Controls.Add(this.btnCopyLog);
            this.grpOutput.Controls.Add(this.chkBatch);
            this.grpOutput.Controls.Add(this.btnBatch);
            this.grpOutput.Controls.Add(this.btnClear);
            this.grpOutput.Controls.Add(this.chkUpdate);
            this.grpOutput.Controls.Add(this.btnTrain);
            this.grpOutput.Controls.Add(this.btnCancel);
            this.grpOutput.Controls.Add(this.btnTest);
            this.grpOutput.Controls.Add(this.lblStatus);
            this.grpOutput.Controls.Add(this.txtOutput);
            this.grpOutput.Controls.Add(this.prgWorking);
            this.grpOutput.Location = new System.Drawing.Point(12, 409);
            this.grpOutput.Name = "grpOutput";
            this.grpOutput.Size = new System.Drawing.Size(369, 234);
            this.grpOutput.TabIndex = 22;
            this.grpOutput.TabStop = false;
            this.grpOutput.Text = "Output";
            // 
            // chkBatch
            // 
            this.chkBatch.AutoSize = true;
            this.chkBatch.Location = new System.Drawing.Point(92, 23);
            this.chkBatch.Name = "chkBatch";
            this.chkBatch.Size = new System.Drawing.Size(54, 17);
            this.chkBatch.TabIndex = 33;
            this.chkBatch.Text = "Batch";
            this.chkBatch.UseVisualStyleBackColor = true;
            this.chkBatch.CheckedChanged += new System.EventHandler(this.chkBatch_CheckedChanged);
            // 
            // btnBatch
            // 
            this.btnBatch.Location = new System.Drawing.Point(6, 34);
            this.btnBatch.Name = "btnBatch";
            this.btnBatch.Size = new System.Drawing.Size(75, 23);
            this.btnBatch.TabIndex = 32;
            this.btnBatch.Text = "Batch";
            this.btnBatch.UseVisualStyleBackColor = true;
            this.btnBatch.Visible = false;
            this.btnBatch.Click += new System.EventHandler(this.btnBatch_Click);
            // 
            // btnClear
            // 
            this.btnClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClear.Location = new System.Drawing.Point(259, 205);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(49, 23);
            this.btnClear.TabIndex = 31;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // chkUpdate
            // 
            this.chkUpdate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkUpdate.AutoSize = true;
            this.chkUpdate.Checked = true;
            this.chkUpdate.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkUpdate.Location = new System.Drawing.Point(6, 209);
            this.chkUpdate.Name = "chkUpdate";
            this.chkUpdate.Size = new System.Drawing.Size(61, 17);
            this.chkUpdate.TabIndex = 30;
            this.chkUpdate.Text = "Update";
            this.chkUpdate.UseVisualStyleBackColor = true;
            this.chkUpdate.CheckedChanged += new System.EventHandler(this.chkUpdate_CheckedChanged);
            // 
            // btnTrain
            // 
            this.btnTrain.Location = new System.Drawing.Point(6, 19);
            this.btnTrain.Name = "btnTrain";
            this.btnTrain.Size = new System.Drawing.Size(75, 23);
            this.btnTrain.TabIndex = 28;
            this.btnTrain.Text = "Train";
            this.btnTrain.UseVisualStyleBackColor = true;
            this.btnTrain.Click += new System.EventHandler(this.btnTrain_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCancel.Location = new System.Drawing.Point(314, 205);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(49, 23);
            this.btnCancel.TabIndex = 27;
            this.btnCancel.Text = "Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnTest
            // 
            this.btnTest.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnTest.Location = new System.Drawing.Point(288, 19);
            this.btnTest.Name = "btnTest";
            this.btnTest.Size = new System.Drawing.Size(75, 23);
            this.btnTest.TabIndex = 29;
            this.btnTest.Text = "Test";
            this.btnTest.UseVisualStyleBackColor = true;
            this.btnTest.Click += new System.EventHandler(this.btnTest_Click);
            // 
            // grpDataSettings
            // 
            this.grpDataSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpDataSettings.Controls.Add(this.comTestSet);
            this.grpDataSettings.Controls.Add(this.label3);
            this.grpDataSettings.Controls.Add(this.comClassifier);
            this.grpDataSettings.Controls.Add(this.label6);
            this.grpDataSettings.Controls.Add(this.dtpStart);
            this.grpDataSettings.Controls.Add(this.label1);
            this.grpDataSettings.Controls.Add(this.btnPickLines);
            this.grpDataSettings.Controls.Add(this.dtpEnd);
            this.grpDataSettings.Controls.Add(this.txtLines);
            this.grpDataSettings.Controls.Add(this.lblLines);
            this.grpDataSettings.Controls.Add(this.label2);
            this.grpDataSettings.Location = new System.Drawing.Point(12, 27);
            this.grpDataSettings.Name = "grpDataSettings";
            this.grpDataSettings.Size = new System.Drawing.Size(369, 161);
            this.grpDataSettings.TabIndex = 27;
            this.grpDataSettings.TabStop = false;
            this.grpDataSettings.Text = "Data settings";
            // 
            // comTestSet
            // 
            this.comTestSet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comTestSet.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comTestSet.FormattingEnabled = true;
            this.comTestSet.Items.AddRange(new object[] {
            "Last 10%",
            "Every 10th item",
            "Random 10%"});
            this.comTestSet.Location = new System.Drawing.Point(86, 129);
            this.comTestSet.Name = "comTestSet";
            this.comTestSet.Size = new System.Drawing.Size(277, 21);
            this.comTestSet.TabIndex = 22;
            this.comTestSet.SelectedIndexChanged += new System.EventHandler(this.comTestSet_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 132);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Test set:";
            // 
            // comClassifier
            // 
            this.comClassifier.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.comClassifier.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comClassifier.FormattingEnabled = true;
            this.comClassifier.Items.AddRange(new object[] {
            "ANN",
            "SVM"});
            this.comClassifier.Location = new System.Drawing.Point(86, 102);
            this.comClassifier.Name = "comClassifier";
            this.comClassifier.Size = new System.Drawing.Size(277, 21);
            this.comClassifier.TabIndex = 20;
            this.comClassifier.SelectedIndexChanged += new System.EventHandler(this.comClassifier_SelectedIndexChanged);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 105);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(51, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Classifier:";
            // 
            // pnlSettingsHolder
            // 
            this.pnlSettingsHolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlSettingsHolder.Location = new System.Drawing.Point(12, 194);
            this.pnlSettingsHolder.Name = "pnlSettingsHolder";
            this.pnlSettingsHolder.Size = new System.Drawing.Size(369, 209);
            this.pnlSettingsHolder.TabIndex = 30;
            // 
            // timUpdateLog
            // 
            this.timUpdateLog.Enabled = true;
            this.timUpdateLog.Interval = 1000;
            this.timUpdateLog.Tick += new System.EventHandler(this.timUpdateLog_Tick);
            // 
            // pnlBatch
            // 
            this.pnlBatch.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pnlBatch.Controls.Add(this.groupBox1);
            this.pnlBatch.Location = new System.Drawing.Point(0, 183);
            this.pnlBatch.Name = "pnlBatch";
            this.pnlBatch.Size = new System.Drawing.Size(358, 180);
            this.pnlBatch.TabIndex = 0;
            this.pnlBatch.Visible = false;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtBatch);
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(358, 180);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Batch input";
            // 
            // txtBatch
            // 
            this.txtBatch.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBatch.Location = new System.Drawing.Point(6, 19);
            this.txtBatch.Multiline = true;
            this.txtBatch.Name = "txtBatch";
            this.txtBatch.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtBatch.Size = new System.Drawing.Size(346, 155);
            this.txtBatch.TabIndex = 35;
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(393, 24);
            this.menuStrip1.TabIndex = 35;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exportToolStripMenuItem,
            this.exportDataToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exportToolStripMenuItem
            // 
            this.exportToolStripMenuItem.Name = "exportToolStripMenuItem";
            this.exportToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.X)));
            this.exportToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.exportToolStripMenuItem.Text = "Run separate";
            this.exportToolStripMenuItem.Click += new System.EventHandler(this.exportToolStripMenuItem_Click);
            // 
            // exportDataToolStripMenuItem
            // 
            this.exportDataToolStripMenuItem.Name = "exportDataToolStripMenuItem";
            this.exportDataToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)(((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.Shift) 
            | System.Windows.Forms.Keys.E)));
            this.exportDataToolStripMenuItem.Size = new System.Drawing.Size(215, 22);
            this.exportDataToolStripMenuItem.Text = "Export data";
            this.exportDataToolStripMenuItem.Click += new System.EventHandler(this.exportDataToolStripMenuItem_Click);
            // 
            // btnCopyLog
            // 
            this.btnCopyLog.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyLog.Location = new System.Drawing.Point(204, 205);
            this.btnCopyLog.Name = "btnCopyLog";
            this.btnCopyLog.Size = new System.Drawing.Size(49, 23);
            this.btnCopyLog.TabIndex = 34;
            this.btnCopyLog.Text = "Copy";
            this.btnCopyLog.UseVisualStyleBackColor = true;
            this.btnCopyLog.Click += new System.EventHandler(this.btnCopyLog_Click);
            // 
            // lblError
            // 
            this.lblError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblError.Location = new System.Drawing.Point(288, 45);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(75, 15);
            this.lblError.TabIndex = 35;
            this.lblError.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // btnPlotError
            // 
            this.btnPlotError.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPlotError.Location = new System.Drawing.Point(132, 205);
            this.btnPlotError.Name = "btnPlotError";
            this.btnPlotError.Size = new System.Drawing.Size(66, 23);
            this.btnPlotError.TabIndex = 36;
            this.btnPlotError.Text = "Plot error";
            this.btnPlotError.UseVisualStyleBackColor = true;
            this.btnPlotError.Click += new System.EventHandler(this.btnPlotError_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(393, 655);
            this.Controls.Add(this.pnlBatch);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.pnlSettingsHolder);
            this.Controls.Add(this.grpOutput);
            this.Controls.Add(this.grpDataSettings);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ANN Classification Test";
            this.grpOutput.ResumeLayout(false);
            this.grpOutput.PerformLayout();
            this.grpDataSettings.ResumeLayout(false);
            this.grpDataSettings.PerformLayout();
            this.pnlBatch.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ProgressBar prgWorking;
        private System.Windows.Forms.Button btnPickLines;
        private System.Windows.Forms.Label lblLines;
        private System.Windows.Forms.TextBox txtLines;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.TextBox txtOutput;
        private System.Windows.Forms.GroupBox grpOutput;
        private System.Windows.Forms.GroupBox grpDataSettings;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnTrain;
        private System.Windows.Forms.Button btnTest;
        private System.Windows.Forms.ComboBox comClassifier;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Panel pnlSettingsHolder;
        private System.Windows.Forms.CheckBox chkUpdate;
        private System.Windows.Forms.Timer timUpdateLog;
        private System.Windows.Forms.ComboBox comTestSet;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Button btnBatch;
        private System.Windows.Forms.CheckBox chkBatch;
        private System.Windows.Forms.Panel pnlBatch;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtBatch;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportDataToolStripMenuItem;
        private System.Windows.Forms.Button btnCopyLog;
        private System.Windows.Forms.Label lblError;
        private System.Windows.Forms.Button btnPlotError;
    }
}