﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Accord.Statistics;
using ClassificationANNTrip.Controls;
using RealTimeGrapher.Helpers;
using Utilities.AtB.Trips;
using Utilities.Database;

namespace ClassificationANNTrip.Classifiers
{
    public enum Statuses
    {
        Idle = 0,
        Training = 100, //Statuses that should disable the GUI are higher than or equal to 100.
        Testing = 101,
    }

    public abstract class AClassifier
    {
        protected bool cancelOperation;

        /// <summary>
        /// Create a user control that can be used to configure this classifier. 
        /// This control will automatically be added to the running form.
        /// </summary>
        public AClassifierSettings Editor
        {
            get { return _editor; }
            protected set
            {
                _editor = value;
                _editor.SettingsChanged += OnSettingsChanged;
            }
        }

        protected List<double> Errors;

        /// <summary>
        /// Returns the current error of the ongoing training (if any).
        /// </summary>
        public abstract double CurrentError { get; protected set; }

        /// <summary>
        /// If the output values is larger than 1, stuff needs to be scaled. 
        /// The neural network will always return values between [0,1]. All
        /// output will be scaled between Min and Max.
        /// </summary>
        public double Min { get; set; }

        /// <summary>
        /// If the output values is larger than 1, stuff needs to be scaled. 
        /// The neural network will always return values between [0,1]. All
        /// output will be scaled between Min and Max.
        /// </summary>
        public double Max { get; set; }

        /// <summary>
        /// Input for training the classifier.
        /// </summary>
        public double[][] Input { get; set; }

        /// <summary>
        /// Output for training the classifier.
        /// </summary>
        public double[][] Output { get; set; }

        /// <summary>
        /// Input for testing the classifier.
        /// </summary>
        public double[][] TestInput { get; set; }

        /// <summary>
        /// Output for testing the classifier.
        /// </summary>
        public double[][] TestOutput { get; set; }

        /// <summary>
        /// Return true if the classifier has been trained, and is ready to be tested.
        /// </summary>
        public abstract bool ReadyForTesting { get; }

        /// <summary>
        /// Returns true if both Input and Output is set and contains data.
        /// </summary>
        public bool ReadyForTraining { get { return !(Input == null || Input.Length == 0 || Input[0].Length == 0 || Output == null || Output.Length == 0 || Output[0].Length == 0); } }

        public abstract string Summary { get; }

        /// <summary>
        /// Trigger this event to update the form status (if the classifier is working, 
        /// the form will be disabled and a progress bar will show). The given string will be
        /// shown above the progress bar.
        /// </summary>
        public event Action<Statuses, string> StatusChanged;

        public event Action SettingsChanged;

        /// <summary>
        /// Trigger this event to log something to the text field in the main form.
        /// </summary>
        public event Action<string> EventLogged;

        protected SMIODataContext _db = new SMIODataContext();

        public AClassifier()
        {

        }

        /// <summary>
        /// Trains the classifier. Note: this is just a wrapper function. Classifier implementations should override DoTraining.
        /// </summary>
        public async Task Train()
        {
            if (!ReadyForTraining)
                throw new Exception("Input and Output variables are not set.");

            SetStatus(Statuses.Training, "Training network ...");

            await DoTraining();
        }

        /// <summary>
        /// This is the function actually doing the training.
        /// </summary>
        protected abstract Task DoTraining();

        /// <summary>
        /// Tests the classifier. Note: this is just a wrapper function. Classifier implementations should override DoTesting.
        /// </summary>
        public Task<ErrorKeeper> Test()
        {
            if (ReadyForTesting == false)
                throw new Exception("This classifier is not ready for testing yet. It probably hasn't been trained.");

            SetStatus(Statuses.Training, "Testing network ...");

            return DoTesting();
        }

        /// <summary>
        /// This is the function actually doing the testing.
        /// </summary>
        protected abstract Task<ErrorKeeper> DoTesting();

        protected void SetStatus(Statuses status, string description)
        {
            var sc = StatusChanged;
            if (sc != null)
                sc(status, description);
        }

        protected void Log(string text)
        {
            var sc = EventLogged;
            if (sc != null)
                sc(text);
        }

        protected virtual void OnSettingsChanged()
        {
            var handler = SettingsChanged;
            if (handler != null) handler();
        }

        /// <summary>
        /// Wrapper function to set Input, Output, TestInput and TestOutput from a list of objects. 
        /// Throw an exception if T is of an unsupported data type.
        /// </summary>
        /// <param name="trainingSet"></param>
        /// <param name="testSet"></param>
        public abstract void SetData<T>(IEnumerable<T> trainingSet, IEnumerable<T> testSet);

        public async void Abort()
        {
            cancelOperation = true;
            await Task.Delay(500);
            Log("User cancelled.");
        }

        private int _plotID = 1;
        private PlotHelper _plot;
        private AClassifierSettings _editor;

        public void PlotError(Form owner)
        {
            if (Errors == null || !Errors.Any())
            {
                MessageBox.Show("No errors to plot.");
                return;
            }
            Plot(owner, Errors);
        }

        public void Plot(Form owner, IEnumerable<double> errors)
        {
            if (_plot == null)
                _plot = PlotHelper.Plot(owner, errors, "Error progress " + _plotID++);
            else
                _plot.Plot(errors, "Error progress " + _plotID++);
        }

        public void ResetData()
        {
            Input = null;
            Output = null;
            TestInput = null;
            TestOutput = null;
        }

        public void ReadSettings(StringCollection settings)
        {
            if (settings == null) return;
            foreach (var setting in settings)
            {
                if(setting.StartsWith(GetType().ToString() + ";"))
                    Editor.SetData(setting.Split(';').Skip(1).ToArray());
            }
        }

        public StringCollection SaveSettings(StringCollection settings)
        {
            if(settings == null) settings = new StringCollection();
            var index = -1;
            for (var i = 0; i < settings.Count; i++)
            {
                if (settings[i].StartsWith(GetType().ToString() + ";"))
                {
                    index = i;
                    break;
                }
            }
            
            var s = GetType().ToString() + ";" + string.Join(";", Editor.GetData());
            if (index == -1)
                settings.Add(s);
            else
                settings[index] = s;

            return settings;
        }

        protected double ScaleToAnn(double d)
        {
            if (Min >= 0 && Min <= 1 && Max >= 0 && Max <= 1) return d;
            return (d - Min) / (Max - Min);
        }

        protected double ScaleFromAnn(double d)
        {
            if (Min >= 0 && Min <= 1 && Max >= 0 && Max <= 1) return d;
            return d*(Max - Min) + Min;
        }
    }

    public class ErrorKeeper
    {
        private readonly double[] _errors;

        public double AverageError
        {
            get { return _errors.Average(); }
        }

        public double MedianError
        {
            get { return _errors.Median(); }
        }

        public double StandardDeviation
        {
            get { return _errors.StandardDeviation(); }
        }

        public double MinimumError
        {
            get { return _errors.Min(); }
        }

        public double MaxmimumError
        {
            get { return _errors.Max(); }
        }

        public double MeanAbsoluteError
        {
            get { return _errors.Select(Math.Abs).Average(); }
        }

        public double MeanSquaredError
        {
            get { return _errors.Select(p => Math.Pow(p, 2)).Average(); }
        }

        public double RootMeanSquaredError
        {
            get { return Math.Sqrt(MeanSquaredError); }
        }
        
        public ErrorKeeper(IEnumerable<double> errors)
        {
            _errors = errors.ToArray();
        }

        public override string ToString()
        {
            return string.Format("Average: {0:n3}, Median: {1:n3}, Min: {2:n3}, Max: {3:n3}, STD: {4:n3}, MAE: {5:n3}, MSE: {6:n3}, RMSE: {7:n3}", 
                AverageError, MedianError, MinimumError, MaxmimumError, StandardDeviation, MeanAbsoluteError, MeanSquaredError, RootMeanSquaredError);
        }
    }
}
