﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Utilities.AtB.Utilities;
using Utilities.EnumUtil;
using Utilities.PowerSet;

namespace Weka_Evaluation_Analyzer
{
    public partial class frmMain : Form
    {
        private List<DataSetInfo> _dataset;
        private string[] _files;
        private string[] _classifiers;

        private bool _loadingSettings;

        private static readonly List<Tuple<char, Func<string, string, bool>>> FilterFunctions = new List<Tuple<char, Func<string, string, bool>>>()
            {
                Tuple.Create<char, Func<string, string, bool>>('=', (text, flt) => text == flt),
                Tuple.Create<char, Func<string, string, bool>>(':', (text, flt) => text.ToLower().Contains(flt.ToLower())),
                Tuple.Create<char, Func<string, string, bool>>('!', (text, flt) => !text.ToLower().Contains(flt.ToLower())),
                Tuple.Create<char, Func<string, string, bool>>('>', (text, flt) => Numeric(text) > Numeric(flt)),
                Tuple.Create<char, Func<string, string, bool>>('<', (text, flt) => Numeric(text) < Numeric(flt))
            };

        private static readonly Dictionary<string, SeriesChartType> ChartTypes = new Dictionary<string, SeriesChartType>
            {
                {"l", SeriesChartType.Line},
                {"bp", SeriesChartType.BoxPlot},
                {"s", SeriesChartType.Spline},
                {"sb", SeriesChartType.StackedBar100}
            };

        private readonly string _plotFile = Data.GetWekaFile("settings", "analyzer_plot.xml");

        public frmMain()
        {
            InitializeComponent();
            chrtPlot.Palette = ChartColorPalette.None;
            chrtPlot.PaletteCustomColors = new Color[]{Color.Red, Color.Peru, Color.RoyalBlue, Color.PaleGreen, Color.MediumVioletRed, Color.Salmon, Color.MediumPurple, Color.Maroon, Color.DarkOliveGreen, Color.Yellow, Color.Black, Color.Gray};
            chrtPlot.Serializer.Content = SerializationContents.Default;
            chrtPlot.Serializer.IsResetWhenLoading = false;

            LoadSettings();

            ReloadData();
        }

        private void ReloadData()
        {
            _files = System.IO.Directory.GetFiles(Data.GetWekaFile("finalDataSet"));
            _classifiers = _files.
                Where(p => p.EndsWith(".txt")).
                Select(System.IO.Path.GetFileNameWithoutExtension).
                Select(p => p.Split('.')[1]).
                Distinct().
                ToArray();

            var cols = "Filnavn;Busstoppkode;Busstoppnavn;Treningslengde;Testlengde;Treningsstart;Treningsstopp;Teststart;Teststopp;Beskrivelse;Kommentar;Nøkkelord;Gruppe;Subgruppe".Split(';').ToList();

            cols.AddRange(new[] { "# tren", "# test" });
            cols.AddRange(_classifiers.Select(p => "MAE for '" + p + "'"));
            cols.AddRange(_classifiers.Select(p => "Corr. for '" + p + "'"));
            cols.AddRange(_classifiers.Select(p => "RMSE. for '" + p + "'"));

            var results = EnumUtil.GetValues<MiniPowType>();
            cols.AddRange(results.Select(r => "SVR " +  r.ToString()));

            //cols.Add("ANN Weather MAE");
            //cols.Add("ANN Worst attributes");
            //cols.Add("ANN Best MAE");
            //cols.Add("ANN Best MAE sets");

            //cols.Add("KNN Worst attributes");
            //cols.Add("KNN Best MAE");
            //cols.Add("KNN Best MAE sets");

            cols.ForEach(p => lstDatasets.Columns.Add(lstDatasets.Columns.Count + " " + p));

            _dataset = DataSetConstants.DataSet().ToList();

            ListDataSet();
        }

        private void ListDataSet()
        {
            IEnumerable<DataSetInfo> matching = null;
            if (!Attempt("Invalid filter.", () => matching = _dataset.Where(Match))) return;
            if (!Attempt("Invalid sort.", () => matching = Sort(matching))) return;

            lstDatasets.BeginUpdate();
            lstDatasets.Items.Clear();
            lstDatasets.Items.AddRange(matching.Select(p => p.ListViewItem).ToArray());
            lstDatasets.EndUpdate();
            SetColumnWidths();
            RefreshItemCount();
        }

        private void RefreshItemCount()
        {
            lblRows.Text = lstDatasets.Items.Count.ToString("n0");
            if (lstDatasets.SelectedItems.Count > 0)
                lblRows.Text += " / " + lstDatasets.SelectedItems.Count.ToString("n0");
        }

        private IEnumerable<DataSetInfo> Sort(IEnumerable<DataSetInfo> matching)
        {
            if (string.IsNullOrEmpty(txtSort.Text.Trim())) return matching;
            var sorts = (chkSort.Checked ? txtSort.Text : "").Split(';');
            foreach (var sort in sorts.Reverse())
            {
                if (string.IsNullOrEmpty(sort.Trim())) continue;
                var handleAsString = sort.Contains("s");
                var ascending = !sort.Contains("d");
                var i = int.Parse(string.Join("", sort.ToCharArray().Where(char.IsDigit)));
                if (handleAsString)
                {
                    if (ascending)
                        matching = matching.OrderBy(p => p.ListViewItem.SubItems[i].Text);
                    else
                        matching = matching.OrderByDescending(p => p.ListViewItem.SubItems[i].Text);
                }
                else
                {
                    if (ascending)
                        matching = matching.OrderBy(p => Numeric(p.ListViewItem.SubItems[i].Text));
                    else
                        matching = matching.OrderByDescending(p => Numeric(p.ListViewItem.SubItems[i].Text));
                }
            }
            return matching;
        }

        private bool Match(DataSetInfo dataSetInfo)
        {
            if (string.IsNullOrEmpty(txtFilter.Text.Trim())) return true;
            var filter = (chkFilter.Checked ? txtFilter.Text : "").Split(';');
            var line = string.Join("\t", dataSetInfo.ListViewItem.SubItems.Cast<ListViewItem.ListViewSubItem>().Select(p => p.Text));
            foreach (var f in filter)
            {
                if (string.IsNullOrEmpty(f.Trim())) continue;
                var func = FilterFunctions.FirstOrDefault(p => f.Contains(p.Item1));
                if (func == null) continue;
                var parts = f.Split(func.Item1);
                if (parts[0] == "") parts[0] = "a";
                var flt = string.Join(func.Item1.ToString(), parts.Skip(1));
                if (!func.Item2(parts[0] == "a" ? line : dataSetInfo.ListViewItem.SubItems[int.Parse(parts[0])].Text, flt))
                    return false;
            }

            return true;
        }

        private static float Numeric(string num)
        {
            float f;
            if (float.TryParse(num, out f) || float.TryParse(num.Replace(".", ","), out f)) return f;

            int x;
            if (int.TryParse(num, out x)) return x;
            if (num.EndsWith("t")) return Numeric(num.Substring(0, num.Length - 1));
            if (num.EndsWith("h")) return Numeric(num.Substring(0, num.Length - 1));
            if (num.EndsWith("d")) return 24*Numeric(num.Substring(0, num.Length - 1));
            if (num.EndsWith("m")) return 24*30*Numeric(num.Substring(0, num.Length - 1));
            if (num.EndsWith("AAS")) return 0;

            DateTime dt;
            if (DateTime.TryParse(num, out dt)) return dt.Ticks;

            return int.MinValue;
        }

        private void txtFilter_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                ListDataSet();
                SaveSettings();
            }
        }

        private void btnPlot_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtPlotColumns.Text.Trim()))
            {
                MessageBox.Show("No columns selected.");
                return;
            }
            try
            {
                var rows = chkSelected.Checked ? lstDatasets.SelectedItems.Cast<ListViewItem>() : lstDatasets.Items.Cast<ListViewItem>();
                var data = txtPlotColumns.Text.Split(';').
                                          Select(p => new
                                              {
                                                  Value = int.Parse(string.Join("", p.ToCharArray().Where(char.IsDigit))),
                                                  Code = string.Join("", p.ToCharArray().Where(c => !char.IsDigit(c)))
                                              }).
                                          Select(p => new
                                              {
                                                  Index = p.Value,
                                                  Type = p.Code == "b" ? "" : p.Code,
                                                  Data = rows.Select(c => Numeric(c.SubItems[p.Value].Text))
                                              });

                foreach (var series in data)
                {
                    var s = new Series(chrtPlot.Series.Count + " " + string.Join(" ", lstDatasets.Columns[series.Index].Text.Split(' ').Skip(1)));
                    if (ChartTypes.ContainsKey(series.Type)) s.ChartType = ChartTypes[series.Type];
                    foreach (var point in series.Data)
                        s.Points.AddXY(s.Points.Count, point);
                    chrtPlot.Series.Add(s);
                }

                RefreshPlotButtons();
                SaveSettings();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message);
            }
        }

        private void SetColumnWidths()
        {
            var hide = new int[0];
            Attempt("Invalid hide", () => hide = (chkHide.Checked ? txtHide.Text : "").Split(';').Where(p => !string.IsNullOrEmpty(p.Trim())).Select(int.Parse).ToArray());

            lstDatasets.BeginUpdate();
            for (var i = 0; i < lstDatasets.Columns.Count; i++)
            {
                if (hide.Contains(i))
                    lstDatasets.Columns[i].Width = 0;
                else
                {
                    lstDatasets.AutoResizeColumn(i, ColumnHeaderAutoResizeStyle.ColumnContent);
                    if (new[] {0, 1, 3, 4}.Contains(i) || i > 11)
                        lstDatasets.AutoResizeColumn(i, ColumnHeaderAutoResizeStyle.HeaderSize);
                }
            }
            lstDatasets.EndUpdate();
        }

        private bool Attempt(string message, Action a)
        {
            try
            {
                a();
                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show(message + e.Message + Environment.NewLine + e.StackTrace);
                return false;
            }
        }

        private void txtPlotColumns_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) btnPlot.PerformClick();
        }

        private void SaveSettings()
        {
            if (_loadingSettings) return;
            var s = Properties.Settings.Default;
            s.Filter = txtFilter.Text;
            s.Sort = txtSort.Text;
            s.Hide = txtHide.Text;
            s.Plot = txtPlotColumns.Text;
            s.PlotSelected = chkSelected.Checked;
            s.Csv = txtCsv.Text;
            s.ActivateFilter = chkFilter.Checked;
            s.ActivateHide = chkHide.Checked;
            s.ActivateSort = chkSort.Checked;
            s.Save();
            SavePlot(_plotFile);
        }

        private void SavePlot(string file)
        {
            chrtPlot.Serializer.Save(file);
            var lines = System.IO.File.ReadAllLines(file, Encoding.Default);
            lines[0] = "<Chart>";
            System.IO.File.WriteAllLines(file, lines, Encoding.Default);
        }

        private void LoadSettings()
        {
            _loadingSettings = true;
            var s = Properties.Settings.Default;
            txtFilter.Text = s.Filter;
            txtSort.Text = s.Sort;
            txtHide.Text = s.Hide;
            txtPlotColumns.Text = s.Plot;
            chkSelected.Checked = s.PlotSelected;
            chkFilter.Checked = s.ActivateFilter;
            chkHide.Checked = s.ActivateHide;
            chkSort.Checked = s.ActivateSort;
            txtCsv.Text = s.Csv;
            LoadPlot(_plotFile);
            _loadingSettings = false;
        }

        private void LoadPlot(string file)
        {
            if (System.IO.File.Exists(file))
            {
                chrtPlot.Serializer.Load(file);
                RefreshPlotButtons();
            }
        }

        private void RefreshPlotButtons()
        {
            btnExportPlot.Visible = btnSave.Visible = btnPickSeries.Visible = chrtPlot.Series.Count > 0;
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            chrtPlot.Series.Clear();
            btnPickSeries.Visible = true;
        }

        private void txtPlotColumns_TextChanged(object sender, EventArgs e)
        {
            SaveSettings();
        }

        private void btnPickSeries_Click(object sender, EventArgs e)
        {
            RealTimeGrapher.frmList<string>.SelectItems(new[] {"Name"}, chrtPlot.Series.Select(p => p.Name).ToList(), chrtPlot.Series.Where(p => p.Enabled).Select(p => p.Name).ToList(), p => new[] {p}, list =>
                {
                    foreach (var s in chrtPlot.Series)
                        s.Enabled = list.Contains(s.Name);
                });
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var path = ShowSaveFileDialog("*.xml");
            if(!string.IsNullOrEmpty(path)) SavePlot(path);
        }

        private void btnLoadPlot_Click(object sender, EventArgs e)
        {
            var sfd = new OpenFileDialog() { Filter = "*.xml|*.xml", FileName = Properties.Settings.Default.LastPath };
            if (sfd.ShowDialog() == DialogResult.Cancel) return;
            Properties.Settings.Default.LastPath = sfd.FileName;
            LoadPlot(sfd.FileName);
        }

        private void btnExportPlot_Click(object sender, EventArgs e)
        {
            var path = ShowSaveFileDialog("*.png");
            if(!string.IsNullOrEmpty(path)) chrtPlot.SaveImage(path, ChartImageFormat.Png);
        }

        private void btnCopyCsv_Click(object sender, EventArgs e)
        {
            var s = GenerateCsv();
            if (!string.IsNullOrEmpty(s))
                Clipboard.SetText(s);
        }

        private void btnGoups_Click(object sender, EventArgs e)
        {
            var s = GroupCSV();
            if (!string.IsNullOrEmpty(s))
                Clipboard.SetText(s);
        }

        private void btnSaveCsv_Click(object sender, EventArgs e)
        {
            var s = GenerateCsv();
            if (string.IsNullOrEmpty(s)) return;
            var path = ShowSaveFileDialog("*.csv");
            if (!string.IsNullOrEmpty(path)) System.IO.File.WriteAllText(path, s, Encoding.Default);
        }

        private string GenerateCsv()
        {
            if (lstDatasets.SelectedItems.Count == 0)
            {
                MessageBox.Show("Nothing selected.");
                return null;
            }
            int[] cols = null;
            if (!Attempt("Invalid column selection.", () => cols = string.IsNullOrEmpty(txtCsv.Text) ? Enumerable.Range(1, lstDatasets.Columns.Count).ToArray() : txtCsv.Text.Split(';').Select(int.Parse).ToArray())) return null;
            return string.Join("µ", lstDatasets.Columns.Cast<ColumnHeader>().Where((p, i) => cols.Contains(i)).Select(p => p.Text)) + Environment.NewLine +
                   string.Join(Environment.NewLine, lstDatasets.SelectedItems.Cast<ListViewItem>().Select(p => 
                       string.Join("µ", p.SubItems.Cast<ListViewItem.ListViewSubItem>().Where((c, i) => cols.Contains(i)).Select((c, i) => c.Text))));
        }

        private string GroupCSV()
        {
            int[] cols = null;
            if (!Attempt("Invalid column selection.", () => cols = string.IsNullOrEmpty(txtCsv.Text) ? Enumerable.Range(1, lstDatasets.Columns.Count).ToArray() : txtCsv.Text.Split(';').Select(int.Parse).ToArray())) return null;

            var data = _dataset.Select(p => new {p.Group, p.Subgroup, Values = cols.Select(d => Numeric(p.ListViewItem.SubItems[d].Text)).ToArray()}).ToArray();
            var indexes = Enumerable.Range(0, cols.Length);

            var groupsandsubgroups = data.GroupBy(p => new
            {
                p.Group,
                p.Subgroup
            }).Select(p => new
                {
                    Descs = new[] {p.Key.Group, p.Key.Subgroup},
                    Averages = indexes.Select(c=>p.Average(d=>d.Values[c]).ToString("n5"))
                }).Select(p => new[] { p.Descs, p.Averages }).Select(p => p.SelectMany(c => c));

            var groups = data.GroupBy(p => new
            {
                p.Group
            }).Select(p => new
                {
                    Descs = new[] { p.Key.Group },
                    Averages = indexes.Select(c => p.Average(d => d.Values[c]).ToString("n5"))
                }).Select(p => new[] { p.Descs, p.Averages }).Select(p => p.SelectMany(c => c));
            return string.Join(Environment.NewLine, groupsandsubgroups.Select(p => string.Join("µ", p))) + Environment.NewLine + Environment.NewLine + Environment.NewLine + string.Join(Environment.NewLine, groups.Select(p => string.Join("µ", p)));
        }

        private string ShowSaveFileDialog(string filter)
        {
            var sfd = new SaveFileDialog()
            {
                Filter = string.Format("{0}|{0}", filter),
                FileName = Properties.Settings.Default.LastPath,
                InitialDirectory = System.IO.Path.GetDirectoryName(Properties.Settings.Default.LastPath)
            };
            if (sfd.ShowDialog() == DialogResult.Cancel) return null;
            Properties.Settings.Default.LastPath = sfd.FileName;
            SaveSettings();
            return sfd.FileName;
        }

        private string ShowFolderBrowser()
        {
            var sfd = new FolderBrowserDialog() {SelectedPath = Properties.Settings.Default.LastSaveToFolder};
            if (sfd.ShowDialog() == DialogResult.Cancel) return null;
            Properties.Settings.Default.LastSaveToFolder = sfd.SelectedPath;
            SaveSettings();
            return sfd.SelectedPath;
        }

        private void chkFilter_CheckedChanged(object sender, EventArgs e)
        {
            if (_loadingSettings) return;
            ListDataSet();
            SaveSettings();
        }

        private void chkHide_CheckedChanged(object sender, EventArgs e)
        {
            if (_loadingSettings) return;
            SetColumnWidths();
            SaveSettings();
        }

        private void copyDatasetsToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyFiles(true, false);
        }

        private void copyEvaluationsToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyFiles(false, true);
        }

        private void copyAllFilesToToolStripMenuItem_Click(object sender, EventArgs e)
        {
            CopyFiles(true, true);
        }

        private void CopyFiles(bool dataset, bool evaluations)
        {
            if (lstDatasets.SelectedItems.Count == 0)
            {
                MessageBox.Show("Nothing selected.");
                return;
            }

            var destination = ShowFolderBrowser();
            if (string.IsNullOrEmpty(destination)) return;
            var sets = lstDatasets.SelectedItems.Cast<ListViewItem>().Select(p => p.Tag as DataSetInfo);
            var count = 0;
            Action<string> copy = f =>
                {
                    if (!System.IO.File.Exists(f)) return;
                    System.IO.File.Copy(f, System.IO.Path.Combine(destination, System.IO.Path.GetFileName(f)), true);
                    count++;
                };
            foreach (var ds in sets)
            {
                if (dataset)
                    foreach (var f in new[] {ds.TrainingFile, ds.TestFile})
                        copy(f);
                if (evaluations)
                    foreach (var f in ds.EvaluationFiles.Where(System.IO.File.Exists))
                        copy(f);
            }
            MessageBox.Show(string.Format("Copied {0} files to '{1}'.", count, destination));
        }

        private void lstDatasets_SelectedIndexChanged(object sender, EventArgs e)
        {
            RefreshItemCount();
        }

        private void reloadDataSetsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ReloadData();
        }

        private void txtHide_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                SetColumnWidths();
                SaveSettings();
            }
        }

        private void lstDatasets_DoubleClick(object sender, EventArgs e)
        {
            var hit = lstDatasets.HitTest(lstDatasets.PointToClient(Control.MousePosition));
            var columnindex = hit.Item.SubItems.IndexOf(hit.SubItem);
            CalculateInformation(columnindex);
        }

        private void btnCalculate_Click(object sender, EventArgs e)
        {
            CalculateInformation(_lastColumnIndex);
        }

        private int _lastColumnIndex = 0;
        private void CalculateInformation(int columnindex)
        {
            _lastColumnIndex = columnindex;
            lblSelectionInfo.Text = string.Format("Average total: {0}, Sum total: {1}, Average listed: {2}, Sum listed: {3}, Average selected: {4}, Sum selected: {5}",
                _dataset.Average(p => Numeric(p.ListViewItem.SubItems[columnindex].Text)).ToString("n5"),
                _dataset.Sum(p => Numeric(p.ListViewItem.SubItems[columnindex].Text)).ToString("n5"),
                lstDatasets.Items.Cast<ListViewItem>().Average(p => Numeric(p.SubItems[columnindex].Text)).ToString("n5"),
                lstDatasets.Items.Cast<ListViewItem>().Sum(p => Numeric(p.SubItems[columnindex].Text)).ToString("n5"),
                lstDatasets.SelectedItems.Cast<ListViewItem>().Average(p => Numeric(p.SubItems[columnindex].Text)).ToString("n5"),
                lstDatasets.SelectedItems.Cast<ListViewItem>().Sum(p => Numeric(p.SubItems[columnindex].Text)).ToString("n5"));
        }
    }
}
