﻿namespace Weka_Evaluation_Analyzer
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea3 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend3 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.lstDatasets = new System.Windows.Forms.ListView();
            this.mnuDataset = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.copyDatasetsToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyEvaluationsToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.copyAllFilesToToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripSeparator();
            this.reloadDataSetsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.txtFilter = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.btnExportPlot = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnLoadPlot = new System.Windows.Forms.Button();
            this.btnPickSeries = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.chrtPlot = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.txtPlotColumns = new System.Windows.Forms.TextBox();
            this.chkSelected = new System.Windows.Forms.CheckBox();
            this.btnPlot = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSort = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtHide = new System.Windows.Forms.TextBox();
            this.lblRows = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCsv = new System.Windows.Forms.TextBox();
            this.btnCopyCsv = new System.Windows.Forms.Button();
            this.btnSaveCsv = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.chkFilter = new System.Windows.Forms.CheckBox();
            this.chkSort = new System.Windows.Forms.CheckBox();
            this.chkHide = new System.Windows.Forms.CheckBox();
            this.btnGoups = new System.Windows.Forms.Button();
            this.lblSelectionInfo = new System.Windows.Forms.Label();
            this.btnCalculate = new System.Windows.Forms.Button();
            this.mnuDataset.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrtPlot)).BeginInit();
            this.SuspendLayout();
            // 
            // lstDatasets
            // 
            this.lstDatasets.ContextMenuStrip = this.mnuDataset;
            this.lstDatasets.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lstDatasets.FullRowSelect = true;
            this.lstDatasets.HideSelection = false;
            this.lstDatasets.Location = new System.Drawing.Point(0, 0);
            this.lstDatasets.Name = "lstDatasets";
            this.lstDatasets.Size = new System.Drawing.Size(840, 277);
            this.lstDatasets.TabIndex = 0;
            this.lstDatasets.UseCompatibleStateImageBehavior = false;
            this.lstDatasets.View = System.Windows.Forms.View.Details;
            this.lstDatasets.SelectedIndexChanged += new System.EventHandler(this.lstDatasets_SelectedIndexChanged);
            this.lstDatasets.DoubleClick += new System.EventHandler(this.lstDatasets_DoubleClick);
            // 
            // mnuDataset
            // 
            this.mnuDataset.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.copyDatasetsToToolStripMenuItem,
            this.copyEvaluationsToToolStripMenuItem,
            this.copyAllFilesToToolStripMenuItem,
            this.toolStripMenuItem1,
            this.reloadDataSetsToolStripMenuItem});
            this.mnuDataset.Name = "mnuDataset";
            this.mnuDataset.Size = new System.Drawing.Size(192, 98);
            // 
            // copyDatasetsToToolStripMenuItem
            // 
            this.copyDatasetsToToolStripMenuItem.Name = "copyDatasetsToToolStripMenuItem";
            this.copyDatasetsToToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.copyDatasetsToToolStripMenuItem.Text = "Copy datasets to ...";
            this.copyDatasetsToToolStripMenuItem.Click += new System.EventHandler(this.copyDatasetsToToolStripMenuItem_Click);
            // 
            // copyEvaluationsToToolStripMenuItem
            // 
            this.copyEvaluationsToToolStripMenuItem.Name = "copyEvaluationsToToolStripMenuItem";
            this.copyEvaluationsToToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.copyEvaluationsToToolStripMenuItem.Text = "Copy evaluations to ...";
            this.copyEvaluationsToToolStripMenuItem.Click += new System.EventHandler(this.copyEvaluationsToToolStripMenuItem_Click);
            // 
            // copyAllFilesToToolStripMenuItem
            // 
            this.copyAllFilesToToolStripMenuItem.Name = "copyAllFilesToToolStripMenuItem";
            this.copyAllFilesToToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.copyAllFilesToToolStripMenuItem.Text = "Copy all files to ...";
            this.copyAllFilesToToolStripMenuItem.Click += new System.EventHandler(this.copyAllFilesToToolStripMenuItem_Click);
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(188, 6);
            // 
            // reloadDataSetsToolStripMenuItem
            // 
            this.reloadDataSetsToolStripMenuItem.Name = "reloadDataSetsToolStripMenuItem";
            this.reloadDataSetsToolStripMenuItem.Size = new System.Drawing.Size(191, 22);
            this.reloadDataSetsToolStripMenuItem.Text = "Reload data sets";
            this.reloadDataSetsToolStripMenuItem.Click += new System.EventHandler(this.reloadDataSetsToolStripMenuItem_Click);
            // 
            // txtFilter
            // 
            this.txtFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilter.Location = new System.Drawing.Point(50, 7);
            this.txtFilter.Name = "txtFilter";
            this.txtFilter.Size = new System.Drawing.Size(722, 20);
            this.txtFilter.TabIndex = 1;
            this.toolTip1.SetToolTip(this.txtFilter, resources.GetString("txtFilter.ToolTip"));
            this.txtFilter.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFilter_KeyUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Filter:";
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.Location = new System.Drawing.Point(15, 124);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.lstDatasets);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.btnExportPlot);
            this.splitContainer1.Panel2.Controls.Add(this.btnSave);
            this.splitContainer1.Panel2.Controls.Add(this.btnLoadPlot);
            this.splitContainer1.Panel2.Controls.Add(this.btnPickSeries);
            this.splitContainer1.Panel2.Controls.Add(this.btnClear);
            this.splitContainer1.Panel2.Controls.Add(this.chrtPlot);
            this.splitContainer1.Panel2.Controls.Add(this.txtPlotColumns);
            this.splitContainer1.Panel2.Controls.Add(this.chkSelected);
            this.splitContainer1.Panel2.Controls.Add(this.btnPlot);
            this.splitContainer1.Size = new System.Drawing.Size(840, 557);
            this.splitContainer1.SplitterDistance = 277;
            this.splitContainer1.TabIndex = 3;
            // 
            // btnExportPlot
            // 
            this.btnExportPlot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnExportPlot.Location = new System.Drawing.Point(650, 249);
            this.btnExportPlot.Name = "btnExportPlot";
            this.btnExportPlot.Size = new System.Drawing.Size(44, 23);
            this.btnExportPlot.TabIndex = 12;
            this.btnExportPlot.Text = "PNG";
            this.toolTip1.SetToolTip(this.btnExportPlot, "Export the current chart to a PNG image file.");
            this.btnExportPlot.UseVisualStyleBackColor = true;
            this.btnExportPlot.Visible = false;
            this.btnExportPlot.Click += new System.EventHandler(this.btnExportPlot_Click);
            // 
            // btnSave
            // 
            this.btnSave.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSave.Location = new System.Drawing.Point(700, 249);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(44, 23);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Save";
            this.toolTip1.SetToolTip(this.btnSave, "Save the current chart to an XML file. It can later be reopened in this applicati" +
        "on by using \'Load\'.");
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnLoadPlot
            // 
            this.btnLoadPlot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnLoadPlot.Location = new System.Drawing.Point(750, 249);
            this.btnLoadPlot.Name = "btnLoadPlot";
            this.btnLoadPlot.Size = new System.Drawing.Size(39, 23);
            this.btnLoadPlot.TabIndex = 10;
            this.btnLoadPlot.Text = "Load";
            this.toolTip1.SetToolTip(this.btnLoadPlot, "Load a previously saved chart.");
            this.btnLoadPlot.UseVisualStyleBackColor = true;
            this.btnLoadPlot.Click += new System.EventHandler(this.btnLoadPlot_Click);
            // 
            // btnPickSeries
            // 
            this.btnPickSeries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnPickSeries.Location = new System.Drawing.Point(795, 249);
            this.btnPickSeries.Name = "btnPickSeries";
            this.btnPickSeries.Size = new System.Drawing.Size(39, 23);
            this.btnPickSeries.TabIndex = 9;
            this.btnPickSeries.Text = "Pick";
            this.toolTip1.SetToolTip(this.btnPickSeries, "Pick which series to show in the chart.");
            this.btnPickSeries.UseVisualStyleBackColor = true;
            this.btnPickSeries.Visible = false;
            this.btnPickSeries.Click += new System.EventHandler(this.btnPickSeries_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(84, 3);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 23);
            this.btnClear.TabIndex = 8;
            this.btnClear.Text = "Clear";
            this.toolTip1.SetToolTip(this.btnClear, "Clear the current chart.");
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // chrtPlot
            // 
            this.chrtPlot.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea3.Name = "ChartArea1";
            this.chrtPlot.ChartAreas.Add(chartArea3);
            legend3.Name = "Legend1";
            this.chrtPlot.Legends.Add(legend3);
            this.chrtPlot.Location = new System.Drawing.Point(0, 32);
            this.chrtPlot.Name = "chrtPlot";
            this.chrtPlot.Size = new System.Drawing.Size(840, 244);
            this.chrtPlot.TabIndex = 7;
            this.chrtPlot.Text = "chart1";
            // 
            // txtPlotColumns
            // 
            this.txtPlotColumns.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPlotColumns.Location = new System.Drawing.Point(240, 5);
            this.txtPlotColumns.Name = "txtPlotColumns";
            this.txtPlotColumns.Size = new System.Drawing.Size(600, 20);
            this.txtPlotColumns.TabIndex = 6;
            this.toolTip1.SetToolTip(this.txtPlotColumns, "Which columns to plot, separated by a semi-colon. Example: \'15;16;17\'.");
            this.txtPlotColumns.TextChanged += new System.EventHandler(this.txtPlotColumns_TextChanged);
            this.txtPlotColumns.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtPlotColumns_KeyUp);
            // 
            // chkSelected
            // 
            this.chkSelected.AutoSize = true;
            this.chkSelected.Location = new System.Drawing.Point(166, 7);
            this.chkSelected.Name = "chkSelected";
            this.chkSelected.Size = new System.Drawing.Size(68, 17);
            this.chkSelected.TabIndex = 1;
            this.chkSelected.Text = "Selected";
            this.toolTip1.SetToolTip(this.chkSelected, "If this is checked, only selected rows will be plotted when you press \'Plot\'. If " +
        "not, all rows currently being displayed in the list will be plotted.");
            this.chkSelected.UseVisualStyleBackColor = true;
            // 
            // btnPlot
            // 
            this.btnPlot.Location = new System.Drawing.Point(3, 3);
            this.btnPlot.Name = "btnPlot";
            this.btnPlot.Size = new System.Drawing.Size(75, 23);
            this.btnPlot.TabIndex = 0;
            this.btnPlot.Text = "Plot";
            this.toolTip1.SetToolTip(this.btnPlot, "Plot the selected columns. If anything is plotted previously, pressing this butto" +
        "n will add more series without clearing the old chart.");
            this.btnPlot.UseVisualStyleBackColor = true;
            this.btnPlot.Click += new System.EventHandler(this.btnPlot_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 35);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Sort:";
            // 
            // txtSort
            // 
            this.txtSort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSort.Location = new System.Drawing.Point(50, 33);
            this.txtSort.Name = "txtSort";
            this.txtSort.Size = new System.Drawing.Size(784, 20);
            this.txtSort.TabIndex = 4;
            this.toolTip1.SetToolTip(this.txtSort, resources.GetString("txtSort.ToolTip"));
            this.txtSort.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFilter_KeyUp);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 61);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Hide:";
            // 
            // txtHide
            // 
            this.txtHide.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtHide.Location = new System.Drawing.Point(50, 59);
            this.txtHide.Name = "txtHide";
            this.txtHide.Size = new System.Drawing.Size(784, 20);
            this.txtHide.TabIndex = 6;
            this.toolTip1.SetToolTip(this.txtHide, "Hides all columns listed. Separate columns with a semi-colon.\r\nExample: 5;6;7 //H" +
        "ides columns 5, 6 and 7.");
            this.txtHide.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtHide_KeyUp);
            // 
            // lblRows
            // 
            this.lblRows.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblRows.Location = new System.Drawing.Point(755, 10);
            this.lblRows.Name = "lblRows";
            this.lblRows.Size = new System.Drawing.Size(100, 23);
            this.lblRows.TabIndex = 8;
            this.lblRows.Text = "0";
            this.lblRows.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 87);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "CSV:";
            // 
            // txtCsv
            // 
            this.txtCsv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCsv.Location = new System.Drawing.Point(50, 85);
            this.txtCsv.Name = "txtCsv";
            this.txtCsv.Size = new System.Drawing.Size(655, 20);
            this.txtCsv.TabIndex = 9;
            this.toolTip1.SetToolTip(this.txtCsv, "Choose which columns to export to CSV.\r\nExample: 2;4;5;6 //Exports columns 2, 4, " +
        "5 and 6 to CSV.\r\nOnly selected rows will be exported.");
            // 
            // btnCopyCsv
            // 
            this.btnCopyCsv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCopyCsv.Location = new System.Drawing.Point(761, 83);
            this.btnCopyCsv.Name = "btnCopyCsv";
            this.btnCopyCsv.Size = new System.Drawing.Size(44, 23);
            this.btnCopyCsv.TabIndex = 14;
            this.btnCopyCsv.Text = "Copy";
            this.toolTip1.SetToolTip(this.btnCopyCsv, "Copy the generated CSV to the clipboard.");
            this.btnCopyCsv.UseVisualStyleBackColor = true;
            this.btnCopyCsv.Click += new System.EventHandler(this.btnCopyCsv_Click);
            // 
            // btnSaveCsv
            // 
            this.btnSaveCsv.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSaveCsv.Location = new System.Drawing.Point(811, 83);
            this.btnSaveCsv.Name = "btnSaveCsv";
            this.btnSaveCsv.Size = new System.Drawing.Size(44, 23);
            this.btnSaveCsv.TabIndex = 13;
            this.btnSaveCsv.Text = "Save";
            this.toolTip1.SetToolTip(this.btnSaveCsv, "Export the generated CSV to a file.");
            this.btnSaveCsv.UseVisualStyleBackColor = true;
            this.btnSaveCsv.Click += new System.EventHandler(this.btnSaveCsv_Click);
            // 
            // toolTip1
            // 
            this.toolTip1.AutomaticDelay = 0;
            this.toolTip1.AutoPopDelay = 50000;
            this.toolTip1.InitialDelay = 100;
            this.toolTip1.ReshowDelay = 100;
            // 
            // chkFilter
            // 
            this.chkFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkFilter.AutoSize = true;
            this.chkFilter.Location = new System.Drawing.Point(778, 9);
            this.chkFilter.Name = "chkFilter";
            this.chkFilter.Size = new System.Drawing.Size(15, 14);
            this.chkFilter.TabIndex = 15;
            this.toolTip1.SetToolTip(this.chkFilter, "Activate or deactivate the filter.");
            this.chkFilter.UseVisualStyleBackColor = true;
            this.chkFilter.CheckedChanged += new System.EventHandler(this.chkFilter_CheckedChanged);
            // 
            // chkSort
            // 
            this.chkSort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkSort.AutoSize = true;
            this.chkSort.Location = new System.Drawing.Point(840, 36);
            this.chkSort.Name = "chkSort";
            this.chkSort.Size = new System.Drawing.Size(15, 14);
            this.chkSort.TabIndex = 16;
            this.toolTip1.SetToolTip(this.chkSort, "Activate or deactivate the sorting.");
            this.chkSort.UseVisualStyleBackColor = true;
            this.chkSort.CheckedChanged += new System.EventHandler(this.chkFilter_CheckedChanged);
            // 
            // chkHide
            // 
            this.chkHide.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.chkHide.AutoSize = true;
            this.chkHide.Location = new System.Drawing.Point(840, 62);
            this.chkHide.Name = "chkHide";
            this.chkHide.Size = new System.Drawing.Size(15, 14);
            this.chkHide.TabIndex = 17;
            this.toolTip1.SetToolTip(this.chkHide, "Activate or deactivate the hidden columns.");
            this.chkHide.UseVisualStyleBackColor = true;
            this.chkHide.CheckedChanged += new System.EventHandler(this.chkHide_CheckedChanged);
            // 
            // btnGoups
            // 
            this.btnGoups.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGoups.Location = new System.Drawing.Point(711, 83);
            this.btnGoups.Name = "btnGoups";
            this.btnGoups.Size = new System.Drawing.Size(44, 23);
            this.btnGoups.TabIndex = 18;
            this.btnGoups.Text = "Group";
            this.toolTip1.SetToolTip(this.btnGoups, "Copy the generated CSV to the clipboard.");
            this.btnGoups.UseVisualStyleBackColor = true;
            this.btnGoups.Click += new System.EventHandler(this.btnGoups_Click);
            // 
            // lblSelectionInfo
            // 
            this.lblSelectionInfo.AutoSize = true;
            this.lblSelectionInfo.Location = new System.Drawing.Point(66, 108);
            this.lblSelectionInfo.Name = "lblSelectionInfo";
            this.lblSelectionInfo.Size = new System.Drawing.Size(10, 13);
            this.lblSelectionInfo.TabIndex = 19;
            this.lblSelectionInfo.Text = "-";
            // 
            // btnCalculate
            // 
            this.btnCalculate.Location = new System.Drawing.Point(49, 108);
            this.btnCalculate.Name = "btnCalculate";
            this.btnCalculate.Size = new System.Drawing.Size(11, 13);
            this.btnCalculate.TabIndex = 20;
            this.toolTip1.SetToolTip(this.btnCalculate, "Click here to generate information about the selected data sets.");
            this.btnCalculate.UseVisualStyleBackColor = true;
            this.btnCalculate.Click += new System.EventHandler(this.btnCalculate_Click);
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(867, 693);
            this.Controls.Add(this.btnCalculate);
            this.Controls.Add(this.lblSelectionInfo);
            this.Controls.Add(this.btnGoups);
            this.Controls.Add(this.chkHide);
            this.Controls.Add(this.chkSort);
            this.Controls.Add(this.chkFilter);
            this.Controls.Add(this.btnCopyCsv);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSaveCsv);
            this.Controls.Add(this.txtCsv);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtHide);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtSort);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtFilter);
            this.Controls.Add(this.lblRows);
            this.Name = "frmMain";
            this.Text = "Weka Evaluation Analyzer";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.mnuDataset.ResumeLayout(false);
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            this.splitContainer1.Panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chrtPlot)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListView lstDatasets;
        private System.Windows.Forms.TextBox txtFilter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSort;
        private System.Windows.Forms.DataVisualization.Charting.Chart chrtPlot;
        private System.Windows.Forms.TextBox txtPlotColumns;
        private System.Windows.Forms.CheckBox chkSelected;
        private System.Windows.Forms.Button btnPlot;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtHide;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label lblRows;
        private System.Windows.Forms.Button btnPickSeries;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnLoadPlot;
        private System.Windows.Forms.Button btnExportPlot;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtCsv;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.Button btnCopyCsv;
        private System.Windows.Forms.Button btnSaveCsv;
        private System.Windows.Forms.CheckBox chkFilter;
        private System.Windows.Forms.CheckBox chkSort;
        private System.Windows.Forms.CheckBox chkHide;
        private System.Windows.Forms.ContextMenuStrip mnuDataset;
        private System.Windows.Forms.ToolStripMenuItem copyDatasetsToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyEvaluationsToToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem copyAllFilesToToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem reloadDataSetsToolStripMenuItem;
        private System.Windows.Forms.Button btnGoups;
        private System.Windows.Forms.Label lblSelectionInfo;
        private System.Windows.Forms.Button btnCalculate;
    }
}

