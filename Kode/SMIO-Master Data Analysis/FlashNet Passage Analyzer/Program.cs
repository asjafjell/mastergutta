﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.Passage;
using Utilities.AtB.Utilities;
using Utilities.Extensions;

namespace FlashNet_Passage_Analyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            var x = PassageStatistics.LoadPeriod(new DateTime(2013, 08, 01), new DateTime(2013, 09, 30));
            Console.Write(x);
            Console.ReadLine();
            return;

            var path = Data.GetDataFolder(Data.DataFolders.PassageAtBusStops);

            var files = System.IO.Directory.GetFiles(path, "*.csv");
            var c = 0;
            foreach (var file in files)
                Console.WriteLine(("[" + c++ + "]").Expand(files.Count().ToString().Length + 3) + System.IO.Path.GetFileName(file));

            do
            {
                Console.WriteLine("Choose file by entering a 0-indexed integer from 0 to {0}, or 'exit':", (files.Length - 1));

                var selection = -1;
                while (selection == -1)
                {
                    var input = Console.ReadLine();
                    if (input == "exit" || input == "") return;
                    if (!int.TryParse(input, out selection) || selection < 0 || selection > files.Length - 1)
                    {
                        Console.WriteLine("'{0}' is not a valid value.", input);
                        selection = -1;
                    }
                }

                var stats = PassageStatistics.FromFile(files[selection]);

                Console.Write(stats);
                Console.WriteLine("");
            } while (true);
        }
    }
}
