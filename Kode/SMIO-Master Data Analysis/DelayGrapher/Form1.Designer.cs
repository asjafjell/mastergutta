﻿using System.Drawing;

namespace DelayGrapher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea2 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend2 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            this.chart = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.progressBar = new System.Windows.Forms.ProgressBar();
            this.lswCalculations = new System.Windows.Forms.ListView();
            this.lsbToBusStop = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lsbFromBusStop = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtBusStopFrom = new System.Windows.Forms.TextBox();
            this.cmbCalculationChooser = new System.Windows.Forms.ComboBox();
            this.myDateTimePicker = new DelayGrapher.MyDateTimePicker();
            this.btnShow = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.chart)).BeginInit();
            this.SuspendLayout();
            // 
            // chart
            // 
            this.chart.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            chartArea2.Name = "ChartArea1";
            this.chart.ChartAreas.Add(chartArea2);
            legend2.Name = "Legend1";
            this.chart.Legends.Add(legend2);
            this.chart.Location = new System.Drawing.Point(358, 177);
            this.chart.Name = "chart";
            this.chart.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Berry;
            this.chart.Size = new System.Drawing.Size(901, 660);
            this.chart.TabIndex = 2;
            this.chart.Text = "chart1";
            // 
            // progressBar
            // 
            this.progressBar.Location = new System.Drawing.Point(12, 622);
            this.progressBar.Name = "progressBar";
            this.progressBar.Size = new System.Drawing.Size(337, 23);
            this.progressBar.TabIndex = 16;
            // 
            // lswCalculations
            // 
            this.lswCalculations.Location = new System.Drawing.Point(742, 37);
            this.lswCalculations.Name = "lswCalculations";
            this.lswCalculations.Size = new System.Drawing.Size(259, 134);
            this.lswCalculations.TabIndex = 17;
            this.lswCalculations.UseCompatibleStateImageBehavior = false;
            this.lswCalculations.View = System.Windows.Forms.View.List;
            // 
            // lsbToBusStop
            // 
            this.lsbToBusStop.FormattingEnabled = true;
            this.lsbToBusStop.Location = new System.Drawing.Point(568, 37);
            this.lsbToBusStop.Name = "lsbToBusStop";
            this.lsbToBusStop.Size = new System.Drawing.Size(158, 134);
            this.lsbToBusStop.TabIndex = 24;
            this.lsbToBusStop.SelectedIndexChanged += new System.EventHandler(this.toListBox_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(568, 15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(46, 13);
            this.label2.TabIndex = 23;
            this.label2.Text = "To stop:";
            // 
            // lsbFromBusStop
            // 
            this.lsbFromBusStop.FormattingEnabled = true;
            this.lsbFromBusStop.Location = new System.Drawing.Point(358, 37);
            this.lsbFromBusStop.Name = "lsbFromBusStop";
            this.lsbFromBusStop.Size = new System.Drawing.Size(204, 134);
            this.lsbFromBusStop.TabIndex = 22;
            this.lsbFromBusStop.SelectedIndexChanged += new System.EventHandler(this.fromListBox_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(355, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 21;
            this.label1.Text = "From stop:";
            // 
            // txtBusStopFrom
            // 
            this.txtBusStopFrom.Location = new System.Drawing.Point(417, 12);
            this.txtBusStopFrom.Name = "txtBusStopFrom";
            this.txtBusStopFrom.Size = new System.Drawing.Size(145, 20);
            this.txtBusStopFrom.TabIndex = 20;
            this.txtBusStopFrom.TextChanged += new System.EventHandler(this.textBoxFrom_TextChanged);
            // 
            // cmbCalculationChooser
            // 
            this.cmbCalculationChooser.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCalculationChooser.FormattingEnabled = true;
            this.cmbCalculationChooser.Location = new System.Drawing.Point(742, 11);
            this.cmbCalculationChooser.Name = "cmbCalculationChooser";
            this.cmbCalculationChooser.Size = new System.Drawing.Size(259, 21);
            this.cmbCalculationChooser.TabIndex = 25;
            this.cmbCalculationChooser.SelectedIndexChanged += new System.EventHandler(this.cmbCalculationChooser_SelectedIndexChanged);
            // 
            // myDateTimePicker
            // 
            this.myDateTimePicker.Location = new System.Drawing.Point(9, 15);
            this.myDateTimePicker.Name = "myDateTimePicker";
            this.myDateTimePicker.Size = new System.Drawing.Size(340, 541);
            this.myDateTimePicker.TabIndex = 26;
            // 
            // btnShow
            // 
            this.btnShow.Location = new System.Drawing.Point(274, 562);
            this.btnShow.Name = "btnShow";
            this.btnShow.Size = new System.Drawing.Size(75, 23);
            this.btnShow.TabIndex = 27;
            this.btnShow.Text = "Show";
            this.btnShow.UseVisualStyleBackColor = true;
            this.btnShow.Click += new System.EventHandler(this.btnShowSelection_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1296, 900);
            this.Controls.Add(this.btnShow);
            this.Controls.Add(this.myDateTimePicker);
            this.Controls.Add(this.cmbCalculationChooser);
            this.Controls.Add(this.lsbToBusStop);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lsbFromBusStop);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtBusStopFrom);
            this.Controls.Add(this.lswCalculations);
            this.Controls.Add(this.progressBar);
            this.Controls.Add(this.chart);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.chart)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart chart;
        private System.Windows.Forms.ProgressBar progressBar;

        private Color[] colors = new Color[]
        {
            Color.Goldenrod,
            Color.YellowGreen,
            Color.DimGray,
            Color.MediumAquamarine, 
            Color.MediumOrchid,
            Color.MediumSeaGreen,
            Color.SeaGreen,
            Color.BlueViolet,
            Color.DarkCyan,
            Color.SlateBlue,
            Color.SlateGray,
            Color.DarkKhaki,
            Color.Turquoise,
            Color.Violet,
            Color.CadetBlue
        };
        private System.Windows.Forms.ListView lswCalculations;
        private System.Windows.Forms.ListBox lsbToBusStop;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListBox lsbFromBusStop;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtBusStopFrom;
        private System.Windows.Forms.ComboBox cmbCalculationChooser;
        private MyDateTimePicker myDateTimePicker;
        private System.Windows.Forms.Button btnShow;
    }
}

