﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace DelayGrapher
{
    public partial class MyDateTimePicker : UserControl
    {
        public MyDateTimePicker()
        {
            InitializeComponent();
            InitializeComboxes();
            LoadSettings();
        }

        private void InitializeComboxes()
        {
            DateTime start = new DateTime(2013, 1, 1, 5, 0, 0);

            while (start.ToString("HH") != "00")
            {
                string str = start.ToShortTimeString();
                cmbStartHour.Items.Add(str);
                cmbEndHour.Items.Add(str);
                start = start.AddHours(1);
            }

            monthCalendar.MaxDate = DateTime.Today;
        }

        public List<int> SelectedDays()
        {
            List<int> selected = new List<int>();
            if (chkMonday.Checked)
                selected.Add(1);
            if (chkTuesday.Checked)
                selected.Add(2);
            if (chkWednesday.Checked)
                selected.Add(3);
            if (chkThursday.Checked)
                selected.Add(4);
            if (chkFriday.Checked)
                selected.Add(5);
            if (chkSaturday.Checked)
                selected.Add(6);
            if (chkSunday.Checked)
                selected.Add(0);

            return selected;
        }

        public IEnumerable<DateTime> SelectedDates
        {
            get
            {

                List<DateTime> dates;
                if (lswDaysSelected.Items.Count > 1)
                {
                    dates =
                        (from ListViewItem dateString in lswDaysSelected.Items
                            select Convert.ToDateTime(dateString.Text)).ToList();
                }
                else
                {
                    var startDate = monthCalendar.SelectionStart;
                    var endDate = monthCalendar.SelectionEnd;
                    int days = (endDate - startDate).Days + 1; // incl. endDate 

                    dates = Enumerable.Range(0, days)
                        .Select(i => startDate.AddDays(i))
                        .ToList();

                }
                return dates;
            }
        }

        public int StartHour
        {
            get
            {
                return Convert.ToDateTime(cmbStartHour.Text).Hour;
            }
        }

        public int EndHour
        {
            get
            {
                return Convert.ToDateTime(cmbEndHour.Text).Hour;
            }
        }

        public void SaveSettings(object sender, EventArgs e)
        {
            Properties.Settings settings = Properties.Settings.Default;

            settings.StartDate = monthCalendar.SelectionStart.ToLongDateString();
            settings.EndDate = monthCalendar.SelectionEnd.ToLongDateString();

            settings.StartHourIndex = cmbStartHour.SelectedIndex;
            settings.EndHourIndex = cmbEndHour.SelectedIndex;

            settings.Monday = chkMonday.Checked;
            settings.Tuesday = chkTuesday.Checked;
            settings.Wednesday = chkWednesday.Checked;
            settings.Thursday = chkThursday.Checked;
            settings.Friday = chkFriday.Checked;
            settings.Saturday = chkSaturday.Checked;
            settings.Sunday = chkSunday.Checked;

            settings.Save();

        }

        private void LoadSettings()
        {
            Properties.Settings settings = Properties.Settings.Default;

            monthCalendar.SelectionStart = Convert.ToDateTime(settings.StartDate);
            monthCalendar.SelectionEnd = Convert.ToDateTime(settings.EndDate);

            cmbStartHour.SelectedIndex = Properties.Settings.Default.StartHourIndex;
            cmbEndHour.SelectedIndex = Properties.Settings.Default.EndHourIndex;

            chkMonday.Checked = settings.Monday;
            chkTuesday.Checked = settings.Tuesday;
            chkWednesday.Checked = settings.Wednesday;
            chkThursday.Checked = settings.Thursday;
            chkFriday.Checked = settings.Friday;
            chkSaturday.Checked = settings.Saturday;
            chkSunday.Checked = settings.Sunday;
        }

        
        private void monthCalendar_DateSelected(object sender, DateRangeEventArgs e)
        {
            if (monthCalendar.SelectionStart.DayOfYear == monthCalendar.SelectionEnd.DayOfYear)
            {
                lswDaysSelected.Items.Add(e.Start.ToShortDateString());
            }
        }

        private void btnClearDatesSelected_Click(object sender, EventArgs e)
        {
            lswDaysSelected.Items.Clear();
        }
    }




}
