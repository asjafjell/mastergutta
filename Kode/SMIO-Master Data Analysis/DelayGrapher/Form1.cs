﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;
using Utilities.AtB.LinksTravelTime;
using Utilities.Database;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using Utilities.AtB.General;
using Utilities.Extensions;

namespace DelayGrapher
{

    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            InitializeComboboxes();
            LoadSettings();
        }

        private BusStop _busStopFrom;
        private BusStop _busStopTo;

        private class CalculationObject 
        {
            public CalculationTypes CalculationType { get; set; }
            public string Text { get; set; }
            public override string ToString()
            {
                return Text;
            }
        }

        private enum CalculationTypes {CorrelationWeatherDelay, CorrelationDayDay}

        readonly Dictionary<CalculationTypes, List<ListViewItem>> _calculations = new Dictionary<CalculationTypes, List<ListViewItem>>();

        private void InitializeComboboxes()
        {
            cmbCalculationChooser.Items.Add(new CalculationObject(){CalculationType = CalculationTypes.CorrelationDayDay, Text = "Korrelasjon - Dag + Dag"});
            cmbCalculationChooser.Items.Add(new CalculationObject(){CalculationType = CalculationTypes.CorrelationWeatherDelay, Text = "Korrelasjon - Vær + Data"});
            cmbCalculationChooser.SelectedIndex = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            chart.ChartAreas.RemoveAt(0);
            chart.ChartAreas.Add("TravelTimeChartArea");

            chart.Series.Add("TravelTimeByHour");
            chart.Series["TravelTimeByHour"].ChartArea = "TravelTimeChartArea";
            chart.ChartAreas["TravelTimeChartArea"].AxisX.IntervalAutoMode = IntervalAutoMode.VariableCount;
            chart.ChartAreas["TravelTimeChartArea"].AxisY.IntervalAutoMode = IntervalAutoMode.VariableCount;

            chart.Series.Add("Temperature");
            chart.Series.Add("Precipitation");
            chart.Series.Add("Wind Extreme");
            chart.Series["Temperature"].ChartArea = "TravelTimeChartArea";
            chart.Series["Precipitation"].ChartArea = "TravelTimeChartArea";
            chart.Series["Wind Extreme"].ChartArea = "TravelTimeChartArea";

            chart.Series["Temperature"].ChartType = SeriesChartType.Line;
            chart.Series["Precipitation"].ChartType = SeriesChartType.Line;
            chart.Series["Wind Extreme"].ChartType = SeriesChartType.Line;
        }

        private void GraphDelaysBetweenBusStops(string busStopFromId, string busStopToId)
        {
            progressBar.Value = 15;
           
            var db = new SMIODataContext();
            var data = db.LTTItems.
                Where(p => p.BusStopFrom.ID == busStopFromId && p.BusStopTo.ID == busStopToId).ToList().
                Where(p => p.ActualDeparture.Hour >= myDateTimePicker.StartHour && p.ActualDeparture.Hour <= myDateTimePicker.EndHour).
                Where(p => myDateTimePicker.SelectedDays().Contains((int) p.ActualDeparture.DayOfWeek)).ToList();

            //Take data from list of dates or from monthCalendar
            IEnumerable<LTTItem> dataByDate;
            if (myDateTimePicker.SelectedDates.Count() > 1)
            {
                dataByDate = data.Where(p => myDateTimePicker.SelectedDates .Contains(p.ActualDeparture.Date));
            }
            else
            {
                dataByDate = data.Where(p => myDateTimePicker.SelectedDates.Contains(p.ActualDeparture.Date));
            }

            List<PeriodGroup<LTTItem>>  groupsByHourOfDay = dataByDate.GroupBy(p => new PeriodGroup<LTTItem>(p.ActualDeparture).Id)
                .Select(p => new PeriodGroup<LTTItem>(p.First().ActualDeparture, p)).OrderBy(p => p.Time.Date).ThenBy(p => p.Time.Hour).ToList();
                
            progressBar.Value = 35;
            
            if (!groupsByHourOfDay.Any()){
                return;
            }
 
            /* STD
             * Graphs the STD for each of the dates*/

            GraphStandardDeviationBars(groupsByHourOfDay);

            /* CORRELATION
            *  Calculates correlation between dates for std */

            if (!CalculateCorrelationBetweenDays(groupsByHourOfDay)) return;
            /* CORRELATION
             * Calculates correlation between weather and std */
            CalculateCorrelationWeatherDelay(groupsByHourOfDay);

            progressBar.Value = 100;
        }

        private void CalculateCorrelationWeatherDelay(List<PeriodGroup<LTTItem>> groupsByHourOfDay)
        {
            var delayArray = groupsByHourOfDay.Select(p => p.Average()).ToArray();
            SMIODataContext database = new SMIODataContext();
            var weatherMonth = database.WeatherObjects.ToList().
                Where(
                    w => myDateTimePicker.SelectedDates.Contains(w.Time.Date)).
                Where(
                    w =>
                        w.Time.Hour >= myDateTimePicker.StartHour &&
                        w.Time.Hour <= myDateTimePicker.EndHour).
                OrderBy(p => p .Time.Date).ThenBy(p => p.Time.Hour);

            var tempArr = weatherMonth.Select(p => p.Temp).ToArray();
            var precipArr = weatherMonth.Select(p => p.Precipitation).ToArray();
            var windArr = weatherMonth.Select(p => p.Wind).ToArray();

            var tempCorr = Correlation(delayArray, tempArr.Select(Convert.ToDouble).ToArray());
            var precipCorr = Correlation(delayArray, precipArr.Select(Convert.ToDouble).ToArray());
            var windCorr = Correlation(delayArray, windArr.Select(Convert.ToDouble).ToArray());

            _calculations.Clear();
            _calculations.Add(CalculationTypes.CorrelationWeatherDelay, new List<ListViewItem>()
            {
                new ListViewItem("Temp + Delay " + tempCorr),
                new ListViewItem("Precip + Delay " + precipCorr),
                new ListViewItem("Wind + Delay " + windCorr),
                
            });
        }

        private bool CalculateCorrelationBetweenDays(List<PeriodGroup<LTTItem>> groupsByHourOfDay)
        {
            var byDay = groupsByHourOfDay.
                GroupBy(p => p.Time.Date).
                Select(p => new
                {
                    Date = p.Key,
                    Stds = p.Select(c => c.StandardDeviation().Value).ToArray()
                });


             if (groupsByHourOfDay.Count() <= 1) return false;

            List<ListViewItem> correlationDayDay = new List<ListViewItem>();

            for (int i = 0; i < byDay.Count(); i++)
            {
                for (int k = i + 1; k < byDay.Count(); k++)
                {
                    if (k >= byDay.Count()) continue;

                    var outer = byDay.ElementAt(i);
                    var inner = byDay.ElementAt(k);

                    double correlation = Correlation(inner.Stds, outer.Stds);
                    ListViewItem item = new ListViewItem(String.Format("{0} {1} {2}", outer.Date.ToShortDateString(), inner.Date.ToShortDateString(), correlation.ToString("N2")));
                    correlationDayDay.Add(item);
                }
            }

            _calculations.Remove(CalculationTypes.CorrelationDayDay);
            _calculations.Add(CalculationTypes.CorrelationDayDay, correlationDayDay);
            return true;
        }

        private void GraphStandardDeviationBars(List<PeriodGroup<LTTItem>> groupsByHourOfDay)
        {
            int xCount = 0;
            foreach (var group in groupsByHourOfDay)
            {
                chart.Series["TravelTimeByHour"].Points.AddXY(@group.Id, @group.StandardDeviation().Value);
                chart.Series["TravelTimeByHour"].Points[xCount].Color = ColorFromInt(@group.Time.Day + @group.Time.Month);
                xCount++;
            }
        }


        private void GraphWeatherForInterval()
        {
            SMIODataContext database = new SMIODataContext();
            var weatherMonth = database.WeatherObjects.ToList().
                 Where(
                     w => myDateTimePicker.SelectedDates.Contains(w.Time.Date)).
                 Where(
                     w =>
                         w.Time.Hour >= myDateTimePicker.StartHour &&
                         w.Time.Hour <= myDateTimePicker.EndHour).
                Where(w => 
                         myDateTimePicker.SelectedDays().Contains((int)w.Time.DayOfWeek)).
                         OrderBy(p => p.Time.Date).ThenBy(p => p.Time.Hour);

            foreach (var item in weatherMonth)
            {
                var x = String.Format("{0} kl {1}", item.Time.ToShortDateString(), item.Time.Hour);
                chart.Series["Temperature"].Points.AddXY(x, item.TempMin);
                chart.Series["Precipitation"].Points.AddXY(x, item.Precipitation);
                chart.Series["Wind Extreme"].Points.AddXY(x, item.WindExtreme);
            }
        }
        
        private Color ColorFromInt(int i)
        {
            int num = i % 15;
            return colors[num];
        }

        private void btnShowSelection_Click(object sender, EventArgs e)
        {
            myDateTimePicker.SaveSettings(sender, e);
            lswCalculations.Items.Clear();
            progressBar.Value = 4;
            SaveSettings(null,null);
            
            if (_busStopFrom != null && _busStopTo != null)
            {
                chart.Series["TravelTimeByHour"].Points.Clear();
                chart.Series["Temperature"].Points.Clear();
                chart.Series["Precipitation"].Points.Clear();
                chart.Series["Wind Extreme"].Points.Clear();

                GraphDelaysBetweenBusStops(_busStopFrom.ID, _busStopTo.ID);
            }
            
            GraphWeatherForInterval();

        }

        private void SaveSettings(object sender, EventArgs e)
        {
            Properties.Settings settings = Properties.Settings.Default;
            
            if (txtBusStopFrom.Text.Trim().Count() != 0 && lsbFromBusStop.Items.Count != 0)
            {
                settings.BusStopFromId = ((BusStop) lsbFromBusStop.SelectedItem).ID;
                if (lsbToBusStop.Items.Count != 0)
                {
                    settings.BusStopToId = ((BusStop) lsbToBusStop.SelectedItem).ID;
                }
            }           

            settings.Save();
        }

        private void LoadSettings()
        {
            Properties.Settings settings = Properties.Settings.Default;
            
            SMIODataContext db = new SMIODataContext();
            _busStopFrom = db.BusStops.FirstOrDefault(p => p.ID == settings.BusStopFromId);
            _busStopTo = db.BusStops.FirstOrDefault(p => p.ID == settings.BusStopToId);

            txtBusStopFrom.Text = _busStopFrom.Name;
            textBoxFrom_TextChanged(null, null);


            /* FETCH BUS STOP SETTINGS
             * Fetch and select bus stops from settings. */
            for (int i = 0; i < lsbFromBusStop.Items.Count; i++)
            {
                var item = (BusStop) lsbFromBusStop.Items[i];
                if (item.ID == _busStopFrom.ID)
                    lsbFromBusStop.SelectedIndex = i;
            }

            lsbBusStopFrom_UpdateElements();

            for (int i = 0; i < lsbToBusStop.Items.Count; i++)
            {
                var item = (BusStop) lsbToBusStop.Items[i];
                if (item.ID == _busStopTo.ID)
                    lsbToBusStop.SelectedIndex = i;
            }
            
            lsbToBusStop.SelectedItem = _busStopTo;
        }

        public double Correlation(double[] array1, double[] array2)
        {
            double[] array_xy = new double[array1.Length];
            double[] array_xp2 = new double[array1.Length];
            double[] array_yp2 = new double[array1.Length];
            for (int i = 0; i < array1.Length; i++)
                array_xy[i] = array1[i] * array2[i];
            for (int i = 0; i < array1.Length; i++)
                array_xp2[i] = Math.Pow(array1[i], 2.0);
            for (int i = 0; i < array1.Length; i++)
                array_yp2[i] = Math.Pow(array2[i], 2.0);
            double sum_x = array1.Sum();
            double sum_y = array2.Sum();
            double sum_xy = array_xy.Sum();
            double sum_xpow2 = array_xp2.Sum();
            double sum_ypow2 = array_yp2.Sum();
            double Ex2 = Math.Pow(sum_x, 2.00);
            double Ey2 = Math.Pow(sum_y, 2.00);

            return (array1.Length * sum_xy - sum_x * sum_y) /
            Math.Sqrt((array1.Length * sum_xpow2 - Ex2) * (array1.Length * sum_ypow2 - Ey2));
        }

       private void textBoxFrom_TextChanged(object sender, EventArgs e)
        {
            var database = new SMIODataContext();
            lsbFromBusStop.Items.Clear();
            lsbToBusStop.Items.Clear();
            string requestString = txtBusStopFrom.Text.ToLower();
            List<BusStop> stops = database.BusStops.Where(s => s.Name.ToLower().StartsWith(requestString)).ToList();
            foreach (var stop in stops)
            {
                stop.Name += stop.ID.Substring(4, 1) == "0" ? " fra byen" : " til byen";
                lsbFromBusStop.Items.Add(stop);
            }
        }
        
        private void fromListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            //lsbBusStopFrom_UpdateElements();

            
        }

        private void lsbBusStopFrom_UpdateElements()
        {
            var database = new SMIODataContext();
            lsbToBusStop.Items.Clear();
            _busStopFrom = (BusStop)lsbFromBusStop.SelectedItem;
            var query = database.LTTItems.
                        Where(t => t.BusStopFrom.ID == _busStopFrom.ID).ToList().
                        Where(t => myDateTimePicker.SelectedDates.Contains(t.ActualArrival.Date)).
                        Select(t => t.BusStopTo).
                        Distinct().ToList();

            foreach (var stop in query)
            {
                lsbToBusStop.Items.Add(stop);
            }
        }


        private void toListBox_SelectedIndexChanged(object sender, EventArgs e)
        {
           _busStopTo = (BusStop)lsbToBusStop.SelectedItem;
           
        }

        private void cmbCalculationChooser_SelectedIndexChanged(object sender, EventArgs e)
        {
            lswCalculations.Clear();
            var selected = (CalculationObject) cmbCalculationChooser.SelectedItem;
            var defaultList = new List<ListViewItem>(); 
            defaultList.Add(new ListViewItem("No items. Do a search!"));

            switch (selected.CalculationType)
            {
                case CalculationTypes.CorrelationWeatherDelay:
                    lswCalculations.Items.AddRange(_calculations.GetValueOrDefault(CalculationTypes.CorrelationWeatherDelay, defaultList).ToArray());
                    break;
                case CalculationTypes.CorrelationDayDay:
                    lswCalculations.Items.AddRange(_calculations.GetValueOrDefault(CalculationTypes.CorrelationDayDay, defaultList).ToArray());
                    break;
                
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

    }
    
}
