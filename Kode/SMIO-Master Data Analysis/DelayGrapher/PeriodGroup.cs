﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Meta.Numerics;
using Meta.Numerics.Statistics;
using Utilities.AtB.LinksTravelTime;

namespace DelayGrapher
{
    public class PeriodGroup<T>
    {
        private readonly IEnumerable<T> _children;
        private DateTime _time;
        public PeriodGroup(DateTime time)
        {
            _time = time;
        }

        public PeriodGroup(DateTime time, IEnumerable<T> children)
            : this(time)
        {
            _children = children;
        }

        public string Id
        {
            get { return String.Format("{0} kl {1}", _time.ToShortDateString(), _time.Hour); }
        }

        public override string ToString()
        {
            return String.Format("{0} at {1} with {2} children.", _time.Date.ToShortDateString(), _time.Hour, _children.Count());
        }

        public DateTime Time { get { return _time; } }

        public double Average()
        {
            if (_children is IEnumerable<LTTItem>)
            {
                return ((IEnumerable<LTTItem>) _children).Average(c => c.TotalTravelTime);
            }
            
            if (_children is IEnumerable<PeriodGroup<LTTItem>>)
            {
                IEnumerable<PeriodGroup<LTTItem>> groupForDate = ((IEnumerable<PeriodGroup<LTTItem>>) _children);

                double totalTravelTime = 0;
                double count = 0;
                foreach (PeriodGroup<LTTItem> periodGroup in groupForDate)
                {
                    totalTravelTime += Convert.ToDouble(periodGroup.Children.Sum(c => c.TotalTravelTime));
                    count += Children.Count();
                }

                return totalTravelTime/count;
            }

            throw new InvalidCastException("Unable to calculate average because of invalid <T> class type.");

        }

        public UncertainValue StandardDeviation()
        {
             if (_children is IEnumerable<LTTItem>)
             {
                 return StandardDeviation((IEnumerable<LTTItem>)_children);
             }
            
            if (_children is IEnumerable<PeriodGroup<LTTItem>>)
            {
                List<LTTItem> allItems = new List<LTTItem>();

                foreach (PeriodGroup<LTTItem> child in (IEnumerable<PeriodGroup<LTTItem>>)_children)
                {
                    allItems.AddRange(child.Children);
                }
                return StandardDeviation(allItems);
            }

            throw new InvalidCastException("Unable to calculate average because of invalid <T> class type.");
        }

        private UncertainValue StandardDeviation(IEnumerable<LTTItem> items)
        {
            return items.Count() > 1
                     ? new Sample(
                         items.Select(p => Convert.ToDouble(p.TotalTravelTime)).ToArray()).PopulationStandardDeviation
                     : new UncertainValue(-2, 0);
        }

        public IEnumerable<T> Children
        {
            get { return _children; }
        }


    }

}
