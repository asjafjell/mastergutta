﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Utilities.AtB.Utilities;
using Utilities.Classifiers;
using Utilities.PowerSet;
using Utilities.Extensions;

namespace WekaLearning
{
    internal class Program
    {
        private static readonly String[] representativeSelection = { "004", "064", "124", "244", "304", "364", "378", "406", "427", "456", "469", "487", "507", "530", "550", "569", "593", "595", "608", "624" };

        public static void Main(string[] args)
        {
            //PowerSetResultSet A = new PowerSetResultSet(@"C:\Master\RepoDataSync\weka\parameterExperimentation\Results\03-28 UtenActualArrival, skikkelig\Ann skikkelig", "608", 20);
            //PowerSetResultSet B = new PowerSetResultSet(@"C:\Master\RepoDataSync\weka\parameterExperimentation\Results\04-02 Ann Parameteroptimalisering", "608", 20);

            //string regular = @"C:\Master\RepoDataSync\weka\parameterExperimentation\Results\03-28 UtenActualArrival, skikkelig\Ann skikkelig";
            //string optimized = @"C:\Master\RepoDataSync\weka\parameterExperimentation\Results\04-02 Ann Parameteroptimalisering";

            //var results = AllResultSets(regular).SelectMany(set => set.WorstAttributes(25)).GroupBy(k => k.Key).Select(elem => new
            //{
            //    name = elem.Key,
            //    count = elem.Sum(p => p.Value)
            //});

            //var resultsOpt = AllResultSets(optimized).SelectMany(set => set.WorstAttributes(25)).GroupBy(k => k.Key).Select(elem => new
            //{
            //    name = elem.Key,
            //    count = elem.Sum(p => p.Value)
            //});


            //EvaluationAnalyzer.AnnLearningrateAndMomentumResultsToCsv(EvaluationAnalyzer.AnnHiddenNodesFile);

            //string folder = @"C:\Users\Aleksander\Desktop\svmcurrrest";

            //var files = Directory.GetFiles(folder);

            //foreach (var file in files)
            //{
            //    var newfile = Path.Combine(folder, Path.GetFileNameWithoutExtension(file)) + ".job";
            //    File.Move(file, newfile);
            //}


            //EvaluationAnalyzer.AnnNodeResultsToCsv(EvaluationAnalyzer.AnnHiddenNodesFile);
            //var files = new List<string>()
            //{
            //    Data.GetWekaFile("parameterExperimentation", "Results", "04-09 Amazon", "Aleksander", "bestAttribute_Aleksander.txt"),
            //    Data.GetWekaFile("parameterExperimentation", "Results", "04-09 Amazon", "Server 1", "bestAttribute_Amazon.txt"),
            //    Data.GetWekaFile("parameterExperimentation", "Results", "04-09 Amazon", "Server 2", "bestAttribute_Amazon.txt")
            //};

            //var powerSetFile = Data.GetWekaFile("parameterExperimentation", "Results", "04-09 Amazon", "Missing Processed", "624powerset.txt");
            //var target = Data.GetWekaFile("parameterExperimentation", "Results", "04-09 Amazon", "Missing Processed", "Missing.txt");

            ////Les inn fasiten, sorter hver linje alfabetisk.
            //var all = File.ReadAllLines(powerSetFile, Encoding.Default).Select(p => string.Join(",", p.Split(',').OrderBy(c => c)));
            
            ////Les inn resultatene fra alle filene, split i blokker, hent ut datasett-linjen fra hver blokk, og sorter denne alfabetisk.
            //var res = files.SelectMany(p => File.ReadAllText(p, Encoding.Default).Replace("\r\n", "\n").Split("\n\n").Where(c => !string.IsNullOrEmpty(c)).Select(c => c.TrimStart(new[] {'\n'}).Split("\n")[2]).Select(c => c == "Empty" ? "" : c)).Select(p => string.Join(",", p.Split(';').OrderBy(c => c)));
            
            ////Lagre enn fil med alle datasettene som finnes i 'all', men ikke i 'res'.
            //File.WriteAllText(target, string.Join(Environment.NewLine, all.Except(res)), Encoding.Default);

            //var v = PowerSetDiffer.NotProcessedSets(files,powerSetFile);
            //File.WriteAllLines(@"C:\Master\RepoDataSync\weka\parameterExperimentation\Results\04-09 Amazon\NotProcessedSets.txt", v.Select(list => String.Join(",",list)));

            //EvaluationAnalyzer.AnnNodeResultsToCsv(EvaluationAnalyzer.AnnHiddenNodesFile2);
            EvaluationAnalyzer.AnnLearningrateAndMomentumResultsToCsv(EvaluationAnalyzer.AnnLearningRateMomentumFile2);
        }

        public static IEnumerable<PowerSetResultSet> AllResultSets(string path)
        {
            return representativeSelection.Select(s => new PowerSetResultSet(path, s, 20));
        }

        public static double MaeAverage(string path)
        {
            double sumBestError = representativeSelection.Sum(s => new PowerSetResultSet(path, s, 20).BestMeanAbsoluteError());
            return sumBestError / representativeSelection.Length;
        }

    }


}
