﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MetInst_Analyzer
{
    class Program
    {
        static void Main(string[] args)
        {
            var file = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "metinst.csv");
            var start = DateTime.Now;

            var xml = XElement.Load("http://api.met.no/weatherapi/locationforecast/1.8/?lat=60.390494;lon=5.329657").Element("product");

            var csv = xml.Elements("time").
                          Where(p => p.Element("location").Elements("precipitation").Any()).
                          Select(p => new
                              {
                                  From = Date(p.Attribute("from")),
                                  To = Date(p.Attribute("to")),
                                  Precipitation = p.Element("location").Element("precipitation")
                              }).
                          Where(p => p.To.Subtract(p.From).TotalMinutes == 60).
                          Select(p => new
                              {
                                  p.From,
                                  p.To,
                                  Rain = p.Precipitation.Attribute("value").Value,
                                  RainMin = (p.Precipitation.Attribute("minvalue") ?? new XAttribute("minvalue", "-")).Value,
                                  RainMax = (p.Precipitation.Attribute("maxvalue") ?? new XAttribute("maxvalue", "-")).Value
                              }).
                          OrderBy(p => p.From).ThenBy(p => p.To).
                          Select(time => string.Format("'{0}','{1}','{2}','{3}','{4}'", time.From, time.To, time.Rain, time.RainMin, time.RainMax));

            System.IO.File.WriteAllLines(file, csv, Encoding.Default);

            Console.WriteLine("Fetched, written to " + file + " in " + DateTime.Now.Subtract(start).TotalMilliseconds + "ms.");

            Console.ReadKey();
        }

        static DateTime Date(XAttribute date)
        {
            return DateTime.ParseExact(date.Value, "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'", CultureInfo.InvariantCulture).AddHours(2);
        }
    }
}
