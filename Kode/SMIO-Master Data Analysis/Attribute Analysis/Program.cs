﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using Utilities.AtB.Tickets;
using Utilities.AtB.Utilities;
using Utilities.Classifiers;
using Utilities.Extensions;
using Utilities.PowerSet;

namespace Attribute_Analysis
{
    class Program
    {
        static void Main(string[] args)
        {
            MethodName();
        }

        public static void MethodName()
        {
            IEnumerable<string> knnFiles = Settings.ChosenDatasets.Select(ds => Path.Combine(Settings.KnnPowerSetFolder, ds) + "resultibk.txt");
            IEnumerable<string> annFiles = Settings.ChosenDatasets.Select(ds => Path.Combine(Settings.AnnPowerSetFolder, ds) + "resultann.txt");
            IEnumerable<string> svmFiles = Settings.ChosenDatasets.Select(ds => Path.Combine(Settings.SvmPowerSetFolder, ds) + "resultsvm.txt");

            //var annRes = Results(knnFiles, false);
            //var knnRes = Results(annFiles, false);
            //var svmRes = Results(svmFiles, true);

            GenerateMiniPowersets();
        }

        /// <summary>
        /// Svm results are special as they have two types of formatting of the results. The first data sets are run in 'Aleksander' format, 
        /// while the last one (624) is run in 'Erlend'-format. 
        /// </summary>
        /// <returns></returns>
        public static IEnumerable<PowerSetResult> Results(IEnumerable<string> files, bool isSvm)
        {
            List<PowerSetResult> results = new List<PowerSetResult>();

            foreach (var file in files)
            {
                if (File.Exists(file))
                {
                    if (file.Contains("624") && isSvm)
                    {
                        IEnumerable<string> tmpResults = File.ReadAllText(file, Encoding.Default).Split(Environment.NewLine.Repeat(2)).Where(p => !string.IsNullOrEmpty(p));
                        results.AddRange(tmpResults.Select(r => new PowerSetResult(r, PowerSetResult.ResultType.Erlend, "Svm", "624")));
                    }
                    else
                    {
                        string fileNr = file.Substring(file.Length - 16, 3);
                        var tmpResults = File.ReadAllLines(file).Select(l => new PowerSetResult(l, PowerSetResult.ResultType.Aleksander, "Svm", fileNr));
                        int count = tmpResults.Count();

                        results.AddRange(tmpResults);
                    }

                }
            }
            return results;
        }

        public static void GenerateMiniPowersets()
        {
            List<string> weatherAttrs = new List<string>() { "Temperature", "Wind", "Humidity", "Precipitation" };
            List<string> footballAttrs = new List<string>() { "SoccerGame" };
            List<string> passengerAttrs = new List<string>() { "PassengersPresent" };
            List<string> ticketAttrs = new List<string>() { "TicketsSoldLastHour", "TicketsSoldLastThreeHours" };

            var datasetSourceFolder = Path.Combine(Data.GetWekaFolder(), "finalDataset");
            
            //var files = Settings.ChosenDatasets.Select(f => datasetSourceFolder + "\\" + f + "train.csv");
            List<string> files = new List<string>();
            string fileIds = null;
            for (int i = 1; i <= 643; i++)
            {
                var file = String.Format("{0}\\{1}train.csv",datasetSourceFolder,i.ToString("000"));
                if (File.Exists(file))
                {
                    files.Add(file);
                    fileIds += i.ToString("000") + ";";
                }
            }

            Debug.WriteLine(fileIds);

            foreach (var file in files)
            {
                var miniPowFolder = Data.GetWekaFile("attributes", "minipowerset");
                var descriptionFolder = Data.GetWekaFile("attributes", "descriptions");
                string fileNr = file.Substring(file.Length - 12, 3);
                var setFile = Path.Combine(miniPowFolder, fileNr + "powerset.txt");
                var descrFile = Path.Combine(descriptionFolder, fileNr + "description.txt");

                List<List<string>> sets = new List<List<string>>();
                List<string> descriptions = new List<string>();

                //All attributes
                List<string> allAttrs = File.ReadLines(file).First().Split(',').ToList();
                sets.Add(allAttrs);
                descriptions.Add("AllAttributes");

                //Base attributes 
                List<string> baseAttrs = new List<string>(allAttrs).Except(weatherAttrs).Except(footballAttrs).Except(passengerAttrs).Except(ticketAttrs).ToList();
                sets.Add(baseAttrs);
                descriptions.Add("BaseAttributes");


                //With weather 
                List<string> withWeather1 = new List<string>(baseAttrs) {weatherAttrs[0]};
                sets.Add(withWeather1);
                descriptions.Add(weatherAttrs[0]);

                List<string> withWeather2 = new List<string>(baseAttrs) {weatherAttrs[1]};
                sets.Add(withWeather2);
                descriptions.Add(weatherAttrs[1]);
                
                List<string> withWeather3 = new List<string>(baseAttrs) {weatherAttrs[2]};
                sets.Add(withWeather3);
                descriptions.Add(weatherAttrs[2]);
                
                List<string> withWeather4 = new List<string>(baseAttrs) {weatherAttrs[3]};
                sets.Add(withWeather4);
                descriptions.Add(weatherAttrs[3]);

                //AllWeather
                List<string> allWeather = new List<string>(baseAttrs);
                allWeather.AddRange(weatherAttrs);
                sets.Add(allWeather);
                descriptions.Add("AllWeather");

                //With football 
                List<string> withFootball = new List<string>(baseAttrs);
                if (allAttrs.Contains(footballAttrs[0]))
                {
                    withFootball.AddRange(footballAttrs);
                    sets.Add(withFootball);
                    descriptions.Add(footballAttrs[0]);

                }

                //With passenger
                List<string> withPassenger = new List<string>(baseAttrs);
                if (allAttrs.Contains(passengerAttrs[0]))
                {
                    withPassenger.AddRange(passengerAttrs);
                    sets.Add(withPassenger);
                    descriptions.Add(passengerAttrs[0]);
                }

                //With tickets 
                if (allAttrs.Contains(ticketAttrs[0]) && allAttrs.Contains(ticketAttrs[1]))
                {
                    List<string> withTickets1 = new List<string>(baseAttrs) {ticketAttrs[0]};
                    sets.Add(withTickets1);
                    descriptions.Add(ticketAttrs[0]);

                    List<string> withTickets2 = new List<string>(baseAttrs) {ticketAttrs[1]};
                    sets.Add(withTickets2);
                    descriptions.Add(ticketAttrs[1]);

                }
                
                //Inverse the sets, as the powerset file should indicate what attributes
                //to remove.
                var inverseSets = sets.Select(s => allAttrs.Except(s)).Select(s => String.Join(",",s));
                File.WriteAllLines(setFile,inverseSets);

                File.WriteAllLines(descrFile, descriptions);
            }
        }
    }

    class Settings
    {
        public static string WorkingFolder = Path.Combine(Data.GetWekaFolder(), "attributes");

        public static string[] ChosenDatasets = { "004", "064", "124", "244", "304", "364", "378", "406", "427", "456", "469", "487", "507", "530", "550", "569", "593", "595", "608", "624" };

        public static string KnnPowerSetFolder = Data.GetWekaFile("parameterExperimentation", "Results",
            "03-28 UtenActualArrival, skikkelig",
            "Knn skikkelig");

        public static string AnnPowerSetFolder = Data.GetWekaFile("parameterExperimentation", "Results",
            "04-07 Ann Combo (1-dagers + utenActualArrival, skikkelig)");

        public static string SvmPowerSetFolder = Data.GetWekaFile("parameterExperimentation", "Results",
            "03-28 UtenActualArrival, skikkelig",
            "Svm skikkelig", "Svm skikkelig - første filer");

    }
}
