﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Net;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Web.Script.Serialization;
using System.Runtime.Serialization;
using Utilities.AtB.Utilities;
using Utilities.Database;
using Utilities.AtB.Lines;
using Utilities.AtB.General;
using Utilities.Extensions;
using System.Xml.Linq;
using System.Device.Location;
using Utilities.Yr;

namespace QueryTester
{

    class TripID
    {
        private string trip { get; set; }

        private DateTime tripDate { get; set; }
        public TripID(string trip, DateTime tripDate)
        {
            this.trip = trip;
            this.tripDate = tripDate;
        }

        public String ToString()
        {
            return this.trip + " " + tripDate;
        }
    }

    class Program
    {
        //hardkodet stoppliste for linje 8 fra kongens gate k2
        
        static List<String> loopsIDs = new List<string>() { "1011", "166", "167", "168", "169", "171", "172", "516", "517", "518", "519", "520", "521", "522", "523", "524", "525", "526", "527", "459", "460", "461" };

        static Dictionary<string, List<Utilities.AtB.Passage.PassageItem>> getValidTrips(string line, List<string> stopsIDs, BusStop fromStop, BusStop toStop, DateTime fromDate, DateTime toDate)
        {
            Dictionary<string, List<Utilities.AtB.Passage.PassageItem>> validTrips = new Dictionary<string, List<Utilities.AtB.Passage.PassageItem>>();
            var db = new SMIODataContext();
            var trips = db.PassageItems
                .Where(p => DbFunctions.DiffDays(fromDate, p.ActualArrival) >= 0)
                .Where(p => DbFunctions.DiffDays(p.ActualArrival, toDate) >= 0)
                .Where(p => p.Line == line)
                .OrderBy(p => p.ActualArrival)
                .ToList()
                .Where(p => stopsIDs.Contains(p.BusStopCode))
                .GroupBy(p => new { ID = p.Trip + "_" + p.ActualArrival.Date });
            
            foreach (var trip in trips)
            {
                int startIndex = stopsIDs.IndexOf(fromStop.ID);
                int stopIndex = stopsIDs.IndexOf(toStop.ID);
                if (trip.Count() >= (stopIndex - startIndex + 1))
                {
                    List<Utilities.AtB.Passage.PassageItem> tripSection = new List<Utilities.AtB.Passage.PassageItem>();
                    for (var i = startIndex; i <= stopIndex; i++)
                    {
                        var passage = trip.ElementAt(i);
                        if (passage.BusStopCode == stopsIDs.ElementAt(i)) tripSection.Add(passage);
                    }
                    if (tripSection.Count() == (stopIndex - startIndex + 1))
                    {
                        string keyString = trip.First().Trip + "_" + trip.First().ActualArrival.Date;
                        validTrips.Add(keyString, tripSection); //contains passage for all intermediate stops
                    }
                }
            }
            return validTrips;
        }

        static void getTravelTimes(string line, List<string> stopsIDs, BusStop fromStop, BusStop toStop, DateTime fromDate, DateTime toDate)
        {
            StreamWriter sw = new StreamWriter(String.Format("C:/Users/simen/master/{0}-{1}_{2}-{3}-log.txt", fromStop.ID, toStop.ID, fromDate.ToShortDateString(), toDate.ToShortDateString()));
            StreamWriter sw2 = new StreamWriter(String.Format("C:/Users/simen/master/{0}-{1}_{2}-{3}_real_error.csv", fromStop.ID, toStop.ID, fromDate.ToShortDateString(), toDate.ToShortDateString()));
            StreamWriter sw3 = new StreamWriter(String.Format("C:/Users/simen/master/{0}-{1}_{2}-{3}.csv", fromStop.ID, toStop.ID, fromDate.ToShortDateString(), toDate.ToShortDateString()));

            var stopsCount = stopsIDs.IndexOf(toStop.ID)-stopsIDs.IndexOf(fromStop.ID);
            sw3.Write("TripID,Departure");
            int count = 1;
            while (count <= stopsCount)
            {
                sw3.Write(",Stop_" + count);
                count++;
            }


            sw3.WriteLine();

            var validTrips = getValidTrips(line, stopsIDs, fromStop, toStop, fromDate, toDate);

            var db = new SMIODataContext();

            var realtimePredictions = db.RealTimeItems
                .Where(r => r.Line == "8")
                .Where(r => DbFunctions.DiffDays(fromDate, r.QueryTime) >= 0)
                .Where(r => DbFunctions.DiffDays(r.QueryTime, toDate) >= 0)
                .Where(r => r.StopID == toStop.RealTimeID)
                .Select(r => new { r.QueryTime, r.Arrival })
                .ToList();

            foreach (var trip in validTrips.Values)
            {
                
                DateTime tripDeparture = trip.First().ActualArrival;
                DateTime tripArrival = trip.Last().ActualArrival;
                List<DateTime> passages = new List<DateTime>();
                var travelTime = 0;
                sw3.Write(trip.First().Trip + "," + tripDeparture);
                for (var i = 1; i < trip.Count(); i++)
                {
                    var passage = trip.ElementAt(i);
                    travelTime += (int)(passage.ActualArrival - trip.ElementAt(i - 1).ActualArrival).TotalSeconds;
                    passages.Add(passage.ActualArrival);
                    sw.Write(passage.ActualArrival.ToShortTimeString() + " ");
                    sw3.Write("," + travelTime);
                    Console.Write(passage.ActualArrival.ToShortTimeString() + " ");
                }
                sw.WriteLine();
                sw3.WriteLine();
                Console.WriteLine();
                
                var tripPredictions = realtimePredictions
                    .Where(r => r.QueryTime >= tripDeparture && r.QueryTime <= tripArrival.AddMinutes(-1))
                    .ToList()
                    .GroupBy(p => p.QueryTime);

                List<DateTime> closePredictionList = new List<DateTime>();
                sw2.Write(trip.First().Trip + "," + tripDeparture + ",");
                for (var i = 0; i < tripPredictions.Count(); i++)
                {
                    var predictionGroup = tripPredictions.ElementAt(i);
                    Console.Write(predictionGroup.First().QueryTime.ToShortTimeString() + ": ");
                    sw.Write(predictionGroup.First().QueryTime.ToShortTimeString() + ": ");
                    
                    var closestPrediction = predictionGroup.First();
                    var minDiff = (int)Math.Abs((closestPrediction.Arrival - tripArrival).TotalSeconds);
                    foreach (var prediction in predictionGroup)
                    {
                        var diff = (int)Math.Abs((prediction.Arrival - tripArrival).TotalSeconds);
                        if (diff < minDiff)
                        {
                            closestPrediction = prediction;
                            minDiff = diff;
                        }
                    }
                    closePredictionList.Add(closestPrediction.Arrival);
                    var er = (int)(closestPrediction.Arrival - tripArrival).TotalSeconds;
                    Console.Write(closestPrediction.Arrival.ToShortTimeString() + " " + er);

                    sw.Write(closestPrediction.Arrival.ToShortTimeString() + " " + er);
                    var comma = (i < tripPredictions.Count() - 1) ? "," : "";
                    sw2.Write(Math.Abs(er) + comma);
                    
                    if (i > 0)
                    {
                        if (Math.Abs((closestPrediction.Arrival - closePredictionList.ElementAt(i-1)).TotalMinutes) > 3){
                            //Console.Write(" <------------ LOST GPS? ");
                            //sw.Write(" <------------ LOST GPS? ");

                            //return;
                        };
                    }
                    Console.WriteLine();
                    sw.WriteLine();
                    
                }
                sw2.WriteLine();

            }
            sw.Close();
            sw2.Close();
            sw3.Close();
        }

        static void getTravelTimeToStop(string line, List<string> stopsIDs, BusStop departureTerminal, BusStop toStop, DateTime departureDateTime, DateTime toDateTime)
        {
            Console.WriteLine("Departures from: " + departureTerminal.Name);
            Console.WriteLine("Arrival at: " + toStop.Name + " [realtime id: " + toStop.RealTimeID + "]");
            var db = new SMIODataContext();
            DateTime toDay = new DateTime(toDateTime.Year, toDateTime.Month, toDateTime.Day).AddDays(1);
            var trips = db.PassageItems
                .Where(p => p.ActualArrival >= departureDateTime && p.ActualArrival <= toDay)
                .Where(p => p.Line == line)
                .OrderBy(p => p.ActualArrival)
                .ToList()
                .Where(p => stopsIDs.Contains(p.BusStopCode))
                .GroupBy(p => new TripID(p.Trip, p.ActualArrival.Date));

            Dictionary<TripID, List<Utilities.AtB.Passage.PassageItem>> validTrips = new Dictionary<TripID, List<Utilities.AtB.Passage.PassageItem>>();
            foreach (var trip in trips)
            {
                TripID tripID = new TripID(trip.First().Trip, trip.First().ActualArrival.Date);
                int stopIndex = stopsIDs.IndexOf(toStop.ID);
                List<Utilities.AtB.Passage.PassageItem> tripSection = new List<Utilities.AtB.Passage.PassageItem>();
                if (trip.Count() >= stopIndex)
                {
                    for (var i = 0; i <= stopIndex; i++)
                    {
                        var passage = trip.ElementAt(i);
                        if (passage.BusStopCode == stopsIDs.ElementAt(i)) tripSection.Add(passage);
                    }
                    if (tripSection.Count() == (stopIndex + 1)) validTrips.Add(tripID, tripSection);
                }
            }

            foreach (var validTrip in validTrips)
            {
                Console.WriteLine("TRIP " + validTrip.Key);
                foreach (var passage in validTrip.Value)
                {
                    Console.Write(passage.BusStopCode + " ");
                }
                Console.WriteLine();
                Console.WriteLine("\n------------------------");
            }    

            Console.WriteLine("ArrivalStop From DB: " + trips.First().Last().BusStop.Name);

            var realtimePredictions = db.RealTimeItems
                .Where(r => r.Line == "8")
                .Where(r => r.QueryTime >= departureDateTime && r.QueryTime <= toDateTime)
                .Where(r => r.StopID == toStop.RealTimeID)
                .Select(r => new { r.QueryTime, r.Arrival})
                .ToList();

            Console.WriteLine("trips: " + trips.Count());
            Console.WriteLine("realtime predictions: " + realtimePredictions.Count());

            for (var i = 0; i < trips.Count(); i++)
            {
                var trip = trips.ElementAt(i);
                DateTime tripDeparture = trip.First().ActualArrival;
                DateTime tripArrival = trip.Last().ActualArrival;
                var tripPredictions = realtimePredictions.Where(r => r.QueryTime >= tripDeparture && r.QueryTime <= tripArrival).ToList();
                int minutes = (int)(trip.Last().ActualArrival - trip.First().ActualArrival).TotalMinutes;
                Console.WriteLine("departure " + trip.First().ActualArrival.ToShortTimeString() + " arrival " + trip.Last().ActualArrival.ToShortTimeString() + " " + minutes);
                //Console.WriteLine("predictions for " + trip.Key + ": " + tripPredictions.Count());
                foreach (var predictionGroup in tripPredictions.GroupBy(p => p.QueryTime))
                {
                    Console.Write(predictionGroup.First().QueryTime.ToShortTimeString() + ": ");
                    var closestPrediction = predictionGroup.First();
                    var minDiff = int.MaxValue;
                    foreach(var prediction in predictionGroup){
                        var diff = (int)Math.Abs((prediction.Arrival - tripArrival).TotalSeconds);
                        //Console.Write(prediction.Arrival.ToShortTimeString() + " ");
                        if (diff < minDiff)
                        {
                            closestPrediction = prediction;
                            minDiff = diff; 
                        }
                    }
                    Console.Write("Closest: " + closestPrediction.Arrival.ToShortTimeString());
                    if (closestPrediction.Arrival > tripArrival) Console.Write(" <------------ LOST GPS? ");
                    Console.WriteLine();
                }
            }

            //foreach (var prediction in realtimePredictions)
            //{
            //    Console.WriteLine(prediction.First().QueryTime + " " + prediction.First().Arrival);
            //}

        }

        //henter trips fra definert start til slutt for bestemt dato, hvor alle busstopp-pasasjer for gjeldende trip finnes
        static void getFullTripsDesc(string fromStop, string toStop, string line, List<string> stopsIDs, DateTime fromDate, DateTime toDate)
        {
            StreamWriter sw = new StreamWriter(String.Format("C:/Users/simen/realtime/{0}-{1}_{2}-{3}.csv",fromStop, toStop, fromDate.ToShortDateString(), toDate.ToShortDateString()));
            StreamWriter swLoops = new StreamWriter(String.Format("C:/Users/simen/realtime/{0}-{1}_{2}-{3}-loops.csv", fromStop, toStop, fromDate.ToShortDateString(), toDate.ToShortDateString()));
            sw.WriteLine("ActualArrival,TimeOfDay,TripID,StopID,Vehicle,Passengers,Temp,Precipitation,Wind,Humidity,TotalStopTime,DistanceLeft,TimeLeft");
            swLoops.WriteLine("ActualArrival,TimeOfDay,Trip,Vehicle,Loop,DistanceLeft,TimeLeft");

            var db = new SMIODataContext();
            BusStop toBusStop = db.BusStops.Where(b => b.ID == toStop).First();
            GeoCoordinate gcTo = new GeoCoordinate((double)toBusStop.Y, (double)toBusStop.X);

            var loopDict = new Dictionary<string, GeoCoordinate>();
            XElement xml = XElement.Load("C:/Users/simen/IdeaProjects/CSVReader/loopsAndStops.xml");
            var placemarks = xml.Elements("Placemark").Where(p => p.Element("description").Value.Contains("LP"));
            foreach (var p in placemarks)
            {
                string desc = p.Element("description").Value;
                string loop = desc.Substring(0, desc.IndexOf("LP") - 1);
                string[] coords = p.Element("Point").Element("coordinates").Value.Split(',');
                string y = coords[1].Substring(0, 12).Replace('.', ',');
                string x = coords[0].Substring(0, 12).Replace('.', ',');
                double lat = Double.Parse(y);
                double lng = Double.Parse(x);

                if (!loopDict.ContainsKey(loop))
                {
                    loopDict.Add(loop, new GeoCoordinate(lat, lng));
                }
            }

            var loopDistances = new Dictionary<string, int>();
            foreach (var l in loopsIDs)
            {
                GeoCoordinate loopGC = loopDict[l];
                loopDistances.Add(l, (int)loopGC.GetDistanceTo(gcTo));
            }

            List<string> validTrips = new List<string>();
            //int stopsOnTrip = stopsIDs.IndexOf(toStop) + 1;
            var trips = db.FullTripItems.Where(t => DbFunctions.DiffDays(fromDate, t.Date) >= 0 && DbFunctions.DiffDays(t.Date, toDate) >= 0 &&//DbFunctions.DiffDays(toDate, t.Date) <= 0 &&
                                            t.ArrivalTime > t.DepartureTime &&
                                            t.Line == line &&
                                            t.DepartureTerminal.ID == fromStop).
                                            Select(t => new {t.Date, t.Trip, t.DepartureTime, t.ArrivalTime }).
                                            OrderBy(t => t.DepartureTime).ToList();
            var virtualTrips = db.PassagesAtVirtualLoops.
                                    Where(t => DbFunctions.DiffDays(fromDate, t.ActualArrival) >= 0 && DbFunctions.DiffDays(t.ActualArrival, toDate) >= 0 && t.Line == line).
                                    ToList().Where(t => loopsIDs.Contains(t.Loop));
            
            //var passages = db.PassageItems.Where(p => DbFunctions.DiffDays(fromDate, p.ActualArrival) == 0 && //DbFunctions.DiffDays(toDate, p.ActualArrival) <= 0 &&
            //                                        p.Line == line).ToList().
            //                                        Where(p => StopIsBefore(p.BusStopCode, toStop));
            var passagesOnTrip = db.PassageItems.Where(p => DbFunctions.DiffDays(fromDate, p.ActualArrival) >= 0 && DbFunctions.DiffDays(p.ActualArrival, toDate) >= 0 &&
                                                    p.Line == line).ToList().
                                                    Where(p => trips.Any(t => t.Trip == p.Trip && t.Date == p.ActualArrival.Date && stopsIDs.Contains(p.BusStopCode))).GroupBy(p => new {ID = p.ActualArrival.Date.ToShortDateString() + "-" +p.Trip}).OrderBy(p => p.First().ActualArrival);

            Console.WriteLine("total passages: " + passagesOnTrip.Count());

            foreach (var trip in passagesOnTrip)
            {
                if (trip.Count() < stopsIDs.Count())
                {
                    Console.WriteLine("Ignore: " + trip.Key);
                    continue;
                }
                Console.Write(trip.Key + " ");
                var totalStopTime = 0;
                var lastPassage = trip.ElementAt(stopsIDs.IndexOf(toStop));

                //for(int i = 0; i < stopsIDs.IndexOf(toStop); i++)
                //{
                //    var p = trip.ElementAt(i);
                //    //Console.Write(p.BusStopCode + " ");
                //    var pc = db.PassengerCounts.FirstOrDefault(d =>
                //                                    d.Trip == p.Trip &&
                //                                    DbFunctions.TruncateTime(d.DateAndTime) == DbFunctions.TruncateTime(p.ActualArrival) &&
                //                                    d.Stop.ID == p.BusStopCode &&
                //                                    d.Vehicle == p.Vehicle);
                //    var passengerCount = pc == null ? -1 : pc.PassengersPresent;
                //    var wo = db.WeatherObjects.FromHour(p.ActualArrival);
                //    totalStopTime += p.StopTime;
                //    var quarters = p.ActualArrival.Hour * 4 + p.ActualArrival.Minute - (p.ActualArrival.Minute % 15);
                //    GeoCoordinate gc = new GeoCoordinate((double)p.BusStop.Y, (double)p.BusStop.X);
                //    int metersLeft = (int)gc.GetDistanceTo(gcTo);
                //    int secondsLeft = (int)(lastPassage.ActualArrival - p.ActualArrival).TotalSeconds;
                //    sw.WriteLine("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}",
                //                    p.ActualArrival,
                //                    quarters,
                //                    p.Trip,
                //                    p.BusStopCode,
                //                    p.Vehicle,
                //                    passengerCount,
                //                    wo.Temp.ToString().Replace(',','.'),
                //                    wo.Precipitation.ToString().Replace(',','.'),
                //                    wo.Wind.ToString().Replace(',','.'),
                //                    wo.Humidity.ToString().Replace(',','.'),
                //                    totalStopTime,
                //                    metersLeft,
                //                    //(p.BusStopCode == stopsIDs[0] ? 1 : 0),
                //                    //(p.BusStopCode == stopsIDs[1] ? 1 : 0),
                //                    //(p.BusStopCode == stopsIDs[2] ? 1 : 0),
                //                    //(p.BusStopCode == stopsIDs[3] ? 1 : 0),
                //                    //(p.BusStopCode == stopsIDs[4] ? 1 : 0),
                //                    secondsLeft);
                //    //Console.WriteLine("{0}, stoptime: {1}, meters left: {2}, seconds left: {3}", p.ActualArrival, totalStopTime, metersLeft, secondsLeft);
                //}
                //Console.WriteLine();
                var loopPassagesOnTrip = virtualTrips.
                                            Where(v => v.ActualArrival > trip.First().ActualArrival &&
                                                v.ActualArrival < lastPassage.ActualArrival &&
                                                v.Vehicle == lastPassage.Vehicle);

                if (loopPassagesOnTrip.Count() > 5)
                {

                    var fiveLoops = loopPassagesOnTrip.Take(5);
                    var firstLoop = loopPassagesOnTrip.First();
                    var hours = firstLoop.ActualArrival.Hour;
                    var quarters = (firstLoop.ActualArrival.Hour * 4) + firstLoop.ActualArrival.Minute - (firstLoop.ActualArrival.Minute % 15);
                    swLoops.Write(hours);
                    for (var i = 0; i < fiveLoops.Count(); i++)
                    {
                        var lp = fiveLoops.ElementAt(i);
                        //var wo = db.WeatherObjects.FromHour(lp.ActualArrival);
                        DateTime arrival = trip.ElementAt(stopsIDs.IndexOf(toStop)).ActualArrival;
                        int secondsLeft = (int)(arrival - lp.ActualArrival).TotalSeconds;
                        //swLoops.WriteLine("{0},{1},{2},{3},{4},{5},{6}",
                        //                lp.ActualArrival,
                        //                hours,
                        //                trip.First().Trip,
                        //                lp.Vehicle,
                        //                lp.Loop,
                        //                loopDistances[lp.Loop],
                        //                secondsLeft);
                        swLoops.Write("," + secondsLeft);
                        Console.WriteLine(lp.Loop + " " + lp.ActualArrival.ToShortTimeString());
                    }
                    swLoops.WriteLine();
                }
            }

            
            //foreach (var t in trips)
            //{
            //    var passagesOnTrip = passages.Where(p => p.Trip == t.Trip);
            //    if (passagesOnTrip.Count() == stopsOnTrip)
            //    {
            //        validTrips.Add(t.Trip);
            //        BusStop arrivalStop = busstops.Where(s => s.ID == toStop).First();
            //        GeoCoordinate arrivalGC = new GeoCoordinate((double)arrivalStop.Y, (double)arrivalStop.X);
            //        DateTime arrivalTime = passagesOnTrip.Last().ActualArrival;
            //        var totalStopTime = 0;
            //        for(var i = 0; i < passagesOnTrip.Count() - 1; i++){
            //            var p = passagesOnTrip.ElementAt(i);
            //            BusStop currentStop = busstops.Where(s => s.ID == p.BusStopCode).First();
            //            GeoCoordinate stopGC = new GeoCoordinate((double)currentStop.Y, (double)currentStop.X);
            //            var quarters = p.ActualArrival.Hour*4 + p.ActualArrival.Minute-(p.ActualArrival.Minute % 15);
            //            totalStopTime += p.StopTime;
            //            int metersLeft = (int)stopGC.GetDistanceTo(arrivalGC);
            //            int secondsLeft = (int)(arrivalTime-p.ActualArrival).TotalSeconds;
            //            sw.WriteLine("{0},{1},{2},{3},{4}", p.ActualArrival, quarters, totalStopTime, metersLeft, secondsLeft);
            //            //Console.WriteLine("{0}, stoptime: {1}, meters left: {2}, seconds left: {3}", p.ActualArrival, p.StopTime);
            //        }
            //    }
            //    //Console.WriteLine(t.Trip + ": " + passages.Where(p => p.Trip == t.Trip).Count() + " valid: " + validTrips.Contains(t.Trip));
            //}        
            sw.Close();
            swLoops.Close();
        }


        static void getFullTrips(string line, List<string> stopsIDs, BusStop departureTerminal, BusStop arrivalTerminal, DateTime departureDateTime, DateTime arrivalDateTime, bool delays)
        {
            var db = new SMIODataContext();
            StreamWriter sw = new StreamWriter(String.Format("C:/Users/simen/realtime/{0}-{1}_{2}-{3}-stops{4}.csv", departureTerminal.ID, arrivalTerminal.ID, departureDateTime.ToShortDateString(), arrivalDateTime.ToShortDateString(), delays ? "-delay" : ""));

            bool writtenHeader = false;

            //while (currentDate < arrivalDateTime)
            //{
            var trips = db.PassageItems.
                Where(p => DbFunctions.DiffDays(departureDateTime, p.ActualArrival) >= 0 && DbFunctions.DiffDays(p.ActualArrival, arrivalDateTime) >= 0).
                Where(p => p.Line == line).GroupBy(p => new {Trip = p.Trip, Start = DbFunctions.DiffDays(departureDateTime,p.ActualArrival)}).ToList().
                Where(p => p.Count() == 20).
                Where(p => p.First().BusStop.ID == departureTerminal.ID && p.Last().BusStop.ID == arrivalTerminal.ID).
                OrderBy(p => p.First().ActualArrival);

            //var loopPassages = db.PassagesAtVirtualLoops.
            //    Where(p => DbFunctions.DiffDays(departureDateTime, p.ActualArrival) >= 0 && DbFunctions.DiffDays(p.ActualArrival, arrivalDateTime) >= 0).
            //    Where(p => loopsIDs.Contains(p.Loop) && p.Line == line).ToList().OrderBy(p => p.ActualArrival).ThenBy(p => p.Vehicle);

            HashSet<int> skippedTripIndices = new HashSet<int>();
            for (var i = 1; i < trips.Count(); i++)
            {
                var trip = trips.ElementAt(i);
                var lastTrip = trips.ElementAt(i - 1);
                if ((trip.First().ActualArrival - lastTrip.First().ActualArrival).TotalHours > 4) continue;
                    //var firstPassage = passages.First();
                    //var lp = loopPassages.Where(l => l.ActualArrival > firstPassage.ActualArrival && l.Vehicle == firstPassage.Vehicle).Take(loopsIDs.Count()).ToList();
                    //foreach (var l in lp)
                    //{
                    //    Console.Write(l.Vehicle + " " + l.ActualArrival + " ");

                    //}
                    //Console.WriteLine();
                    if (!writtenHeader)
                    {
                        sw.Write("DepartureDateTime,DepartureHour");
                        int n = 0;
                        foreach (var p in trip.Take(trip.Count() - 1))
                        {
                            sw.Write(",NP-" + n + ",N-" + n);
                            n++;
                        }
                        sw.WriteLine();
                        writtenHeader = true;
                    }
                    int totalTravelTime = (int)(trip.Last().ActualArrival - trip.First().ActualArrival).TotalMinutes;
                    if (totalTravelTime > 40)
                    {
                        skippedTripIndices.Add(i);
                        continue;
                    }
                    bool skip = false;
                    for (var t = 1; t < trip.Count(); t++)
                    {
                        int ltt = (int)(trip.ElementAt(t).ActualArrival - trip.ElementAt(t - 1).ActualArrival).TotalSeconds;
                        if (ltt < 0 || ltt > 360)
                        {
                            skip = true;
                        }

                    }
                    if (skip)
                    {
                        skippedTripIndices.Add(i);
                        continue;
                    }
                    Console.Write(trip.First().ActualArrival + "," + trip.First().ActualArrival.Hour);
                    sw.Write(trip.First().ActualArrival + "," + trip.First().ActualArrival.Hour);
                    int totalTime = 0;
                    int prevIndex = i-1;
                    while (skippedTripIndices.Contains(prevIndex))
                    {
                        prevIndex--;
                    }
                    lastTrip = trips.ElementAt(prevIndex);
                    int lastTotalTime = 0;
                    for (var t = 1; t < trip.Count(); t++)
                    {
                        if (delays)
                        {
                            int delay = (int)(trip.ElementAt(t).ActualArrival - trip.ElementAt(t).ScheduledArrival).TotalMinutes;
                            int lastDelay = (int)(lastTrip.ElementAt(t).ActualArrival - lastTrip.ElementAt(t).ScheduledArrival).TotalMinutes;
                            Console.Write("," + lastDelay + "," + delay);
                            sw.Write("," + lastDelay + "," + delay);
                        }
                        else
                        {
                            lastTotalTime += (int)(lastTrip.ElementAt(t).ActualArrival - lastTrip.ElementAt(t - 1).ActualArrival).TotalSeconds;
                            totalTime += (int)(trip.ElementAt(t).ActualArrival - trip.ElementAt(t - 1).ActualArrival).TotalSeconds;
                            Console.Write("," + lastTotalTime + "," + totalTime);
                            sw.Write("," + lastTotalTime + "," + totalTime);
                        }

                    }
                    Console.WriteLine();
                    sw.WriteLine();
                }
            sw.Close();
        }

        static Boolean StopIsBefore(List<string> stopsIDs, string isbefore, string stop)
        {
            return stopsIDs.IndexOf(isbefore) <= stopsIDs.IndexOf(stop) && stopsIDs.IndexOf(isbefore) >= 0;
        }
        static void Main(string[] args)
        {
            //Laga eigen klasse for å unngå merge conflicts.
            if (Data.User == Data.Users.Erlend)
            {
                ErlendTest.Run();
                return;
            }

            var db = new SMIODataContext();
            var fromDate = new DateTime(2014, 2, 17);
            var toDate = new DateTime(2014, 2, 20);


            BusStop kongens_gate_K2 = db.BusStops.Single(s => s.ID == "16010907");
            //BusStop blakli = db.BusStops.Single(s => s.ID == "16010061");
            //BusStop nardoSenteret = db.BusStops.Single(s => s.ID == "16010341");
            //BusStop lerkendal = db.BusStops.Single(s => s.ID == "16010264");
            BusStop samfundet = db.BusStops.Single(s => s.ID == "16010476");
            BusStop risvollan_senter = db.BusStops.Single(s => s.ID == "16010394");
            //getFullTrips("8", kongens_gate_K2, blakli, fromDate, toDate, false);
            List<String> stopsIDs = new List<string>(){"16010907","16010011","16010476","16010112","16010376","16010264","16010263","16010102","16010131","16010320","16010341","16010465","16010457","16010037","16010395","16010548","16010394","16010420","16010294"};

            getTravelTimes("8", stopsIDs, kongens_gate_K2, risvollan_senter, fromDate, toDate);

            Console.WriteLine("done");
            
            //<-----------
             
            Console.ReadKey();
        }
    }
}
