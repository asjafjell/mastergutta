﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.Passage;
using Utilities.Database;

namespace QueryTester
{
    class ErlendTest
    {
        public static void Run()
        {
            var db = new SMIODataContext();
            var d = new DateTime(2013, 08, 01);
            var res = db.PassageItems.
                         Where(p => DbFunctions.TruncateTime(p.ActualArrival) == d && p.Line == "22").
                         OrderBy(p => p.Vehicle).
                         ThenBy(p => p.ActualArrival).
                         ToList();

            var trips = new List<List<PassageItem>>();
            while (res.Any())
            {
                var tripID = res.First().Trip;
                var trip = res.TakeWhile(p => p.Trip != tripID).ToList();
                res = res.Skip(trip.Count).ToList();
                trips.Add(trip);
                Console.WriteLine(string.Join(",", trip.Select(p => p.BusStopCode)));
            }

            Console.ReadLine();
        }
    }
}
