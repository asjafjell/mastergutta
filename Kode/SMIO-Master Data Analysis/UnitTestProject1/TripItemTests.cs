﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.AtB.Trips;
using Utilities.Database;

namespace UnitTestProject1
{
    [TestClass]
    public class TripItemTests : TripItem
    {
        List<TripItem> _trips;
        private SMIODataContext _db;

        [TestInitialize]
        public void Initialize()
        {
            _db = new SMIODataContext();
            _trips = new List<TripItem>()
                {
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 26, 15, 35, 00),
                            ActualDeparture = new DateTime(2014, 02, 26, 15, 35, 12),
                            ScheduledArrival = new DateTime(2014, 02, 26, 15, 35, 00),
                            ActualArrival = new DateTime(2014, 02, 26, 15, 35, 12),
                        },
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 26, 15, 45, 00),
                            ActualDeparture = new DateTime(2014, 02, 26, 15, 45, 13),
                            ScheduledArrival = new DateTime(2014, 02, 26, 15, 45, 00),
                            ActualArrival = new DateTime(2014, 02, 26, 15, 45, 13),
                        },
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 26, 16, 35, 00),
                            ActualDeparture = new DateTime(2014, 02, 26, 16, 35, 14),
                            ScheduledArrival = new DateTime(2014, 02, 26, 16, 35, 00),
                            ActualArrival = new DateTime(2014, 02, 26, 16, 35, 14),
                        },
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 26, 17, 35, 00),
                            ActualDeparture = new DateTime(2014, 02, 26, 17, 35, 15),
                            ScheduledArrival = new DateTime(2014, 02, 26, 17, 35, 00),
                            ActualArrival = new DateTime(2014, 02, 26, 17, 35, 15),
                        },
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 26, 18, 35, 00),
                            ActualDeparture = new DateTime(2014, 02, 26, 18, 35, 16),
                            ScheduledArrival = new DateTime(2014, 02, 26, 18, 35, 00),
                            ActualArrival = new DateTime(2014, 02, 26, 18, 35, 16),
                        },
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 27, 15, 35, 00),
                            ActualDeparture = new DateTime(2014, 02, 27, 15, 35, 17),
                            ScheduledArrival = new DateTime(2014, 02, 27, 15, 35, 00),
                            ActualArrival = new DateTime(2014, 02, 27, 15, 35, 17),
                        },
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 27, 15, 45, 00),
                            ActualDeparture = new DateTime(2014, 02, 27, 15, 45, 18),
                            ScheduledArrival = new DateTime(2014, 02, 27, 15, 45, 00),
                            ActualArrival = new DateTime(2014, 02, 27, 15, 45, 18),
                        },
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 27, 15, 55, 00),
                            ActualDeparture = new DateTime(2014, 02, 27, 15, 55, 19),
                            ScheduledArrival = new DateTime(2014, 02, 27, 15, 55, 00),
                            ActualArrival = new DateTime(2014, 02, 27, 15, 55, 19),
                        },
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 27, 16, 35, 00),
                            ActualDeparture = new DateTime(2014, 02, 27, 16, 35, 20),
                            ScheduledArrival = new DateTime(2014, 02, 27, 16, 35, 00),
                            ActualArrival = new DateTime(2014, 02, 27, 16, 35, 20),
                        },
                    new TripItem()
                        {
                            ScheduledDeparture = new DateTime(2014, 02, 27, 17, 35, 00),
                            ActualDeparture = new DateTime(2014, 02, 27, 17, 35, 21),
                            ScheduledArrival = new DateTime(2014, 02, 27, 17, 35, 00),
                            ActualArrival = new DateTime(2014, 02, 27, 17, 35, 21),
                        }
                };
        }

        [TestMethod]
        public void AnnMethodTests()
        {
            var item = _db.Trips.Single(p => p.LineID == "22" && p.ActualDeparture == new DateTime(2013, 10, 4, 0, 2, 4));
            var inputs = item.ToAnnInput();
            var target = new[]
                {
                    item.DayOfWeek,
                    item.MinuteOfDay,
                    item.MonthOfYear,
                    item.ActualArrival.TimeOfDay.TotalMinutes,
                    item.ScheduledDeparture.TimeOfDay.TotalMinutes,
                    item.ScheduledArrival.TimeOfDay.TotalMinutes,
                    item.IsSummerTime ? 1 : 0,
                    item.IsHoliday ? 1 : 0,
                    item.IsGeneralVacation ? 1 : 0,
                    item.LineID.GetHashCode(),
                    item.RouteID.GetHashCode(),
                    item.VehicleID.GetHashCode(),
                    item.InitialDelay,
                    item.SoccerGameWithinTwoHoursFromNow ? 1 : 0,
                    item.SoccerGameLessThanTwoHoursAgo ? 1 : 0,
                    item.SoccerGameViewers,
                    (double) item.Temperature,
                    (double) item.Precipitation,
                    (double) item.Wind,
                    item.Humidity,
                    (double) item.TemperatureYesterday,
                    (double) item.PrecipitationYesterday,
                    (double) item.WindYesterday,
                    item.HumidityYesterday,
                    (double) item.TemperatureLastWeek,
                    (double) item.PrecipitationLastWeek,
                    (double) item.WindLastWeek,
                    item.HumidityLastWeek,
                    item.DelayLastTrip,
                    item.AverageDelayLastFiveTrips,
                    item.StandardDeviationOfDelayForLastFiveTrips,
                    item.DelayForThisTripLastWeek,
                    item.AverageDelayForLastFiveTripsLastWeek
                };

            for (var i = 0; i < inputs.Length; i++)
            {
                Assert.AreEqual(target[i], inputs[i]);
            }
        }

        [TestMethod]
        public void FindDelayForTripsTests()
        {
            Assert.AreEqual(17, FindDelayForTrip(_trips, new DateTime(2014, 02, 27, 15, 35, 00)));
            Assert.AreEqual(int.MinValue, FindDelayForTrip(_trips, new DateTime(2011, 02, 27, 15, 35, 21)));
            Assert.AreEqual(17, FindDelayForTrip(_trips, new DateTime(2014, 02, 27, 15, 35, 21)));

            var target = new[] {14, 15, 16, 17, 18};
            var test = FindDelayForTripsBefore(_trips, new DateTime(2014, 02, 27, 15, 55, 00), 5);
            for (var i = 0; i < target.Length; i++)
                Assert.AreEqual(target[i], test[i]);

            target = new[] {int.MinValue, int.MinValue, int.MinValue, int.MinValue, int.MinValue};
            test = FindDelayForTripsBefore(_trips, new DateTime(2013, 02, 27, 15, 35, 00), 5);
            for (var i = 0; i < target.Length; i++)
                Assert.AreEqual(target[i], test[i]);

            target = new[] {int.MinValue, int.MinValue, int.MinValue, 12, 13};
            test = FindDelayForTripsBefore(_trips, new DateTime(2014, 02, 26, 16, 35, 00), 5);
            for (var i = 0; i < target.Length; i++)
                Assert.AreEqual(target[i], test[i]);

            target = new[] {17, 18, 19, 20, 21};
            test = FindDelayForTripsBefore(_trips, new DateTime(2015, 02, 27, 15, 35, 00), 5);
            for (var i = 0; i < target.Length; i++)
                Assert.AreEqual(target[i], test[i]);
        }
    }
}
