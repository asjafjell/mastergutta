﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Forms;
using ClassificationANNTrip.Classifiers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTestProject1
{
    [TestClass]
    public class AClassifierTests :AClassifier
    {
        [TestMethod]
        public void ScaleTests()
        {
            Min = -7;
            Max = 18;

            Assert.AreEqual(0, ScaleToAnn(-7));
            Assert.AreEqual(0.5, ScaleToAnn(5.5));
            Assert.AreEqual(1, ScaleToAnn(18));

            Assert.AreEqual(-7, ScaleFromAnn(0));
            Assert.AreEqual(5.5, ScaleFromAnn(0.5));
            Assert.AreEqual(18, ScaleFromAnn(1));

            Min = 0;
            Max = 1;

            Assert.AreEqual(0, ScaleToAnn(0));
            Assert.AreEqual(0.5, ScaleToAnn(0.5));
            Assert.AreEqual(1, ScaleToAnn(1));

            Assert.AreEqual(0, ScaleFromAnn(0));
            Assert.AreEqual(0.5, ScaleFromAnn(0.5));
            Assert.AreEqual(1, ScaleFromAnn(1));

            Min = 10;
            Max = 20;

            Assert.AreEqual(0, ScaleToAnn(10));
            Assert.AreEqual(0.5, ScaleToAnn(15));
            Assert.AreEqual(1, ScaleToAnn(20));

            Assert.AreEqual(10, ScaleFromAnn(0));
            Assert.AreEqual(15, ScaleFromAnn(0.5));
            Assert.AreEqual(20, ScaleFromAnn(1));
        }

        public override double CurrentError { get; protected set; }

        public override bool ReadyForTesting
        {
            get { throw new NotImplementedException(); }
        }

        public override string Summary
        {
            get { throw new NotImplementedException(); }
        }

        protected override Task DoTraining()
        {
            throw new NotImplementedException();
        }

        protected override Task<ErrorKeeper> DoTesting()
        {
            throw new NotImplementedException();
        }

        public override void SetData<T>(IEnumerable<T> trainingSet, IEnumerable<T> testSet)
        {
            throw new NotImplementedException();
        }
    }
}
