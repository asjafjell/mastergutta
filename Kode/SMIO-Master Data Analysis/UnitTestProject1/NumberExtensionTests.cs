﻿using System;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Utilities.Extensions;

namespace UnitTestProject1
{
    [TestClass]
    public class NumberExtensionTests
    {
        [TestMethod]
        public void StandardDeviationTests()
        {
            Assert.AreEqual(double.NaN, new double[] { }.StandardDeviation());
            Assert.AreEqual(double.NaN, new int[] { }.StandardDeviation());
            Assert.AreEqual(double.NaN, new float[] { }.StandardDeviation());
            Assert.AreEqual(double.NaN, new decimal[] { }.StandardDeviation());

            Assert.AreEqual(0, new double[] { 4, 4 }.StandardDeviation());
            Assert.AreEqual(0, new int[] { 4, 4 }.StandardDeviation());
            Assert.AreEqual(0, new float[] { 4, 4 }.StandardDeviation());
            Assert.AreEqual(0, new decimal[] { 4, 4 }.StandardDeviation());

            Assert.AreEqual(11.146151191803, new double[] { 4, 2, 45, 6, 7, 3, 2, 4, 5, 2, 12, 1, 6 }.StandardDeviation(), 0.005);
            Assert.AreEqual(11.146151191803, new int[] { 4, 2, 45, 6, 7, 3, 2, 4, 5, 2, 12, 1, 6 }.StandardDeviation(), 0.005);
            Assert.AreEqual(11.146151191803, new float[] { 4, 2, 45, 6, 7, 3, 2, 4, 5, 2, 12, 1, 6 }.StandardDeviation(), 0.005);
            Assert.AreEqual(11.146151191803, new decimal[] { 4, 2, 45, 6, 7, 3, 2, 4, 5, 2, 12, 1, 6 }.StandardDeviation(), 0.005);
        }

        [TestMethod]
        public void NormalizeTests()
        {
            var test = new double[][]
                {
                    new double[] {-7, 0, 10, 0},
                    new double[] {5.5, 0.5, 15, 0},
                    new double[] {18, 1, 20, 0}
                };

            var normalized = test.Normalize();

            for (var i = 0; i < normalized.Length; i++)
            {
                Assert.AreEqual(new[] {0.0, 0.5, 1.0}[i], normalized[i].Distinct().First());
                Assert.AreEqual(0.0, normalized[i][3]);
            }
        }
    }
}
