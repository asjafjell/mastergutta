﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Xml.Linq;
using Utilities.Extensions;
using Utilities.Soccer;
using System.Data.Entity;

namespace RBK_Data_Fetcher
{
    internal class Program
    {
        private static void Main(string[] args)
        {

            //var file = System.IO.Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.DesktopDirectory), "Rosenborg.csv");

            //var games = FetchGames().Select(p=> p.ToString()).ToList();
            //games.Insert(0, SoccerGame.GetHeaders());

            //System.IO.File.WriteAllLines(file,games, Encoding.UTF8);

            var games = SoccerGame.ReadFile(@"C:\Master\Repo\Data\fotball\2013_Rosenborg.csv");

            using (var db = new SoccerGameContext())
            {
                var game = games.ElementAt(3);
                
                db.SoccerGames.Add(game);
                db.SaveChanges();

                var query = from b in db.SoccerGames
                            orderby b.Time
                           select b;

                foreach (var soccerGame in query)
                {
                    Console.WriteLine(soccerGame.HomeTeam);
                }
            }

        }

        private static IEnumerable<SoccerGame> FetchGames()
        {
            const string url = "http://godfoten.rbk.no/terminlisteprint.aspx?y=2013&sid=1&lang=nb-NO";

            var wc = new WebClient { Encoding = Encoding.UTF8 };
            var html = wc.DownloadString(url);

            if (!html.Contains("Terminliste"))
            {
                throw new Exception("Invalid RBK data.");
            }

            var parts =
                html.Split(
                    " <table id=\"tblTerminliste\" cellspacing=\"0\" cellpadding=\"0\" width=\"750\">")[1].
                    Split("<table id=\"tblMessage\" cellspacing=\"0\" cellpadding=\"1\" width=\"550\">")[0].
                    Split("<table id=\"tblInfo\" cellspacing=\"0\" cellpadding=\"0\" width=\"100%\">")[1].
                    Split("</table>")[0];

            var xml = XElement.Parse("<root>" + parts + "</root>");

            var games = xml.Elements("tr").Skip(2).
                            Select(p => p.Elements("td").ToArray()).
                            Where(p => p.Count() >= 2).
                            Select(p => new
                            {
                                HomeTeam = p[0].Value.Split(" - ")[0].Trim(),
                                AwayTeam = p[0].Value.Split(" - ")[1].Trim(),
                                Time = p[3].Value.Trim(),
                            }).Where(p => p.HomeTeam == "Rosenborg").
                            Select(p => new SoccerGame()
                            {
                                Time =
                                    DateTime.ParseExact(p.Time, "dd'.'MM'.'yyyy", CultureInfo.InvariantCulture),
                                HomeTeam = p.HomeTeam,
                                AwayTeam = p.AwayTeam,
                            });


            return games;

        } 

       
    }
    public class SoccerGameContext : DbContext
    {
        public DbSet<SoccerGame> SoccerGames { get; set; }
    }
}

