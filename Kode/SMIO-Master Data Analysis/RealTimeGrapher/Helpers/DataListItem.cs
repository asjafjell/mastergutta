﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RealTimeGrapher
{
    public class DataListItem<T>
    {
        private readonly T _item;
        private readonly string[] _displayStrings;
        private ListViewItem _lvi;

        public DataListItem(T item, params string[] displayStrings)
        {
            _item = item;
            _displayStrings = displayStrings;
        }

        public ListViewItem ListViewItem
        {
            get { return _lvi ?? (_lvi = new ListViewItem(_displayStrings) { Tag = _item }); }
        }

        public bool Selected
        {
            get { return ListViewItem.Selected; }
            set { ListViewItem.Selected = value; }
        }
    }
}
