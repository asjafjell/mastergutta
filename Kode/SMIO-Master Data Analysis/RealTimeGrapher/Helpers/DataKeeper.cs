﻿using System;
using System.Collections.Generic;

namespace RealTimeGrapher.Helpers
{
    public class DataKeeper
    {
        public DateTime Start { get; private set; }
        public DateTime End { get; private set; }

        private PlotHelper _helper;
        private Dictionary<string, string> _dataDict;
        public PlotHelper Helper { get { return _helper; } }
        public frmMain Owner { get; private set; }

        public DataKeeper(DateTime start, DateTime end, frmMain owner)
        {
            _dataDict = new Dictionary<string, string>();
            Owner = owner;
            Start = start;
            End = end;
            _helper = new PlotHelper(owner);
        }

        public void AddData(string key, string value)
        {
            _dataDict.Add(key, value);
        }

        public string GetData(string key)
        {
            return _dataDict[key];
        }
    }
}
