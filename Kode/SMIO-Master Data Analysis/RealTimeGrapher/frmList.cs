﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RealTimeGrapher.Helpers;

namespace RealTimeGrapher
{
    public partial class frmList<T> : Form
    {
        public List<T> Selected { get; private set; }

        public frmList(string[] headers, IEnumerable<DataListItem<T>> list)
        {
            InitializeComponent();

            lstItems.Clear();
            headers.ToList().ForEach(p => lstItems.Columns.Add(p));
            lstItems.Items.AddRange(list.Select(p => p.ListViewItem).ToArray());

            var totalWidth = 0;
            for (var i = 0; i < lstItems.Columns.Count; i++)
            {
                lstItems.Columns[i].AutoResize(ColumnHeaderAutoResizeStyle.ColumnContent);
                var size = lstItems.Columns[i].Width;
                lstItems.Columns[i].Width = size;
                totalWidth += size + 5;
            }

            Width = Math.Max(226, totalWidth + lstItems.Left*2);
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Selected = new List<T>();
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Selected = lstItems.SelectedItems.Cast<ListViewItem>().Select(p => (T)p.Tag).ToList();
            DialogResult = DialogResult.OK;
            Close();
        }

        public static void SelectItems(string[] headers, List<T> items, Func<T, string[]> func, Action<List<T>> onSuccess)
        {
            var list = items.Select(p => new DataListItem<T>(p, func(p)));
            var form = new frmList<T>(headers, list);
            if (form.ShowDialog() != DialogResult.Cancel)
                onSuccess(form.Selected);
        }

        public static void SelectItems(string[] headers, List<T> items, List<T> selectedItems, Func<T, string[]> func, Action<List<T>> onSuccess)
        {
            var list = items.Select(p => new DataListItem<T>(p, func(p)) {Selected = selectedItems.Contains(p)});
            var form = new frmList<T>(headers, list);
            var res = form.ShowDialog();
            if (res != DialogResult.Cancel)
                onSuccess(form.Selected);
        }
    }
}
