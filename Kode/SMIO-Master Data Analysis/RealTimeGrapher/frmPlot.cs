﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RealTimeGrapher
{
    public partial class frmPlot : Form
    {
        public frmPlot()
        {
            InitializeComponent();
            SetLoading(true);
        }

        public void SetLoading(bool loading)
        {
            if (pnlLoading.InvokeRequired)
            {
                pnlLoading.Invoke(new MethodInvoker(() => SetLoading(loading)));
                return;
            }
            pnlLoading.Visible = loading;
            Chart.Visible = !loading;
        }
    }
}
