﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealTimeGrapher.Helpers;

namespace RealTimeGrapher.Plots
{
    public abstract class PlotBase
    {
        public abstract bool ShowLines { get; }
        public abstract bool ShowStops { get; }

        public abstract void Plot(DataKeeper keeper);
    }
}
