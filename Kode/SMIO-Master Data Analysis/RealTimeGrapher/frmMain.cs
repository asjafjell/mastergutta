﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RealTimeGrapher.Helpers;
using RealTimeGrapher.Plots;
using RealTimeGrapher.Plots.Implementations;
using Utilities.AtB.General;
using Utilities.Database;

namespace RealTimeGrapher
{
    public partial class frmMain : Form
    {
        private readonly Type[] _plots;

        /// <summary>
        /// Returns an instance of the selected plot class.
        /// </summary>
        protected PlotBase SelectedPlot
        {
            get { return (PlotBase) GetType().GetMethod("Plot").MakeGenericMethod(_plots[comType.SelectedIndex]).Invoke(this, null); }
        }

        private PlotBase _selected = null;
        /// <summary>
        /// Keeps track of the selected plot class to avoid unneccessary instantiations.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public PlotBase Plot<T>() where T: PlotBase, new()
        {
            if(_selected == null || !(_selected is T))
                return _selected = new T();
            return _selected;
        }

        readonly SMIODataContext _db = new SMIODataContext();

        public frmMain()
        {
            InitializeComponent();

            _plots = Assembly.GetExecutingAssembly().GetTypes().Where(p => p.IsClass && p.MemberType != MemberTypes.NestedType && p.Namespace == "RealTimeGrapher.Plots.Implementations").ToArray();
            ListPlotTypes();
        }

        /// <summary>
        /// Creates a DataKeeper from the current form values.
        /// </summary>
        /// <returns></returns>
        private DataKeeper GetDataKeeper()
        {
            var dk = new DataKeeper(dtpStart.Value, dtpEnd.Value, this);
            dk.AddData("lines", txtLines.Text);
            dk.AddData("stops", txtStop.Text);
            return dk;
        }

        /// <summary>
        /// Lists all available plot types with human friendly names.
        /// </summary>
        private void ListPlotTypes()
        {
            comType.Items.Clear();
            foreach (var plot in _plots)
            {
                var pt = plot.Name.ToCharArray().ToList();
                var ups = new List<int>();
                for (var i = 1; i < pt.Count; i++)
                    if (Char.IsUpper(pt[i]) && pt[i - 1] != '_')
                        ups.Add(i);
                ups.Reverse();
                ups.ForEach(p => pt.Insert(p, ' '));
                comType.Items.Add(string.Join("", pt).Replace("_", " / "));
            }
            comType.SelectedIndex = 0;
        }

        private void btnPlot_Click(object sender, EventArgs e)
        {
            try
            {
                SelectedPlot.Plot(GetDataKeeper());
            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Plot failed with the exception message: {0}", ex.Message));
            }
        }

        private void comType_SelectedIndexChanged(object sender, EventArgs e)
        {
            lblLines.Visible = btnPickLines.Visible = txtLines.Visible = SelectedPlot.ShowLines;
            lblStops.Visible = btnPickStops.Visible = txtStop.Visible = SelectedPlot.ShowStops;
        }

        private void btnPickLines_Click(object sender, EventArgs e)
        {
            frmList<string>.SelectItems(new[] {"Line"},
                                        _db.Lines.Select(p => p.ID).ToList().OrderBy(int.Parse).ToList(),
                                        s => new[] {s},
                                        s =>
                                            {
                                                txtLines.Text = string.Join(", ", s);
                                            });
        }

        private void btnPickStops_Click(object sender, EventArgs e)
        {
            var lines = txtLines.Text.Split(',').Select(p => p.Trim()).ToArray();
            frmList<BusStop>.SelectItems(new[] {"ID", "RealTime ID", "Name", "X", "Y"},
                                         _db.Lines.Where(p => lines.Contains(p.ID)).SelectMany(p => p.Stops).ToList(),
                                         s => new[] {s.ID, s.RealTimeID, s.Name, s.X.ToString("n6"), s.Y.ToString("n6")},
                                         s =>
                                             {
                                                 txtStop.Text = string.Join(", ", s.Select(p => p.ID));
                                             });
        }
    }
}
