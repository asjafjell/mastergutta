﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;
using Newtonsoft.Json.Linq;
using Utilities.AtB.General;
using Utilities.AtB.RealTime;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace GPSLogger
{
    public partial class frmMain : Form
    {
        readonly Dictionary<WebBrowser, int> _browsers;
        private DateTime _lastDataExport = DateTime.Now;

        public frmMain()
        {
            InitializeComponent();

            _browsers = new Dictionary<WebBrowser, int>()
                {
                    {webBrowser1, 5},
                    {webBrowser2, 22},
                    {webBrowser3, 8},
                    {webBrowser4, 9},
                    {webBrowser5, 60},
                    {webBrowser6, 63},
                    {webBrowser7, 6},
                    {webBrowser8, 3},
                    {webBrowser9, 7},
                    {webBrowser10, 36},
                    {webBrowser11, 18},
                    {webBrowser12, 66},
                };

            foreach (var browser in _browsers.Keys)
                browser.DocumentCompleted += browser_DocumentCompleted;
            
            webBrowser1.Navigate(GetLineUrl(_browsers[webBrowser1]));
        }

        void browser_DocumentCompleted(object sender, WebBrowserDocumentCompletedEventArgs e)
        {
            var wb = (WebBrowser)sender;
            if (e.Url.ToString().Contains("Login"))
            {
                var doc = wb.Document;

                doc.GetElementById("tbUsername").SetAttribute("value", "studentbruker");
                doc.GetElementById("tbPassword").SetAttribute("value", "studentbruker");
                doc.GetElementById("_btnLogin").InvokeMember("click");
            }
            else if(e.Url.ToString().Contains("Desktop"))
            {
                wb.Navigate(GetLineUrl(_browsers[wb]));
            }else if (e.Url.ToString().Contains("VehicleStatus"))
            {
                Save(_browsers[wb], wb.DocumentText);
            }
        }

        private readonly string[] _columns = new[]
            {
                "areaID",
                "areaCode",
                "areaDesc",
                "lineID",
                "lineAlphaCode",
                "pathID",
                "pathDirection",
                "vehicleID",
                "vehicleAlphaCode",
                "vehicleStatus",
                "origStopCode",
                "origStopDesc",
                "origStopType",
                "destStopCode",
                "destStopDesc",
                "destStopType",
                "termDestStopCode",
                "termDestStopDesc",
                "proxChangeStopCode",
                "proxChangeStopDesc",
                "proxChangeTimeArrive",
                "geo_long",
                "geo_lat",
                "locationCity",
                "locationAddress",
                "timeRefreshLocation",
                "passengersCount",
                "passengersLevel",
                "secDelay",
                "secInterval",
                "secDelayPlanned",
                "secBonusLine",
                "secOffset",
                "secDelayFormatted",
                "secIntervalFormatted",
                "secDelayPlannedFormatted",
                "secBonusLineFormatted",
                "relativePosition",
                "driverCode",
                "driverName",
                "tripCode",
                "shiftCode"
            };

        private Dictionary<int, List<string>> _lastSaved = new Dictionary<int, List<string>>();
        private int _consecutiveErrors = 0;
        private void Save(int lineID, string html)
        {
            try
            {
                System.IO.Directory.CreateDirectory(GetDataFolder());
                var save = new List<string>();
                var xml = XElement.Parse(html.Replace("geo:long", "geo_long").Replace("geo:lat", "geo_lat"));
                xml = xml.Element("channel");
                foreach (var item in xml.Elements("item"))
                {
                    var data = new string[_columns.Length];
                    for (int i = 0; i < _columns.Length; i++)
                        data[i] = item.Element(_columns[i]).Value;

                    var line = string.Join(";", data);
                    if (!_lastSaved.ContainsKey(lineID) || !_lastSaved[lineID].Contains(line))
                        save.Add(line);
                }

                System.IO.File.AppendAllLines(GetLineFile(lineID), save, Encoding.Default);
                if(_lastSaved.ContainsKey(lineID))
                    _lastSaved[lineID] = save;
                else
                    _lastSaved.Add(lineID, save);
                _consecutiveErrors = 0;
            }
            catch (Exception ex)
            {
                LogException(ex);
            }

            if (_consecutiveErrors > 50)
            {
                System.Diagnostics.Process.Start(Application.ExecutablePath);
                System.IO.File.AppendAllText(System.IO.Path.Combine(GetDataFolder(), "restarts_" + Environment.MachineName + ".txt"), DateTime.Now + Environment.NewLine, Encoding.Default);
                Environment.Exit(0);
            }
        }

        private void LogException(Exception ex)
        {
            System.IO.File.AppendAllText(System.IO.Path.Combine(GetDataFolder(), "errors_" + Environment.MachineName + ".txt"),
                    DateTime.Now.ToString() + Environment.NewLine +
                ex.Message + Environment.NewLine +
                ex.StackTrace + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine + Environment.NewLine, Encoding.Default);
            _consecutiveErrors++;
        }

        private string GetDataFolder()
        {
            return Data.GetDataFolder(Data.DataFolders.GPSLog);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (_browsers == null) return;
            timer1.Interval = 30000;
            foreach (var browser in _browsers)
                if(!browser.Key.IsBusy)
                    browser.Key.Navigate(GetLineUrl(browser.Value));
                else
                    browser.Key.Stop();
        }

        private string GetLineFile(int line)
        {
            return System.IO.Path.Combine(GetDataFolder(), "Line_" + line + "_" + Environment.MachineName + "_" + DateTime.Now.ToString("yyyy-MM-dd") + ".csv");
        }

        private string GetLineUrl(int line)
        {
            return string.Format("http://st.atb.no/FlashNet/Proxy/ResourcesProxy.ashx?RequestType=VehiclesBasic&RequestFormat=GeoRss&LineID={0}&PathID=&VehicleID=&VehicleStatus=&ExternalCall=false", line);
        }
    }
}
