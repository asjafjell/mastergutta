﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Utilities.Extensions;
using System.ComponentModel.DataAnnotations;


namespace Utilities.Soccer
{
    public class SoccerGame
    {
        [Key]
        public int ID { get; set; }
        public DateTime Time { get; set; }
        public string HomeTeam { get; set; }
        public string AwayTeam { get; set; }
        public int Viewers { get; set; }

        private const string DateFormat = "yyyy-MM-dd HH:mm";

        public override string ToString()
        {
            return string.Format("'{0}', '{1}', '{2}'", Time.ToString(DateFormat), HomeTeam, AwayTeam);
        }

        public static string GetHeaders()
        {
            return string.Format("'{0}', '{1}', '{2}', '{3}", "Time", "HomeTeam", "AwayTeam", "Viewers");
        }

        public static IEnumerable<SoccerGame> ReadFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).Skip(1).Select(line => line.Split(";")).
                          Select(p => new SoccerGame()
                          {
                              Time = DateTime.ParseExact(p[0], "dd'.'MM'.'yyyy HH':'mm", CultureInfo.InvariantCulture),
                              HomeTeam = p[1],
                              AwayTeam = p[2],
                              Viewers = int.Parse(p[3])
                          });
        }
    }
}