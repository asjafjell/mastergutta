﻿namespace Utilities.PowerSet
{
    public enum SettingsAttribute
    {
        KeepAttributes = 0,
        RemoveAttributes = 1,
        FileIds = 2,
        Classifier = 3
    }
}
