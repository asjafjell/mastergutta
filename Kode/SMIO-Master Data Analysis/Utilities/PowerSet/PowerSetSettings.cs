﻿using System;
using System.IO;
using Utilities.Extensions;

namespace Utilities.PowerSet
{
    public class PowerSetSettings
    {

        private readonly string _path;

        public PowerSetSettings(String path)
        {
            _path = path;

            if (!File.Exists(path))
                throw new FileNotFoundException();
        }
        
        public string[] AttributeArray(SettingsAttribute attribute)
        {
            string[] data = Attribute(attribute).Split(';');

            if (data.Length == 1 && data[0] == (""))
                return new String[0];

            return data;
        }

        public string Attribute(SettingsAttribute attribute)
        {
            string line = File.ReadAllLines(_path)[(int) attribute];
            var str = attribute + ":";
            var contents = line.Split(str);

            if (contents.Length <= 1)
            {
                throw new Exception("The line does not contain data for " + attribute);
            }

            return contents[1].Trim();
        }
    }
}


