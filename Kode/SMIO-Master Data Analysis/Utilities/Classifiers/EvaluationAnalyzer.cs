﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.Utilities;
using Utilities.Extensions;
using Utilities.PowerSet;

namespace Utilities.Classifiers
{
    public class EvaluationAnalyzer
    {

        public const string SvmCKernelFile2 =    @"G:\MasterData\Z_Resultat\SVM - finn beste parametre\C, Kernel på representative datasett.txt";
        public const string SvmCKernelFile3 =    @"G:\MasterData\Z_Resultat\SVM - finn beste parametre\C 2_-15-2_15, RBF på representative datasett.txt";
        public const string SvmCKernelFile4 =    @"G:\MasterData\Z_Resultat\SVM - finn beste parametre\C 11 21, RBF på representative datasett.txt";
        public const string SvmCKernelFile5 = @"G:\MasterData\Z_Resultat\SVM - finn beste parametre\C 12 16, RBF på representative datasett.txt";
        public const string SvmCKernelFile6 = @"G:\MasterData\Z_Resultat\SVM - finn beste parametre\Grid_1_unfinished.txt";
        public const string SvmCKernelFile7 = @"G:\MasterData\Z_Resultat\SVM - finn beste parametre\Grid_4.txt";
        public const string SvmCKernelFile = @"G:\MasterData\Z_Resultat\SVM - finn beste parametre\Grid_6.txt";
        public const string KnnFile2 =            @"G:\MasterData\Z_Resultat\KNN - finn beste parametre\grid_3.txt";
        public const string KnnFile = @"G:\MasterData\Z_Resultat\KNN - finn beste parametre\grid_5.txt";
        
        public const string AnnHiddenNodesFile = @"C:\Master\RepoDataSync\weka\log\bestAttribute_Aleksander.txt.bak";
        public const string AnnHiddenNodesFile2 = @"C:\Master\RepoDataSync\weka\parameters\Ann\log 2014-05-05 15-49-00. Ann Node Kjøring 2.txt";
        public const string AnnLearningRateMomentumFile2 = @"C:\Master\RepoDataSync\weka\parameters\Ann\AnnLearningRateAndMomentumTesting2All.txt";
        public const string AnnEpochFile1 = @"C:\Master\RepoDataSync\weka\parameters\Ann\AnnEpochsTesting 1 -- 1-1000.txt";
        public const string AnnEpochFile2 = @"C:\Master\RepoDataSync\weka\parameters\Ann\AnnEpochsTesting 2 -- 1000-5000.txt";
        public const string AnnEpochFile3 = @"C:\Master\RepoDataSync\weka\parameters\Ann\AnnEpochsTesting3 -- 1-1100.txt";



        public static string SvmResultsToCsv2(string filePath)
        {
            var runs = File.ReadAllText(filePath, Encoding.Default).Split(Environment.NewLine.Repeat(2)).
                              Where(p => !string.IsNullOrEmpty(p)).
                              Select(p => p.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToArray()).
                              Select(c => new
                              {
                                  Kernel = c[0].Split(", ")[0],
                                  C = Math.Round(double.Parse(c[0].Split(", ")[1].Substring(2).Replace(".", ",")), 1),
                                  File = Path.GetFileName(c[0].Split('\'')[1]),

                                  Training = Find(c, "Training finished in "),
                                  Testing = Find(c, "Testing finished in "),
                                  Correlation = Find(c, "Correlation"),
                                  MAE = Find(c, "Mean absolute error"),
                                  RMSE = Find(c, "Root mean squared error"),
                                  RAE = Find(c, "Relative absolute error"),
                                  RRSE = Find(c, "Root relative squared error"),
                                  NumInstances = Find(c, "Total Number of Instances")
                              });

            runs = runs.Where(p => p.C != 0);
            runs = runs.Where(p => !p.File.EndsWith("569"));

            var byKernel = runs.GroupBy(p => p.Kernel).OrderBy(p => p.Key).ToList();
            var byC = runs.GroupBy(p => p.C).OrderBy(p => p.Key).ToList();
            var byKernelAndC = runs.GroupBy(p => p.Kernel).Select(p => new
            {
                Kernel = p.Key,
                Values = p.GroupBy(c => c.C).OrderBy(c => c.Key).Select(c => new[] {c.Key, c.Average(d => d.MAE)})
            })
                .Select(
                    p =>
                        new
                        {
                            p.Kernel,
                            CValues = p.Values.Select(c => c[0]).ToArray(),
                            CResults = p.Values.Select(c => c[1]).ToArray()
                        })
                .ToList();
            var byKernelFileAndC = runs.GroupBy(p => p.Kernel + ", " + p.File).Select(p => new
            {
                KernelFile = p.Key,
                Values = p.GroupBy(c => c.C).OrderBy(c => c.Key).Select(c => new[] {c.Key, c.Average(d => d.MAE)})
            })
                .Select(
                    p =>
                        new
                        {
                            p.KernelFile,
                            CValues = p.Values.Select(c => c[0]).ToArray(),
                            CResults = p.Values.Select(c => c[1]).ToArray()
                        })
                .ToList();
            var filesByKernel =
                runs.GroupBy(p => p.File)
                    .Select(
                        p =>
                            new
                            {
                                File = p.Key,
                                Kernels =
                                    p.GroupBy(c => c.Kernel)
                                        .OrderBy(c => c.Key)
                                        .Select(c => new {Kernel = c.Key, Average = c.Average(d => d.MAE)})
                            })
                    .ToList();

            var n = Environment.NewLine;
            return "Kernels" + n +
                   string.Join(";", byKernel.Select(p => p.Key)) + n +
                   string.Join(";", byKernel.Select(p => p.Average(c => c.MAE))) + n.Repeat(2) +
                   "C-values" + n +
                   ";" + string.Join(";", byC.Select(p => p.Key)) + n +
                   ";" + string.Join(";", byC.Select(p => p.Average(c => c.MAE))) + n.Repeat(2) +
                   "Kernel\\C-value;" + string.Join(";", byKernelAndC.First().CValues) + n +
                   string.Join(n,
                       byKernelAndC.Select(p => p.Kernel + ";" + string.Join(";", p.CResults))) + n.Repeat(2) +
                   "KernelFile\\C-value;" + string.Join(";", byKernelFileAndC.First().CValues) + n +
                   string.Join(n,
                       byKernelFileAndC.Select(p => p.KernelFile + ";" + string.Join(";", p.CResults))) + n.Repeat(2) +
                   "Files\\Kernels;" + n + string.Join(";", filesByKernel.First().Kernels.Select(p => p.Kernel)) + n +
                   string.Join(n,
                       filesByKernel.Select(p => p.File + ";" + string.Join(";", p.Kernels.Select(c => c.Average))));
        }

        public static string SvmResultsToCsvCGammaGrid1(string filePath)
        {
            var runs = File.ReadAllText(filePath, Encoding.Default).Replace(Environment.NewLine, "\n").Replace("\n", Environment.NewLine).Split(Environment.NewLine.Repeat(2)).
                            Where(p => !string.IsNullOrEmpty(p)).
                            Select(p => p.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToArray()).Where(p => p.Length != 0).
                            Select(c => new
                            {
                                Kernel = c[0].Split(", ")[0],
                                C = double.Parse(c[0].Split(", ")[1].Substring(2).Replace(".", ",")),
                                Gamma = double.Parse(c[0].Split(", ")[2].Substring(6).Replace(".", ",")),
                                File = Path.GetFileName(c[0].Split('\'')[1]),
                                OriginalTop = c[0],

                                Training = Find(c, "Training finished in "),
                                Testing = Find(c, "Testing finished in "),
                                Correlation = Find(c, "Correlation"),
                                MAE = Find(c, "Mean absolute error"),
                                RMSE = Find(c, "Root mean squared error"),
                                RAE = Find(c, "Relative absolute error"),
                                RRSE = Find(c, "Root relative squared error"),
                                NumInstances = Find(c, "Total Number of Instances")
                            });

            //runs = runs.Where(p => p.C != 0);
            //runs = runs.Where(p => !p.File.EndsWith("569"));

            var byGammaAndC = runs.GroupBy(p => new { p.C, p.Gamma }).Select(p => new { C = GammaizeGrid1(p.Key.C), Gamma = GammaizeGrid1(p.Key.Gamma), Mae = p.Average(c => c.MAE), Count = p.Count(), Files = p }).ToList();
            var maeRes = "";
            var counts = "";
            var n = Environment.NewLine;
            for (var c = -15; c <= 15; c++)
            {
                var line = "";
                var countLine = "";
                for (var g = -15; g <= 15; g++)
                {
                    var value = byGammaAndC.FirstOrDefault(p => p.C == c && p.Gamma == g);
                    line += (line == "" ? "" : ";") + (value == null ? "-" : value.Mae.ToString());
                    countLine += (countLine == "" ? "" : ";") + (value == null ? "0" : value.Count.ToString("n0"));
                }
                maeRes += (maeRes == "" ? "" : n) + line;
                counts += (counts == "" ? "" : n) + countLine;
            }

            var twenties = string.Join(n + n, runs.GroupBy(p => new { p.C, p.Gamma, p.File }).Where(p => p.Count() > 1).Select(p => string.Join(n, p.Select(c => c.OriginalTop))));
            var x = twenties.Length;

            return maeRes + n.Repeat(5) + counts;
        }

        public static string SvmResultsToCsvCGammaGrid2(string filePath)
        {
            var runs = File.ReadAllText(filePath, Encoding.Default).Replace(Environment.NewLine, "\n").Replace("\n", Environment.NewLine).Split(Environment.NewLine.Repeat(2)).
                            Where(p => !string.IsNullOrEmpty(p)).
                            Select(p => p.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToArray()).Where(p => p.Length != 0).
                            Select(c => new
                                {
                                    Kernel = c[0].Split(", ")[0],
                                    C = double.Parse(c[0].Split(", ")[1].Substring(2).Replace(".", ",")),
                                    Gamma = double.Parse(c[0].Split(", ")[2].Substring(6).Replace(".", ",")),
                                    File = Path.GetFileName(c[0].Split('\'')[1]),
                                    OriginalTop = c[0],
                                    Training = Find(c, "Training finished in "),
                                    Testing = Find(c, "Testing finished in "),
                                    Correlation = Find(c, "Correlation"),
                                    MAE = Find(c, "Mean absolute error"),
                                    RMSE = Find(c, "Root mean squared error"),
                                    RAE = Find(c, "Relative absolute error"),
                                    RRSE = Find(c, "Root relative squared error"),
                                    NumInstances = Find(c, "Total Number of Instances")
                                });

            //runs = runs.Where(p => p.C != 0);
            //runs = runs.Where(p => !p.File.EndsWith("569"));

            var byGammaAndC = runs.GroupBy(p => new { p.C, p.Gamma }).Select(p => new { C = GammaizeGrid2(p.Key.C), Gamma = GammaizeGrid2(p.Key.Gamma), Mae = p.Average(c => c.MAE), Count = p.Count(), Files = p }).ToList();
            var maeRes = "";
            var counts = "";
            var n = Environment.NewLine;
            for (decimal c = -1; c <= 4; c += 0.2m)
            {
                var line = "";
                var countLine = "";
                for (decimal g = -6; g <= -3; g += 0.2m)
                {
                    var value = byGammaAndC.FirstOrDefault(p => p.C == c && p.Gamma == g);
                    line += (line == "" ? "" : ";") + (value == null ? "-" : value.Mae.ToString());
                    countLine += (countLine == "" ? "" : ";") + (value == null ? "0" : value.Count.ToString("n0"));
                }
                maeRes += (maeRes == "" ? "" : n) + line;
                counts += (counts == "" ? "" : n) + countLine;
            }

            var twenties = string.Join(n + n, runs.GroupBy(p => new { p.C, p.Gamma, p.File }).Where(p => p.Count() > 1).Select(p => string.Join(n, p.Select(c => c.OriginalTop))));
            var x = twenties.Length;

            return maeRes + n.Repeat(5) + counts;
        }

        public static string SvmResultsToCsvCGammaGrid4(string filePath)
        {
            var runs = File.ReadAllText(filePath, Encoding.Default).Replace(Environment.NewLine, "\n").Replace("\n", Environment.NewLine).Split(Environment.NewLine.Repeat(2)).
                            Where(p => !string.IsNullOrEmpty(p)).
                            Select(p => p.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToArray()).Where(p => p.Length != 0).
                            Select(c => new
                            {
                                Kernel = c[0].Split(", ")[0],
                                C = double.Parse(c[0].Split(", ")[1].Substring(2).Replace(".", ",")),
                                Gamma = double.Parse(c[0].Split(", ")[2].Substring(6).Replace(".", ",")),
                                File = Path.GetFileName(c[0].Split('\'')[1]),
                                OriginalTop = c[0],

                                Training = Find(c, "Training finished in "),
                                Testing = Find(c, "Testing finished in "),
                                Correlation = Find(c, "Correlation"),
                                MAE = Find(c, "Mean absolute error"),
                                RMSE = Find(c, "Root mean squared error"),
                                RAE = Find(c, "Relative absolute error"),
                                RRSE = Find(c, "Root relative squared error"),
                                NumInstances = Find(c, "Total Number of Instances")
                            });

            //runs = runs.Where(p => p.C != 0);
            //runs = runs.Where(p => !p.File.EndsWith("569"));

            var byGammaAndC = runs.GroupBy(p => new { p.C, p.Gamma }).Select(p => new { C = GammaizeGrid4(p.Key.C), Gamma = GammaizeGrid4(p.Key.Gamma), Mae = p.Average(c => c.MAE), Count = p.Count(), Files = p }).ToList();
            var maeRes = "";
            var counts = "";
            var n = Environment.NewLine;

            for (decimal c = 2; c <= 3; c += 0.05m)
            {
                var line = "";
                var countLine = "";
                for (decimal g = -5.6m; g <= -5; g += 0.05m)
                {
                    var value = byGammaAndC.FirstOrDefault(p => p.C == c && p.Gamma == g);
                    line += (line == "" ? "" : ";") + (value == null ? "-" : value.Mae.ToString());
                    countLine += (countLine == "" ? "" : ";") + (value == null ? "0" : value.Count.ToString("n0"));
                }
                maeRes += (maeRes == "" ? "" : n) + line;
                counts += (counts == "" ? "" : n) + countLine;
            }

            var twenties = string.Join(n + n, runs.GroupBy(p => new { p.C, p.Gamma, p.File }).Where(p => p.Count() > 1).Select(p => string.Join(n, p.Select(c => c.OriginalTop))));
            var x = twenties.Length;

            return maeRes + n.Repeat(5) + counts;
        }

        public static string SvmResultsToCsvCGammaGrid5(string filePath)
        {
            var runs = File.ReadAllText(filePath, Encoding.Default).Replace(Environment.NewLine, "\n").Replace("\n", Environment.NewLine).Split(Environment.NewLine.Repeat(2)).
                            Where(p => !string.IsNullOrEmpty(p)).
                            Select(p => p.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToArray()).Where(p => p.Length != 0).
                            Select(c => new
                            {
                                Kernel = c[0].Split(", ")[0],
                                C = double.Parse(c[0].Split(", ")[1].Substring(2).Replace(".", ",")),
                                Gamma = double.Parse(c[0].Split(", ")[2].Substring(6).Replace(".", ",")),
                                File = Path.GetFileName(c[0].Split('\'')[1]),
                                OriginalTop = c[0],

                                Training = Find(c, "Training finished in "),
                                Testing = Find(c, "Testing finished in "),
                                Correlation = Find(c, "Correlation"),
                                MAE = Find(c, "Mean absolute error"),
                                RMSE = Find(c, "Root mean squared error"),
                                RAE = Find(c, "Relative absolute error"),
                                RRSE = Find(c, "Root relative squared error"),
                                NumInstances = Find(c, "Total Number of Instances")
                            });

            //runs = runs.Where(p => p.C != 0);
            //runs = runs.Where(p => !p.File.EndsWith("569"));

            var byGammaAndC = runs.GroupBy(p => new { p.C, p.Gamma }).Select(p => new { C = p.Key.C, Gamma = p.Key.Gamma, Mae = p.Average(c => c.MAE), Count = p.Count(), Files = p }).ToList();
            var maeRes = "";
            var counts = "";
            var n = Environment.NewLine;
            for (decimal c = 5.657m; c <= 6.062m; c += 0.05m)
            {
                var line = "";
                var countLine = "";
                for (decimal g = 0.0237m; g <= 0.0259m; g += 0.0005m)
                {
                    var value = byGammaAndC.FirstOrDefault(p => Math.Abs((decimal)p.C - c) < 0.0005m && Math.Abs((decimal)p.Gamma - g) < 0.0005m);
                    line += (line == "" ? "" : ";") + (value == null ? "-" : value.Mae.ToString());
                    countLine += (countLine == "" ? "" : ";") + (value == null ? "0" : value.Count.ToString("n0"));
                }
                maeRes += (maeRes == "" ? "" : n) + line;
                counts += (counts == "" ? "" : n) + countLine;
            }

            var twenties = string.Join(n + n, runs.GroupBy(p => new { p.C, p.Gamma, p.File }).Where(p => p.Count() > 1).Select(p => string.Join(n, p.Select(c => c.OriginalTop))));
            var x = twenties.Length;

            return maeRes + n.Repeat(5) + counts;
        }

        public static string SvmResultsToCsvCGammaGrid6(string filePath)
        {
            var runs = File.ReadAllText(filePath, Encoding.Default).Replace(Environment.NewLine, "\n").Replace("\n", Environment.NewLine).Split(Environment.NewLine.Repeat(2)).
                            Where(p => !string.IsNullOrEmpty(p)).
                            Select(p => p.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToArray()).Where(p => p.Length != 0).
                            Select(c => new
                            {
                                Kernel = c[0].Split(", ")[0],
                                C = double.Parse(c[0].Split(", ")[1].Substring(2).Replace(".", ",")),
                                Gamma = double.Parse(c[0].Split(", ")[2].Substring(6).Replace(".", ",")),
                                File = Path.GetFileName(c[0].Split('\'')[1]),
                                OriginalTop = c[0],

                                Training = Find(c, "Training finished in "),
                                Testing = Find(c, "Testing finished in "),
                                Correlation = Find(c, "Correlation"),
                                MAE = Find(c, "Mean absolute error"),
                                RMSE = Find(c, "Root mean squared error"),
                                RAE = Find(c, "Relative absolute error"),
                                RRSE = Find(c, "Root relative squared error"),
                                NumInstances = Find(c, "Total Number of Instances")
                            });

            //runs = runs.Where(p => p.C != 0);
            //runs = runs.Where(p => !p.File.EndsWith("569"));

            var byGammaAndC = runs.GroupBy(p => new { p.C, p.Gamma }).Select(p => new { C = p.Key.C, Gamma = p.Key.Gamma, Mae = p.Average(c => c.MAE), Count = p.Count(), Files = p }).ToList();
            var maeRes = "";
            var counts = "";
            var n = Environment.NewLine;
            for (decimal c = 5.657m; c < 5.758m; c += 0.01m)
            {
                var line = "";
                var countLine = "";
                for (decimal g = 0.0247m; g < 0.0258m; g += 0.0001m)
                {
                    var value = byGammaAndC.FirstOrDefault(p => Math.Abs((decimal)p.C - c) < 0.0001m && Math.Abs((decimal)p.Gamma - g) < 0.0001m);
                    line += (line == "" ? "" : ";") + (value == null ? "-" : value.Mae.ToString());
                    countLine += (countLine == "" ? "" : ";") + (value == null ? "0" : value.Count.ToString("n0"));
                }
                maeRes += (maeRes == "" ? "" : n) + line;
                counts += (counts == "" ? "" : n) + countLine;
            }

            var twenties = string.Join(n + n, runs.GroupBy(p => new { p.C, p.Gamma, p.File }).Where(p => p.Count() > 1).Select(p => string.Join(n, p.Select(c => c.OriginalTop))));
            var x = twenties.Length;

            return maeRes + n.Repeat(5) + counts;
        }

        public static string SvmResultsToCsvJobOverview(string filePath)
        {
            var runs = File.ReadAllText(filePath, Encoding.Default).Replace(Environment.NewLine, "\n").Replace("\n", Environment.NewLine).Split(Environment.NewLine.Repeat(2)).
                            Where(p => !string.IsNullOrEmpty(p)).
                            Select(p => p.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToArray()).Where(p => p.Length != 0).
                            Select(c => new
                            {
                                Kernel = c[0].Split(", ")[0],
                                C = double.Parse(c[0].Split(", ")[1].Substring(2).Replace(".", ",")),
                                Gamma = double.Parse(c[0].Split(", ")[2].Substring(6).Replace(".", ",")),
                                File = Path.GetFileName(c[0].Split('\'')[1]),

                                Training = Find(c, "Training finished in "),
                                Testing = Find(c, "Testing finished in "),
                                Correlation = Find(c, "Correlation"),
                                MAE = Find(c, "Mean absolute error"),
                                RMSE = Find(c, "Root mean squared error"),
                                RAE = Find(c, "Relative absolute error"),
                                RRSE = Find(c, "Root relative squared error"),
                                NumInstances = Find(c, "Total Number of Instances")
                            });

            return string.Join(Environment.NewLine, runs.Select(p => GammaizeGrid1(p.C) + ";" + GammaizeGrid1(p.Gamma) + ";" + p.File + ";" + p.MAE));
        }

        private static decimal GammaizeGrid4(double gamma)
        {
            for (decimal i = -8; i <= 8; i += 0.05m)
            {
                if (Math.Abs(Math.Pow(2, (double)i) - gamma) < 0.000000005) return i;
            }
            throw new Exception("Invalid gamma.");
        }

        private static decimal GammaizeGrid2(double gamma)
        {
            for (decimal i = -8; i <= 8; i += 0.2m)
            {
                if (Math.Abs(Math.Pow(2, (double)i) - gamma) < 0.00000005) return i;
            }
            throw new Exception("Invalid gamma.");
        }

        private static int GammaizeGrid1(double gamma)
        {
            for (var i = -15; i <= 15; i++)
            {
                if (Math.Abs(Math.Pow(2, i) - gamma) < 0.0000005) return i;
            }
            throw new Exception("Invalid gamma.");
        }

        public static string KnnResultsToCsv(string filePath)
        {
            var runs = File.ReadAllText(filePath, Encoding.Default).
                            Replace(Environment.NewLine, "\n").
                            Replace("\n", Environment.NewLine).
                            Split(Environment.NewLine.Repeat(2)).
                            Where(p => !string.IsNullOrEmpty(p)).
                            Select(p => p.Split(Environment.NewLine).Where(c => !string.IsNullOrEmpty(c)).ToArray()).
                            Where(p => p.Length != 0).
                            GroupBy(p => p[0]).
                            Select(p => p.First()).
                            Select(c => new
                                {
                                    Algorithm = c[0].Split(", ")[0].Substring(10),
                                    Distance = c[0].Split(", ")[1].Substring(9),
                                    ActualK = int.Parse(c[0].Split(", ")[2].Substring(2)),
                                    K = int.Parse(c[0].Split(", ")[3].Substring(3)),
                                    File = Path.GetFileName(c[0].Split('\'')[1]),

                                    Training = Find(c, "Training finished in "),
                                    Testing = Find(c, "Testing finished in "),
                                    Correlation = Find(c, "Correlation"),
                                    MAE = Find(c, "Mean absolute error"),
                                    RMSE = Find(c, "Root mean squared error"),
                                    RAE = Find(c, "Relative absolute error"),
                                    RRSE = Find(c, "Root relative squared error"),
                                    NumInstances = Find(c, "Total Number of Instances")
                                });

            //runs = runs.Where(p => p.C != 0);
            //runs = runs.Where(p => !p.File.EndsWith("304"));
            //runs = runs.Where(p => !p.File.EndsWith("378"));
            //runs = runs.Where(p => !p.File.EndsWith("427"));
            //runs = runs.Where(p => !p.File.EndsWith("595"));

            var byFileAndK = runs.GroupBy(p => p.File)
                .ToDictionary(p => p.Key, p => p.GroupBy(c => c.K).ToDictionary(k => k.Key, c => c.Average(d => d.MAE)));
            var byFileAndAlgorithm = runs.GroupBy(p => p.File)
                .ToDictionary(p => p.Key,
                    p => p.GroupBy(c => c.Algorithm).ToDictionary(k => k.Key, c => c.Average(d => d.MAE)));
            var byFileAndDistance = runs.GroupBy(p => p.File)
                .ToDictionary(p => p.Key,
                    p => p.GroupBy(c => c.Distance).ToDictionary(k => k.Key, c => c.Average(d => d.MAE)));
            var byAlgorithmAndK = runs.GroupBy(p => p.Algorithm)
                .ToDictionary(p => p.Key, p => p.GroupBy(c => c.K).ToDictionary(k => k.Key, c => c.Average(d => d.MAE)));
            var byAlgorithmAndDistance = runs.GroupBy(p => p.Algorithm)
                .ToDictionary(p => p.Key,
                    p => p.GroupBy(c => c.Distance).ToDictionary(k => k.Key, c => c.Average(d => d.MAE)));

            var best = runs.GroupBy(p => new {p.Algorithm, p.Distance, p.K}).
                Select(
                    p =>
                        new
                        {
                            Point = p.Key,
                            Average = p.Average(c => c.MAE),
                            Std = p.Select(c => c.MAE).StandardDeviation()
                        }).
                Select(p => new {p.Average, p.Std, p.Point, Score = p.Average + p.Std}).
                OrderBy(p => p.Score);

            var n = Environment.NewLine;
            return "File\\K;" + Csvify(byFileAndK) + n.Repeat(3) +
                   "File\\Algorithm;" + Csvify(byFileAndAlgorithm) + n.Repeat(3) +
                   "File\\Distance;" + Csvify(byFileAndDistance) + n.Repeat(3) +
                   "Algorithm\\K;" + Csvify(byAlgorithmAndK) + n.Repeat(3) +
                   "Algorithm\\Distance;" + Csvify(byAlgorithmAndDistance) + n.Repeat(3) +
                   "Absolute minimum" + n + "Algorithm;Distance;K;MAE;STD;Score" + n +
                   string.Join(n,
                       best.Take(50)
                           .Select(
                               p =>
                                   p.Point.Algorithm + ";" + p.Point.Distance + ";" + p.Point.K + ";" + p.Average + ";" +
                                   p.Std + ";" + p.Score));
        }

        private static string Csvify(Dictionary<string, Dictionary<int, double>> dict)
        {
            string n = Environment.NewLine;
            return string.Join(";", dict[dict.Keys.First()].Select(p => p.Key)) + n +
                   string.Join(n, dict.Select(p => p.Key + ";" + string.Join(";", p.Value.Select(c => c.Value))));
        }

        private static string Csvify(Dictionary<string, Dictionary<string, double>> dict)
        {
            string n = Environment.NewLine;
            return string.Join(";", dict[dict.Keys.First()].Select(p => p.Key)) + n +
                   string.Join(n, dict.Select(p => p.Key + ";" + string.Join(";", p.Value.Select(c => c.Value))));
        }

        public static string AnnNodeResultsToCsv(string filePath)
        {
            var s = File.ReadAllText(filePath, Encoding.Default).Replace("\r\n", "\n").Split("\n".Repeat(2)).
                Where(p => !string.IsNullOrEmpty(p)).
                Select(p => p.Split("\n").Where(c => !string.IsNullOrEmpty(c)).ToArray());

            int count = s.Count();

            var runs = s.
                Select(c => new
                {
                    Neurons = c[0].Split(", ")[0].Split("Neurons:")[1],
                    LearningRate =
                        Math.Round(Double.Parse(c[0].Split(", ")[1].Split("LearningRate:")[1].Replace(".", ",")), 1),
                    Momentum = Math.Round(Double.Parse(c[0].Split(", ")[2].Split("Momentum:")[1].Replace(".", ",")), 1),
                    Epochs = c[0].Split(", ")[3].Split("Epochs:")[1],
                    File = String.Join("", c[0].Split("finalDataSet\\")[1].Take(3)),
                    Training = Find(c, "Training finished in "),
                    Testing = Find(c, "Testing finished in "),
                    Correlation = Find(c, "Correlation"),
                    MAE = Find(c, "Mean absolute error"),
                    RMSE = Find(c, "Root mean squared error"),
                    RAE = Find(c, "Relative absolute error"),
                    RRSE = Find(c, "Root relative squared error"),
                    NumInstances = Find(c, "Total Number of Instances")
                });

            runs.ToList();

            var files = runs.GroupBy(res => res.File).Select(p => new
            {
                file = p.Key,
                elements = p.OrderByDescending(elem => elem.Neurons.Length).ThenByDescending(elem => elem.Neurons)
            });

            List<string> output = new List<string>();

            string headers = "File;";
            headers += String.Join(";", files.ElementAt(0).elements.Select(elm => elm.Neurons).ToList());

            output.Add(headers);

            foreach (var file in files)
            {
                List<double> results = file.elements.Select(r => r.MAE).ToList();
                var row = file.file + ";" + String.Join(";", results);
                output.Add(row);
            }

            File.WriteAllLines(Data.GetWekaFile("parameters", "ann", "AnnHiddenNodes2.txt"), output);

            return "I is finished. Lol. You thought this was the results. Fooled you. I saved to file";
        }

        public static string AnnLearningrateAndMomentumResultsToCsv(string filePath)
        {
            var s = File.ReadAllText(filePath, Encoding.Default).Replace("\r\n","\n").Split("\n".Repeat(2)).
                Where(p => !string.IsNullOrEmpty(p)).
                Select(p => p.Split("\n").Where(c => !string.IsNullOrEmpty(c)).ToArray());

            int i = s.Count();

            var runs = s.
                Select(c => new
                {
                    Neurons = c[0].Split(", ")[0].Split("Neurons:")[1],
                    LearningRate =
                        Math.Round(Double.Parse(c[0].Split(", ")[1].Split("LearningRate:")[1].Replace(".", ",")), 1),
                    Momentum = Math.Round(Double.Parse(c[0].Split(", ")[2].Split("Momentum:")[1].Replace(".", ",")), 1),
                    Epochs = c[0].Split(", ")[3].Split("Epochs:")[1],
                    File = String.Join("", c[0].Split("finalDataSet\\")[1].Take(3)),
                    Training = Find(c, "Training finished in "),
                    Testing = Find(c, "Testing finished in "),
                    Correlation = Find(c, "Correlation"),
                    MAE = Find(c, "Mean absolute error"),
                    RMSE = Find(c, "Root mean squared error"),
                    RAE = Find(c, "Relative absolute error"),
                    RRSE = Find(c, "Root relative squared error"),
                    NumInstances = Find(c, "Total Number of Instances")
                }).ToList();

            var networks = runs.GroupBy(res => res.Neurons).Select(p => new
            {
                neurons = p.Key,
                elements = p
            });

            List<string> output = new List<string>();

            int count = runs.Count();

            //First, go through the five networks
            foreach (var network in networks)
            {
                output.Add("Network: " + network.neurons);

                var lrGroups = network.elements.GroupBy(x => new { x.LearningRate, x.Momentum },
                    (key, group) => new
                    {
                        key.Momentum,
                        key.LearningRate,
                        MAEAverage = group.Average(p => p.MAE),
                        STDAverage = group.Select(p => p.MAE).StandardDeviation(),
                        TrainingTimeAverage = group.Average(p => p.Training)
                    }).GroupBy(lrGroup => lrGroup.LearningRate).OrderBy(grp => grp.Key);

                string lrGroupsHeader = " ;" +
                                        string.Join(";", lrGroups.ElementAt(0).Select(e => e.Momentum).OrderBy(p => p));
                output.Add(lrGroupsHeader);

                foreach (var lrGroup in lrGroups)
                {
                    string res = lrGroup.ElementAt(0).LearningRate.ToString("n1") + ";";
                    res += String.Join(";",
                        lrGroup.OrderBy(grp => grp.Momentum).Select(p => (p.MAEAverage + p.STDAverage).ToString("n5")));
                    output.Add(res);
                }

                Debug.WriteLine("network average time (" + network.neurons + ") " +
                                lrGroups.Where(grp => grp.Key <= 0.4)
                                    .Select(
                                        grp =>
                                            grp.Where(p => p.Momentum <= 0.8)
                                                .Select(row => row.TrainingTimeAverage)
                                                .Average(p => p))
                                    .Average(p => p));

                output.Add("");
            }

            File.WriteAllLines(Data.GetWekaFile("parameters", "ann","AnnLearningRateMomentumRatingCsv.csv"),output);

            return "I is finished. Lol. Fooled you.";
        }

        public static string AnnEpochResultsToCsv(string filePath)
        {
            var s = File.ReadAllText(filePath, Encoding.Default).Replace("\r\n", "\n").Split("\n".Repeat(2)).
                Where(p => !string.IsNullOrEmpty(p)).
                Select(p => p.Split("\n").Where(c => !string.IsNullOrEmpty(c)).ToArray());

            int i = s.Count();

            var runs = s.
                Select(c => new
                {
                    Neurons = c[0].Split(", ")[0].Split("Neurons:")[1],
                    LearningRate =
                        Math.Round(Double.Parse(c[0].Split(", ")[1].Split("LearningRate:")[1].Replace(".", ",")), 1),
                    Momentum = Math.Round(Double.Parse(c[0].Split(", ")[2].Split("Momentum:")[1].Replace(".", ",")), 1),
                    Epochs = c[0].Split(", ")[3].Split("Epochs:")[1],
                    File = String.Join("", c[0].Split("finalDataSet\\")[1].Take(3)),
                    Training = Find(c, "Training finished in "),
                    Testing = Find(c, "Testing finished in "),
                    Correlation = Find(c, "Correlation"),
                    MAE = Find(c, "Mean absolute error"),
                    RMSE = Find(c, "Root mean squared error"),
                    RAE = Find(c, "Relative absolute error"),
                    RRSE = Find(c, "Root relative squared error"),
                    NumInstances = Find(c, "Total Number of Instances")
                }).ToList();

            var networks = runs.GroupBy(res => res.Neurons).Select(p => new
            {
                neurons = p.Key,
                elements = p
            });

            List<string> output = new List<string>();

            int count = runs.Count();

            //First, go through the five networks
            foreach (var network in networks)
            {
                output.Add("Network: " + network.neurons);

                var fileGroups = network.elements.GroupBy(n => n.File).Select(fGroup => new
                {
                    File = fGroup.Key,
                    Elements = fGroup.OrderBy(p => p.Epochs)
                });


                string lrGroupsHeader = "File ;" + String.Join(";", fileGroups.ElementAt(0).Elements.Select(e => e.Epochs));
                output.Add(lrGroupsHeader);

                foreach (var fileGroup in fileGroups)
                {
                    string res = fileGroup.Elements.ElementAt(0).File + ";";
                    res += String.Join(";", fileGroup.Elements.Select(e => e.MAE.ToString("n5")));
                        
                    output.Add(res);
                }
                
                output.Add("");
                output.Add("");
                output.Add("");
                output.Add("");

            }

            File.WriteAllLines(Data.GetWekaFile("parameters", "ann", "AnnEpochCsv3.csv"), output);

            return "I is finished. Lol. Fooled you.";
        }

        /// <summary>
        /// Finds the single item in the enumerable that contains the key, and extracts a number from that line.
        /// </summary>
        /// <param name="enumerable"></param>
        /// <param name="key"></param>
        /// <returns></returns>
        public static double Find(IEnumerable<string> enumerable, string key)
        {
            return double.Parse(string.Join("", enumerable.Single(p => p.Contains(key)).ToCharArray().Where(p => char.IsDigit(p) || p == '.' || p == '-')).Replace(".", ","));
        }
    }




}
