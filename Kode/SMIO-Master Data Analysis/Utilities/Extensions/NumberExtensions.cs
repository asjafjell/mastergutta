﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Utilities.Extensions
{
    public static class NumberExtensions
    {
        /// <summary>
        /// Returns the population standard deviation of the given collection of numbers.
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public static double StandardDeviation(this IEnumerable<double> numbers)
        {
            if (numbers.Count() < 2) return double.NaN;
            var average = numbers.Average();
            return Math.Sqrt(numbers.Sum(p => Math.Pow(p - average, 2))/(numbers.Count()));
        }

        /// <summary>
        /// Returns the population standard deviation of the given collection of numbers.
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public static double StandardDeviation(this IEnumerable<decimal> numbers)
        {
            return numbers.Select(p => (double)p).StandardDeviation();
        }

        /// <summary>
        /// Returns the population standard deviation of the given collection of numbers.
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public static double StandardDeviation(this IEnumerable<float> numbers)
        {
            return numbers.Select(p => (double)p).StandardDeviation();
        }

        /// <summary>
        /// Returns the population standard deviation of the given collection of numbers.
        /// </summary>
        /// <param name="numbers"></param>
        /// <returns></returns>
        public static double StandardDeviation(this IEnumerable<int> numbers)
        {
            return numbers.Select(p => (double)p).StandardDeviation();
        }

        public static double[][] Normalize(this double[][] numbers)
        {
            var limits = numbers[0].Select(p => new MinMax(p, p)).ToArray();
            for (var i = 1; i < numbers.Length; i++)
                for (var j = 0; j < numbers[i].Length; j++)
                    limits[j].Update(numbers[i][j]);

            var normalized = numbers.Select(p=>new double[p.Length]).ToArray();
            for (var i = 1; i < numbers.Length; i++)
                for (var j = 0; j < numbers[i].Length; j++)
                    normalized[i][j] = limits[j].Max == limits[j].Min ? limits[j].Max : (numbers[i][j] - limits[j].Min)/(limits[j].Max - limits[j].Min);

            return normalized;
        }
    }

    public class MinMax
    {
        public double Min { get; set; }
        public double Max { get; set; }

        public MinMax(double min, double max)
        {
            Min = min;
            Max = max;
        }

        public void Update(double value)
        {
            Max = Math.Max(Max, value);
            Min = Math.Min(Min, value);
        }
    }
}
