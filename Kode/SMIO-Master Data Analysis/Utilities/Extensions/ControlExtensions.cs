﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Utilities.Extensions
{
    public static class ControlExtensions
    {
        public static IEnumerable<TextBox> GetTextBoxes(this Control c)
        {
            return c.GetControlsRecursive().OfType<TextBox>().OrderBy(p => p.PointToScreen(Point.Empty).Y).ToList();
        }

        public static IEnumerable<Label> GetLabels(this Control c)
        {
            return c.GetControlsRecursive().OfType<Label>().OrderBy(p => p.PointToScreen(Point.Empty).Y).ToList();
        }

        public static IEnumerable<Tuple<string, string>> GetInputValues(this Control c)
        {
            var labels = c.GetLabels();
            var controls = c.GetControlsRecursive().Where(p => !(p is Label)).ToList();

            foreach (var label in labels)
            {
                var l = label;
                var closest = controls.
                    Select(p => new
                        {
                            Control = p,
                            Value = l.PointToScreen(Point.Empty).Y - p.PointToScreen(Point.Empty).Y
                        }).
                    Select(p => new
                        {
                            p.Control,
                            Value = Math.Abs(p.Value),
                            Negative = p.Value < 0
                        }).
                    OrderBy(p => p.Value).
                    First();

                yield return new Tuple<string, string>(l.Text, GetValue(closest.Control));
            }
        }

        private static string GetValue(Control c)
        {
            if (c is TextBox || c is Label || c is Button || c is Form || c is GroupBox)
                return c.Text;
            if (c is DateTimePicker)
                return ((DateTimePicker) c).Value.ToString(((DateTimePicker) c).CustomFormat);
            if (c is ComboBox)
                return string.Format("{0} ({1})", ((ComboBox) c).SelectedItem, ((ComboBox) c).SelectedIndex);

            return "[Unknown value]";
        }

        public static IEnumerable<Control> GetControlsRecursive(this Control c)
        {
            if (c.Controls.Count == 0) return new[] { c };
            return c.Controls.Cast<Control>().SelectMany(GetControlsRecursive);
        }
    }
}
