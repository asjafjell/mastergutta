﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace Utilities.AtB.Lines
{
    public class Line
    {
        [Key]
        public string ID { get; set; }
        public virtual ICollection<BusStop> Stops { get; set; }

        public static void GenerateTable()
        {
            DbHelper.Reset(Data.DataFolders.Lines);

            Console.WriteLine("Generating line table.");

            var db = new SMIODataContext();
            var lineIDs = db.PassageItems.Select(p => p.Line).Distinct().ToList();
            lineIDs.AddRange(db.RealTimeItems.Select(p => p.Line).Distinct());
            lineIDs = lineIDs.Distinct().ToList();

            var lines = lineIDs.Select(p => new Line() {ID = p, Stops = new List<BusStop>()}).ToList();

            Console.WriteLine("  > Found {0:n0} lines.", lines.Count);

            var count = 0;
            foreach (var line in lines)
            {
                var stopIDs = db.PassageItems.Where(p => p.Line == line.ID).Select(p => p.BusStopCode).Distinct().ToList();
                var rtIDs = db.RealTimeItems.Where(p=>p.Line==line.ID).Select(p=>p.StopID).Distinct().ToList();
                line.Stops = db.BusStops.Where(p => stopIDs.Contains(p.ID) || rtIDs.Contains(p.RealTimeID)).ToList();

                db.Lines.Add(line);
                db.SaveChanges();

                Console.WriteLine("  > Found {0:n0} stops for line {1} ({2}/{3}).", line.Stops.Count, line.ID, ++count, lines.Count);
            }
        }

        public static void CheckLines(SMIODataContext db)
        {
            if(!db.Lines.Any())
                GenerateTable();
        }

        public static List<Trip> GetTrips(int line)
        {
            return GetTrips(line.ToString());
        }

        public static List<Trip> GetTrips(string line)
        {
            var db = new SMIODataContext();
            var start = DateTime.Now.AddDays(-30).Date;
            var trips = db.FullTripItems.
                           Where(p => p.Date >= start && p.Line == line).
                           GroupBy(p => p.DepartureTerminal.ID + "|" + p.ArrivalTerminal.ID).
                           Select(p => p.FirstOrDefault()).
                           ToList().
                           Select(p => new Trip(p.Trip, p.DepartureTerminal, p.ArrivalTerminal, db)).
                           ToList();
            return trips;
        } 
    }

    public class Trip
    {
        public string TripID { get; set; }
        public List<BusStop> Stops { get; set; }

        public BusStop StartStop { get { return Stops == null || !Stops.Any() ? null : Stops.First(); } }
        public BusStop EndStop { get { return Stops == null || !Stops.Any() ? null : Stops.Last(); } }

        public bool IsValid { get; private set; }

        public Trip(string tripID, BusStop start, BusStop end, SMIODataContext db)
        {
            TripID = tripID;

            Stops = new List<BusStop>();
            var stop = start;
            do
            {
                Stops.Add(stop);
                if (stop == end) break;
                stop = Next(Stops, tripID, stop, db);
                if (stop == null) break;
            } while (true);

            IsValid = start == StartStop && end == EndStop;
        }

        private BusStop Next(List<BusStop> routeThisFar, string tripID, BusStop stop, SMIODataContext db)
        {
            var start = DateTime.Now.AddDays(-30).Date;
            var stops = db.LTTItems.
                           Where(p => p.ActualDeparture >= start && p.Trip == tripID && p.BusStopFrom.ID == stop.ID).
                           GroupBy(p => p.BusStopTo).
                           OrderByDescending(p => p.Count()).
                           ToList().
                           Where(p => routeThisFar.All(c => c.ID != p.Key.ID)).
                           Select(p => new Tuple<BusStop, int>(p.Key, p.Count())).
                           ToList();

            return stops.Count == 0 ? null : stops.First().Item1;
        }
    }
}
