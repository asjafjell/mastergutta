﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json.Linq;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace Utilities.AtB.RealTime
{
    public class RealTimeItem : DbLoader
    {
        [Key]
        public int ID { get; set; }

        public string StopID { get; set; }
        public DateTime QueryTime { get; set; }

        public string Line { get; set; }
        public DateTime Arrival { get; set; }
        public RealTimeTypes Type { get; set; }

        public enum RealTimeTypes
        {
            Prediction,
            Schedule
        }

        public static List<RealTimeItem> Realtime(string id, WebClient wc = null)
        {
            if (wc == null) wc = new WebClient();
            var jsonString = wc.DownloadString("https://www.atb.no/xmlhttprequest.php?service=realtime.getBusStopRealtimeForecast&busStopId=" + Uri.EscapeDataString(id));
            if(string.IsNullOrEmpty(jsonString)) return new List<RealTimeItem>();
            dynamic json = JObject.Parse(jsonString.Substring(1, jsonString.Length - 2));
            var items = new List<RealTimeItem>();
            foreach (var p in json.forecast)
            {
                items.Add(new RealTimeItem()
                    {
                        Line = p.rute,
                        Arrival = ParseDateTime((string) p.ankomst),
                        Type = p.type == "Prev" ? RealTimeTypes.Prediction : RealTimeTypes.Schedule
                    });
            }

            var tomorrow = false;
            for (var i = 1; i < items.Count; i++)
            {
                if (items[i].Arrival < items[i - 1].Arrival)
                    tomorrow = true;
                if (tomorrow)
                    items[i].Arrival = items[i].Arrival.AddDays(1);
            }

            return items;
        }

        private static DateTime ParseDateTime(string ankomst)
        {
            TimeSpan t = DateTime.ParseExact(ankomst, "HH:mm", CultureInfo.InvariantCulture).TimeOfDay;
            var n = DateTime.Now;
            return new DateTime(n.Year, n.Month, n.Day, t.Hours, t.Minutes, 0);
        }

        public override string ToString()
        {
            return Line + ", " + Arrival.ToString("HH:mm");
        }
        public static IEnumerable<RealTimeItem> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).Where(p => !string.IsNullOrEmpty(p)).Select(line => line.Split('|')).SelectMany(LoadItems);
        }

        private static IEnumerable<RealTimeItem> LoadItems(string[] p)
        {
            var stop = p[0];
            var date = new DateTime(long.Parse(p[1]));
            foreach (var line in p.Skip(2))
            {
                var parts = line.Split(';');
                var lineID = parts[0];
                var times = parts.Skip(1).Select(c =>
                                                 new RealTimeItem()
                                                 {
                                                     StopID = stop,
                                                     QueryTime = date,
                                                     Arrival = new DateTime(long.Parse(c)),
                                                     Line = lineID,
                                                     Type = RealTimeTypes.Prediction
                                                 }
                    );
                foreach (var realTimeItem in times)
                    yield return realTimeItem;
            }
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.RealTime);
            var files = System.IO.Directory.GetFiles(path, "*.txt").ToList();

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.RealTime, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = FromFile(file).ToList();

                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[RealTimeItems]([Line],[Arrival],[Type],[StopID],[QueryTime]) VALUES('{0}','{1}',{2},'{3}','{4}');",
                    p.Line, p.Arrival.ToString("yyyy-MM-dd HH:mm:ss.fff"), (int)p.Type, p.StopID, p.QueryTime.ToString("yyyy-MM-dd HH:mm:ss.fff"))), Encoding.Default);
            }
        }

        public static void HealBusStopIDChanges()
        {
            var path = Data.GetDataFolder(Data.DataFolders.RealTime);
            var files = System.IO.Directory.GetFiles(path, "*.txt").ToList();
            
            Console.WriteLine("Healing bus stop changes.");

            var changes = System.IO.File.ReadAllLines(@"G:\MasterData\busstops\BusStopID changes at 2014-02-24.txt", Encoding.Default).
                                 Where(p => !string.IsNullOrEmpty(p) && p.Contains(";")).
                                 Select(p => p.Split(';')).
                                 Where(p => p.Length == 2 && p[0].Length > 0 && p[1].Length > 0).
                                 Select(p => new {Old = p[0], New = p[1]}).
                                 ToList();

            Console.WriteLine("  > Read change map.");

            foreach (var file in files)
            {
                Console.Write("  > Working on file '{0}'.", System.IO.Path.GetFileName(file));
                var sb = new StringBuilder();
                foreach (var line in System.IO.File.ReadAllLines(file,Encoding.Default))
                {
                    var parts = line.Split('|');
                    var hit = changes.SingleOrDefault(p => p.Old == parts[0]);
                    if (hit != null)
                        parts[0] = hit.New;
                    sb.AppendLine(string.Join("|", parts));
                }
                System.IO.File.WriteAllText(file, sb.ToString(), Encoding.Default);
                Console.Write("\r  > Healed file '{0}'.{1}", System.IO.Path.GetFileName(file), Environment.NewLine);
            }

            Console.WriteLine("Done.");
        }

        /*public static int CountFilesToBeImported(bool ignoreImported = true)
        {
            var path = Data.GetDataFolder(Data.DataFolders.RealTime);
            var db = new SMIODataContext();
            var files = System.IO.Directory.GetFiles(path, "*.txt").ToList();

            if (ignoreImported && files.Count > 0)
                files = files.Where(p => !ImportHistoryItem.IsImported(db, p)).ToList();

            return files.Count;
        }

        public static void ImportData(bool ignoreImported = true)
        {
            var path = Data.GetDataFolder(Data.DataFolders.RealTime);

            var db = new SMIODataContext();

            var files = System.IO.Directory.GetFiles(path, "*.txt").ToList();

            if (ignoreImported && files.Count > 0)
            {
                var count = files.Count;
                files = files.Where(p => !ImportHistoryItem.IsImported(db, p)).ToList();
                if (count - files.Count > 0 && files.Count > 0)
                    Console.WriteLine("Ignored {0} already imported files.", count - files.Count);
            }

            if (files.Count == 0) return;

            var remove = new List<string>();
            var add = new List<string>();
            foreach (var file in files)
            {
                var info = new System.IO.FileInfo(file);
                if (info.Length > 2000000)
                {
                    remove.Add(file);
                    add.AddRange(DbHelper.SplitFiles(System.IO.Path.GetDirectoryName(file), new[] {file}, false, false, false));
                }
            }

            remove.ForEach(p => System.IO.File.Move(p, p + ".bak"));
            remove.ForEach(p => files.Remove(p));
            files.AddRange(add);

            foreach (var file in files)
            {
                var items = FromFile(file).ToList();

                if (items.Count == 0) continue;
                Console.WriteLine("Found {0:n0} items.", items.Count);

                if (items.Count > 0)
                {
                    db.RealTimeItems.AddRange(items);
                    db.SaveChanges();
                    db.Dispose();
                    db = new SMIODataContext();

                    Console.WriteLine("  > Inserted {0:n0} new items.", items.Count);
                }

                if (ignoreImported)
                {
                    files.ToList().ForEach(p => ImportHistoryItem.Import(db, p));
                    db.SaveChanges();
                }
            }

            Console.WriteLine();
        }*/
    }
}
