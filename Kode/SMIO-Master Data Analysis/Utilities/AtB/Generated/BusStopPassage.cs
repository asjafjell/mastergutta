﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using Utilities.AtB.Passage;
using Utilities.AtB.Utilities;
using Utilities.Database;
using Utilities.Extensions;

namespace Utilities.AtB.Generated
{
    public class BusStopPassage : DbLoader
    {
        private const int InvalidData = -999999;

        [Key]
        public int ID { get; set; }

        public DateTime ScheduledArrivalDateTime { get; set; }
        public int DayOfWeek { get; set; }
        public int ActualArrival { get; set; }
        public int ScheduledArrival { get; set; }
        public int TimeOfDayQuarter { get; set; }
        public int TimeOfDayHalfHour { get; set; }
        public int TimeOfDayHour { get; set; }
        public int StopTime { get; set; }
        public int HeadWay { get; set; }
        public int Delay { get; set; }
        public int Vehicle { get; set; }
        public int PassengersPresent { get; set; }
        public double Temperature { get; set; }
        public double Precipitation { get; set; }
        public double Wind { get; set; }
        public int Humidity { get; set; }
        public int SoccerGame { get; set; }
        public int DelayLast { get; set; }
        public int AverageDelayLast3 { get; set; }
        public int DelayYesterday { get; set; }
        public int DelayDayBeforeYesterday { get; set; }
        public int DelayLastWeek { get; set; }
        public int AverageDelayLast3OfSameDay { get; set; }
        public string BusStopCode { get; set; }
        public int TicketsSoldLastHour { get; set; }
        public int TicketsSoldLastThreeHours { get; set; }

        public static void Generate(bool reset = false)
        {
            var line = 8;
            //if(reset) DbHelper.Reset(Data.DataFolders.BusStopPassages);
            //FetchPassages(line.ToString());
            AddTicketData(line.ToString());
        }

        private static void FetchPassages(string line)
        {
            Console.WriteLine("Fetching passages ({0:HH:mm}).", DateTime.Now);
            Console.Write(" > 0");
            var db = new SMIODataContext();
            var objCtx = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)db).ObjectContext;
            var passages = db.PassageItems.Where(p => p.Line == line).OrderBy(p => p.ScheduledArrival).ToList();
            var items = new List<BusStopPassage>();
            var soccers = db.SoccerGames.ToList();
            var delays = new List<DelayHolder>();
            foreach (var passage in passages)
            {
                float tod = passage.ScheduledArrival.Hour*60 + passage.ScheduledArrival.Minute;

                var wo = db.WeatherObjects.FromHour(passage.ActualArrival);
                
                var soccer = 0;
                var game = soccers.FirstOrDefault(p => Math.Abs(passage.ActualArrival.Subtract(p.Time.AddHours(1)).TotalHours) < 2.5);
                if (game != null) soccer = game.Viewers;

                var pc = db.PassengerCounts.FirstOrDefault(p => 
                    p.Trip == passage.Trip && 
                    DbFunctions.TruncateTime(p.DateAndTime) == DbFunctions.TruncateTime(passage.ActualArrival) && 
                    p.Stop.ID == passage.BusStopCode &&
                    p.Vehicle == passage.Vehicle);
                var passengerCount = pc == null ? -1 : pc.PassengersPresent;

                var selectedDelays = delays.Where(p => p.BusStopCode == passage.BusStopCode).ToList();

                var item = new BusStopPassage()
                    {
                        ActualArrival = passage.ActualArrival.Hour*60 + passage.ActualArrival.Minute,
                        ScheduledArrival = (int) tod,
                        ScheduledArrivalDateTime = passage.ScheduledArrival,
                        Delay = passage.Delay,
                        HeadWay = passage.ActualHeadWay,
                        BusStopCode = passage.BusStopCode,
                        DayOfWeek = (int) passage.ActualArrival.DayOfWeek,
                        TimeOfDayQuarter = (int) (tod/15f),
                        TimeOfDayHalfHour = (int) (tod/30f),
                        TimeOfDayHour = (int) (tod/60f),
                        StopTime = passage.StopTime,
                        Vehicle = int.Parse(passage.Vehicle),
                        Wind = (double) wo.Wind,
                        Precipitation = (double) wo.Precipitation,
                        Temperature = (double) wo.Temp,
                        Humidity = wo.Humidity,
                        SoccerGame = soccer,
                        PassengersPresent = passengerCount,
                        DelayYesterday = FindDelay(selectedDelays, passage.ScheduledArrival, 1, 1),
                        DelayDayBeforeYesterday = FindDelay(selectedDelays, passage.ScheduledArrival, 1, 2),
                        DelayLastWeek = FindDelay(selectedDelays, passage.ScheduledArrival, 7, 7),
                        DelayLast = FindPreviousDelay(selectedDelays),
                        AverageDelayLast3OfSameDay = (int)Math.Round(AverageLast3OfSameDay(selectedDelays, passage.ScheduledArrival), 0),
                        AverageDelayLast3 = (int)Math.Round(AverageLast3(selectedDelays), 0)
                    };
                items.Add(item);
                Console.Write("\r > {0:n0}", items.Count);

                delays.Add(new DelayHolder(passage));

                var sql = string.Format("INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[BusStopPassages]([ActualArrival],[ScheduledArrival]," +
                                        "[Delay],[HeadWay],[BusStopCode],[DayOfWeek],[TimeOfDayQuarter],[TimeOfDayHalfHour],[TimeOfDayHour],[StopTime]," +
                                        "[Vehicle],[Wind],[Precipitation],[Temperature],[Humidity],[SoccerGame],[PassengersPresent],[DelayYesterday]," +
                                        "[DelayDayBeforeYesterday],[DelayLastWeek],[DelayLast],[AverageDelayLast3OfSameDay],[AverageDelayLast3],[ScheduledArrivalDateTime]," +
                                        "[TicketsSoldLastHour], [TicketsSoldLastThreeHours]) " +
                                        "VALUES({0},{1},{2},{3},'{4}',{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},'{23}', -1, -1);",
                                        item.ActualArrival, item.ScheduledArrival, item.Delay, item.HeadWay, item.BusStopCode, item.DayOfWeek,
                                        item.TimeOfDayQuarter, item.TimeOfDayHalfHour, item.TimeOfDayHour, item.StopTime, item.Vehicle, Sql(item.Wind),
                                        Sql(item.Precipitation), Sql(item.Temperature), item.Humidity, item.SoccerGame, item.PassengersPresent,
                                        item.DelayYesterday, item.DelayDayBeforeYesterday, item.DelayLastWeek, item.DelayLast,
                                        item.AverageDelayLast3OfSameDay, item.AverageDelayLast3, Sql(item.ScheduledArrivalDateTime));
                objCtx.ExecuteStoreCommand(sql);
            }
        }

        public static void AddTicketData(string line)
        {
            var db = new SMIODataContext();
            var objCtx = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)db).ObjectContext;
            var start = new DateTime(2013, 11, 10, 20, 0, 0);
            var end = new DateTime(2013, 11, 25, 2, 0, 0);
            var count = 0;
            var data = db.BusStopPassages.Where(p => p.ScheduledArrivalDateTime >= start && p.ScheduledArrivalDateTime <= end).ToList();
            foreach (var passage in data)
            {
                var ticketsSoldLastHour = db.TicketData.SoldLastPeriod(line, passage.BusStopCode, passage.ScheduledArrivalDateTime, 60);
                var ticketsSoldLastThreeHours = db.TicketData.SoldLastPeriod(line, passage.BusStopCode, passage.ScheduledArrivalDateTime, 180);
                Console.Write("\r  > {0:n0} / {1:n0}", count++, data.Count);
                
                var sql = string.Format("UPDATE [Utilities.Database.SMIODataContext].[dbo].[BusStopPassages] SET [TicketsSoldLastHour]={0}, [TicketsSoldLastThreeHours]={1}" +
                                        "WHERE ID={2};",
                                       ticketsSoldLastHour, ticketsSoldLastThreeHours, passage.ID);
                objCtx.ExecuteStoreCommand(sql);
            }
        }

        private static int FindPreviousDelay(List<DelayHolder> delays)
        {
            if (delays.Count == 0) return InvalidData;
            return delays[delays.Count - 1].Delay;
        }

        protected static double AverageLast3(List<DelayHolder> delays)
        {
            if (delays.Count == 0) return InvalidData;
            return delays.Skip(delays.Count - 3).Select(p => p.Delay).Average();
        }

        protected static double AverageLast3OfSameDay(List<DelayHolder> delays, DateTime scheduledArrival)
        {
            if (!delays.Any()) return InvalidData;
            var ds = delays.Where(p => p.Schedule.Date < scheduledArrival.Date);
            ds = ds.Where(p => p.Schedule.DayOfWeek == scheduledArrival.DayOfWeek);
            ds = ds.Where(p => p.Schedule.TimeOfDay == scheduledArrival.TimeOfDay);
            ds = ds.Skip(ds.Count() - 3);
            if (!ds.Any()) return InvalidData;
            return ds.Select(p => p.Delay).Average();
        }

        protected static int FindDelay(List<DelayHolder> delays, DateTime scheduledArrival, int dayInterval, int dayStart)
        {
            if (!delays.Any()) return InvalidData;
            var found = delays.Where(p =>
                                     p.Schedule.Date <= scheduledArrival.Date.AddDays(-dayStart) &&
                                     p.Schedule.TimeOfDay == scheduledArrival.TimeOfDay);
            if (!found.Any()) return InvalidData;
            if (dayInterval == 1) return found.Last().Delay;
            found = found.Where(p => p.Schedule.DayOfWeek == scheduledArrival.DayOfWeek);
            if (!found.Any()) return InvalidData;
            return found.Last().Delay;
        }
        
        public class DelayHolder
        {
            public DateTime Schedule;
            public int Delay;
            public string BusStopCode;

            public DelayHolder(PassageItem passage)
            {
                Schedule = passage.ScheduledArrival;
                Delay = passage.Delay;
                BusStopCode = passage.BusStopCode;
            }

            public DelayHolder(DateTime schedule, int delay, string busstop)
            {
                Schedule = schedule;
                Delay = delay;
                BusStopCode = busstop;
            }
        }

        public static void Analyze(string stopCode)
        {
            Console.WriteLine("Analyzing bus stop passages for '{0}' ...", stopCode);

            var db = new SMIODataContext();

            var start = new DateTime(2013, 09, 01);
            var bsps = db.BusStopPassages.
                Where(p => p.ScheduledArrivalDateTime > start && p.BusStopCode == stopCode).
                OrderBy(p => p.ScheduledArrivalDateTime);
            var stop = db.BusStops.Single(p => p.ID == stopCode);

            Console.WriteLine("  > " + stop.Name);
            Console.WriteLine("  > {0:n0}", bsps.Count());

            var last = DateTime.MinValue;
            var first = DateTime.MinValue;

            Action check = () =>
                {
                    if (first != DateTime.MinValue && last != DateTime.MinValue)
                    {
                        if (last.Subtract(first).TotalHours < 24) return;
                        Console.WriteLine("      > Healthy period: {0:yyyy-MM-dd HH:mm:ss} - {1:yyyy-MM-dd HH:mm:ss} {2:000} {3:000}.", first, last, last.Subtract(first).TotalDays, last.Subtract(first).TotalHours);
                        first = DateTime.MinValue;
                        last = DateTime.MinValue;
                    }
                };

            foreach (var bsp in bsps)
            {
                var values = new[] {bsp.DelayLast, bsp.AverageDelayLast3, bsp.DelayYesterday, bsp.DelayDayBeforeYesterday, bsp.DelayLastWeek, bsp.AverageDelayLast3OfSameDay};
                if (values.All(p => p != InvalidData))
                {
                    if (first == DateTime.MinValue) first = bsp.ScheduledArrivalDateTime;
                    last = bsp.ScheduledArrivalDateTime;
                }
                else if (values.Any(p => p == InvalidData))
                    check();
            }
            check();
        }

        public double[] ToAnnData(string[] cols = null)
        {
            if (cols == null) cols = new string[0];
            //cols = new string[] { "passenger", "ticket" };

            var l = new List<object>();

            //l.Add(ScheduledArrivalDateTime);
            l.Add(DayOfWeek);
            //l.Add(ActualArrival); //This is actually TimeOfDayMinute
            //l.Add(ScheduledArrival);
            l.Add(TimeOfDayQuarter);
            //l.Add(TimeOfDayHalfHour);
            //l.Add(TimeOfDayHour);
            l.Add(StopTime);
            //l.Add(HeadWay);
            //l.Add(Vehicle);
            if (cols.Contains("passenger")) l.Add(PassengersPresent);
            l.Add(Temperature);
            l.Add(Precipitation);
            l.Add(Wind);
            l.Add(Humidity);
            l.Add(SoccerGame);
            l.Add(DelayLast);
            l.Add(AverageDelayLast3);
            l.Add(DelayYesterday);
            l.Add(DelayDayBeforeYesterday);
            l.Add(DelayLastWeek);
            l.Add(AverageDelayLast3OfSameDay);
            if (cols.Contains("ticket"))
            {
                l.Add(TicketsSoldLastHour);
                l.Add(TicketsSoldLastThreeHours);
            }
            l.Add(Delay);

            return l.ToDoubles();
        }

        public static string[] GetHeaders(string[] cols = null)
        {
            if (cols == null) cols = new string[0];
            //cols = new string[] { "passenger", "ticket" };
            return new[]
                {
                    //"ScheduledArrivalDateTime",
                    "DayOfWeek",
                    //"ActualArrival",
                    //"ScheduledArrival",
                    "TimeOfDayQuarter",
                    //"TimeOfDayHalfHour",
                    //"TimeOfDayHour",
                    "StopTime",
                    //"HeadWay",
                    //"Vehicle",
                    cols.Contains("passenger") ? "PassengersPresent" : "",
                    "Temperature",
                    "Precipitation",
                    "Wind",
                    "Humidity",
                    "SoccerGame",
                    "DelayLast",
                    "AverageDelayLast3",
                    "DelayYesterday",
                    "DelayDayBeforeYesterday",
                    "DelayLastWeek",
                    "AverageDelayLast3OfSameDay",
                    cols.Contains("ticket") ? "TicketsSoldLastHour" : "",
                    cols.Contains("ticket") ? "TicketsSoldLastThreeHours" : "",
                    "Delay"
                }.Where(p => !string.IsNullOrEmpty(p)).ToArray();
        }

        public double[] ToSimiAnnData(string[] cols = null)
        {
            if (cols == null) cols = new string[0];

            var l = new List<object>();

            l.Add(ScheduledArrivalDateTime);
            l.Add(DayOfWeek);
            //l.Add(ActualArrival);
            l.Add(ScheduledArrival);
            l.Add(TimeOfDayQuarter);
            //l.Add(TimeOfDayHalfHour);
            //l.Add(TimeOfDayHour);
            l.Add(StopTime);
            l.Add(HeadWay);
            l.Add(Vehicle);
            if (cols.Contains("passenger")) l.Add(PassengersPresent);
            l.Add(Temperature);
            l.Add(Precipitation);
            l.Add(Wind);
            l.Add(Humidity);
            l.Add(SoccerGame);
            l.Add(DelayLast);
            l.Add(AverageDelayLast3);
            l.Add(DelayYesterday);
            l.Add(DelayDayBeforeYesterday);
            l.Add(DelayLastWeek);
            l.Add(AverageDelayLast3OfSameDay);
            if (cols.Contains("ticket"))
            {
                l.Add(TicketsSoldLastHour);
                l.Add(TicketsSoldLastThreeHours);
            }
            l.Add(Delay);

            return l.ToDoubles();
        }

        public static string[] GetSimiHeaders(string[] cols = null)
        {
            if (cols == null) cols = new string[0];
            return new[]
                {
                    "ScheduledArrivalDateTime",
                    "DayOfWeek",
                    //"ActualArrival",
                    "ScheduledArrival",
                    "TimeOfDayQuarter",
                    //"TimeOfDayHalfHour",
                    //"TimeOfDayHour",
                    "StopTime",
                    "HeadWay",
                    "Vehicle",
                    cols.Contains("passenger") ? "PassengersPresent" : "",
                    "Temperature",
                    "Precipitation",
                    "Wind",
                    "Humidity",
                    "SoccerGame",
                    "DelayLast",
                    "AverageDelayLast3",
                    "DelayYesterday",
                    "DelayDayBeforeYesterday",
                    "DelayLastWeek",
                    "AverageDelayLast3OfSameDay",
                    cols.Contains("ticket") ? "TicketsSoldLastHour" : "",
                    cols.Contains("ticket") ? "TicketsSoldLastThreeHours" : "",
                    "Delay"
                }.Where(p => !string.IsNullOrEmpty(p)).ToArray();
        }

        public static void ToWeka(DateTime start, DateTime end, bool filtered, string stop = "")
        {
            var db = new SMIODataContext();
            var path = Data.GetWekaFile(string.Format("BusStopPassages_{0:yyyy-MM-dd}_{1:yyyy-MM-dd}{2}.csv", start, end, filtered ? "_filtered" : ""));
            System.IO.File.WriteAllLines(path,
                                         new[] { string.Join(",", GetHeaders()) }.
                                             Concat(
                                                 db.BusStopPassages.
                                                    Where(p => p.ScheduledArrivalDateTime > start && p.ScheduledArrivalDateTime <= end && (stop == "" || stop == p.BusStopCode)).
                                                    ToList().
                                                    Where(p => !filtered || Filter(p)).
                                                    OrderBy(p => p.ScheduledArrivalDateTime).
                                                    Select(p => p.ToAnnData()).
                                                    Select(p => string.Join(",", p.Select(d => d.ToString().Replace(",", ".")))))
                                         , Encoding.Default);
            Console.WriteLine("Trips from {0:yyyy-MM-dd} to {1:yyyy-MM-dd} saved to file '{2}'.", start, end, path);
        }

        private static bool Filter(BusStopPassage trip)
        {
            return new[] {trip.AverageDelayLast3, trip.DelayDayBeforeYesterday, trip.Delay, trip.DelayLast, trip.DelayLastWeek, trip.DelayYesterday, trip.AverageDelayLast3OfSameDay}.
                Select(p => p == int.MinValue ? -10000 : p).
                Select(Math.Abs).All(p => p <= 3000);
        }

        public static void GenerateSimi()
        {
            var db = new SMIODataContext();
            const string stop = "16010264";
            var passages = db.BusStopPassages.Where(p => stop == p.BusStopCode).ToList();

            var rows = passages.Where(p => p.BusStopCode == stop);
            var trainingSet = FilterRows(rows, new[] { new Tuple<DateTime, DateTime>(new DateTime(2014, 02, 16, 00, 00, 00), new DateTime(2014, 02, 17, 23, 59, 59)), });

            var forEachMinute = new List<BusStopPassage>();
            for (var dt = new DateTime(2014, 02, 17, 0, 0, 0); dt < new DateTime(2014, 02, 18, 0, 0, 0); dt = dt.AddSeconds(30))
            {
                var previous = trainingSet.Where(p => p.ScheduledArrivalDateTime <= dt).OrderBy(p=>p.ScheduledArrivalDateTime).LastOrDefault();
                if (previous != null)
                {
                    var p = new BusStopPassage()
                        {
                            ActualArrival = previous.ActualArrival,
                            AverageDelayLast3 = previous.AverageDelayLast3,
                            AverageDelayLast3OfSameDay = previous.AverageDelayLast3OfSameDay,
                            BusStopCode = previous.BusStopCode,
                            DayOfWeek = previous.DayOfWeek,
                            Delay = previous.Delay,
                            DelayDayBeforeYesterday = previous.DelayDayBeforeYesterday,
                            DelayLast = previous.DelayLast,
                            DelayLastWeek = previous.DelayLastWeek,
                            DelayYesterday = previous.DelayYesterday,
                            HeadWay = previous.HeadWay,
                            Humidity = previous.Humidity,
                            ID = previous.ID,
                            PassengersPresent = previous.PassengersPresent,
                            Precipitation = previous.Precipitation,
                            ScheduledArrival = previous.ScheduledArrival,
                            ScheduledArrivalDateTime = dt,
                            SoccerGame = previous.SoccerGame,
                            StopTime = previous.StopTime,
                            Temperature = previous.Temperature,
                            TicketsSoldLastHour = previous.TicketsSoldLastHour,
                            TicketsSoldLastThreeHours = previous.TicketsSoldLastThreeHours,
                            TimeOfDayHalfHour = previous.TimeOfDayHalfHour,
                            TimeOfDayHour = previous.TimeOfDayHour,
                            TimeOfDayQuarter = previous.TimeOfDayQuarter,
                            Vehicle = previous.Vehicle,
                            Wind = previous.Wind
                        };
                    forEachMinute.Add(p);
                }
            }

            SaveSimiRows(forEachMinute, "Simen.csv", null);
        }

        private static void SaveSimiRows(IEnumerable<BusStopPassage> set, string name, string[] cols)
        {
            var path = Data.GetWekaFile("finalDataset", name);
            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
            System.IO.File.WriteAllLines(path,
                                         new[] { string.Join(",", GetSimiHeaders(cols)) }.Concat(
                                             set.
                                                 OrderBy(p => p.ScheduledArrivalDateTime).
                                                 Select(p => p.ToSimiAnnData(cols)).
                                                 Select(p => string.Join(",", p.Select((d,i) => i==0?new DateTime((long)d).ToString("yyyy-MM-dd HH:mm:ss"):d.ToString().Replace(",", ".")))))
                                         , Encoding.Default);
            Console.WriteLine("Saved '{0}'.", path);
        }

        public static void SaveRows(IEnumerable<BusStopPassage> set, string name, string[] cols)
        {
            var path = Data.GetWekaFile("finalDataset", name);
            System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(path));
            System.IO.File.WriteAllLines(path,
                                         new[] { string.Join(",", GetHeaders(cols)) }.Concat(
                                             set.
                                                 OrderBy(p => p.ScheduledArrivalDateTime).
                                                 Select(p => p.ToAnnData(cols)).
                                                 Select(p => string.Join(",", p.Select(d => d.ToString().Replace(",", ".")))))
                                         , Encoding.Default);
            Console.WriteLine("Saved '{0}'.", path);
        }

        public static List<BusStopPassage> FilterRows(IEnumerable<BusStopPassage> rows, IEnumerable<Tuple<DateTime, DateTime>> intervals)
        {
            return intervals.SelectMany(p => rows.Where(c => c.ScheduledArrivalDateTime >= p.Item1 && c.ScheduledArrivalDateTime <= p.Item2)).ToList();
        }
    }
}
