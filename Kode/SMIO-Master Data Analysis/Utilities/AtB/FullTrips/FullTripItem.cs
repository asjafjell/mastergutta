﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace Utilities.AtB.FullTrips
{
    public class FullTripItem : DbLoader
    {
        [Key]
        public int ID { get; set; }
        public DateTime Date { get; set; }
        public string Shift { get; set; }
        public string Line { get; set; }
        public string RouteID { get; set; }
        public string Trip { get; set; }
        public string Vehicle { get; set; }
        public virtual BusStop DepartureTerminal { get; set; }
        public virtual BusStop ArrivalTerminal { get; set; }
        public TimeSpan ScheduledDepartureTime { get; set; }
        public TimeSpan DepartureTime { get; set; }
        public TimeSpan ScheduledArrivalTime { get; set; }
        public TimeSpan ArrivalTime { get; set; }
        public int Length { get; set; }
        public int OperatedMT { get; set; }
        public string WithoutService { get; set; }
        public TimeSpan AverageTravelTime { get; set; } // (hh:mm)
        public TimeSpan TravelTime { get; set; } // (hh:mm)
        public TimeSpan ScheduledLayover { get; set; } // (mm:ss)
        public TimeSpan Layover { get; set; } // (mm:ss)
        public decimal NotPerformedLayover { get; set; } //%
        public string DepartureDelay { get; set; }
        public int ArrivalAdvanceSeconds { get; set; }
        public TimeSpan AverageHeadway { get; set; } // (hh:mm)
        public TimeSpan Headway { get; set; } // (hh:mm)
        public decimal AverageOperationalSpeed { get; set; } // (Km/h)
        public decimal AverageCommercialSpeed { get; set; } // (Km/h)
        public decimal OperationalSpeed { get; set; } // (Km/h)
        public decimal CommercialSpeed { get; set; } // (Km/h)
        public string Order { get; set; }
        public string Anomaly { get; set; }
        public string LeaveOnTime { get; set; }
        public string ArriveOnTime { get; set; }

        public static IEnumerable<FullTripItem> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).
                          Skip(1).
                          Where(p => !string.IsNullOrEmpty(p)).
                          Select(p => p.Split(';')).
                          Select(Load);
        }

        private static FullTripItem Load(string[] p)
        {
            return new FullTripItem()
                {
                    Date = ParseDateTime(p[0], "dd/MM/yyyy"),
                    Shift = p[1],
                    Line = p[2],
                    RouteID = (p[3]),
                    Trip = p[4],
                    Vehicle = (p[5]),
                    DepartureTerminal = BusStop.FromCSVFormat(p[6]),
                    ArrivalTerminal = BusStop.FromCSVFormat(p[7]),
                    ScheduledDepartureTime = ParseTimeSpan(p[8], "HH:mm:ss"),
                    DepartureTime = ParseTimeSpan(p[9], "HH:mm:ss"),
                    ScheduledArrivalTime = ParseTimeSpan(p[10], "HH:mm:ss"),
                    ArrivalTime = ParseTimeSpan(p[11], "HH:mm:ss"),
                    Length = ParseInt(p[12]),
                    OperatedMT = ParseInt(p[13]),
                    WithoutService = p[14],
                    AverageTravelTime = ParseTimeSpan(p[15], "HH:mm"), // (hh:mm)
                    TravelTime = ParseTimeSpan(p[16], "HH:mm"), // (hh:mm)
                    ScheduledLayover = ParseTimeSpan(p[17], "mm:ss"), // (hh:mm)
                    Layover = ParseTimeSpan(p[18], "mm:ss"), // (hh:mm)
                    NotPerformedLayover = ParseDecimal(p[19]), //%
                    DepartureDelay = p[20],
                    ArrivalAdvanceSeconds = ParseSeconds(p[21], "mm:ss"),
                    AverageHeadway = ParseTimeSpan(p[22], "mm:ss"), // (hh:mm)
                    Headway = ParseTimeSpan(p[23], "mm:ss"), // (hh:mm)
                    AverageOperationalSpeed = ParseDecimal(p[24]), // (Km/h)
                    AverageCommercialSpeed = ParseDecimal(p[25]), // (Km/h)
                    OperationalSpeed = ParseDecimal(p[26]), // (Km/h)
                    CommercialSpeed = ParseDecimal(p[27]), // (Km/h)
                    Order = p[28],
                    Anomaly = p[29],
                    LeaveOnTime = p[30],
                    ArriveOnTime = p[31]
                };
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.Fulltrips);
            var files = System.IO.Directory.GetFiles(path, "*.csv");

            Func<decimal, string> n = d => d.ToString().Replace(",", ".");
            Func<TimeSpan, string> t = d => d.Hours.ToString("n0") + ":" + d.Minutes.ToString("n0") + ":" + d.Seconds.ToString("n0");

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.Fulltrips, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                HealFullTripsFile(file);
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = FromFile(file).ToList();

                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[FullTripItems]([Date],[Shift],[Line],[RouteID],[Trip],[Vehicle],[ScheduledDepartureTime],[DepartureTime],[ScheduledArrivalTime],[ArrivalTime],[Length],[OperatedMT],[WithoutService],[AverageTravelTime],[TravelTime],[ScheduledLayover],[Layover],[NotPerformedLayover],[DepartureDelay],[ArrivalAdvanceSeconds],[AverageHeadway],[Headway],[AverageOperationalSpeed],[AverageCommercialSpeed],[OperationalSpeed],[CommercialSpeed],[Order],[Anomaly],[LeaveOnTime],[ArriveOnTime],[ArrivalTerminal_ID],[DepartureTerminal_ID]) VALUES(" +
                    "'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}',{10},{11},'{12}','{13}','{14}','{15}','{16}',{17},'{18}',{19},'{20}','{21}',{22},{23},{24},{25},'{26}','{27}','{28}','{29}','{30}','{31}');",
                    p.Date.ToString("yyyy-MM-dd HH:mm:ss.fff"), p.Shift, p.Line, p.RouteID, p.Trip, p.Vehicle, t(p.ScheduledDepartureTime), t(p.DepartureTime), t(p.ScheduledArrivalTime), t(p.ArrivalTime), p.Length, p.OperatedMT, p.WithoutService, t(p.AverageTravelTime), t(p.TravelTime), t(p.ScheduledLayover), t(p.Layover), n(p.NotPerformedLayover), p.DepartureDelay, p.ArrivalAdvanceSeconds, t(p.AverageHeadway), t(p.Headway), n(p.AverageOperationalSpeed), n(p.AverageCommercialSpeed), n(p.OperationalSpeed), n(p.CommercialSpeed), p.Order, p.Anomaly, p.LeaveOnTime, p.ArriveOnTime, p.ArrivalTerminal.ID, p.DepartureTerminal.ID)), Encoding.Default);

                BusStop.Append(items.SelectMany(p => new[] {p.DepartureTerminal, p.ArrivalTerminal}));
            }
        }

        /// <summary>
        /// Repairs issues where some lines has ',' as separator instead of ';' on the last few items.
        /// </summary>
        /// <param name="file"></param>
        private static void HealFullTripsFile(string file)
        {
            Console.WriteLine("  > Healing full trip items.");

            if (System.IO.File.Exists(file + ".bak"))
            {
                Console.WriteLine("      > Already done. Skipping.");
                return;
            }

            Console.WriteLine("      > Loading file ...");
            var lines = System.IO.File.ReadAllLines(file, Encoding.Default).ToList();

            bool didSomething = false;
            for (var i = 0; i < lines.Count; i++)
            {
                var parts = lines[i].Split(';');
                if (parts.Length != 32)
                {
                    if (parts.Length < 10 && i == lines.Count - 1)
                    {
                        lines[i] = "";
                        continue;
                    }

                    var newParts = new List<string>();
                    newParts.AddRange(parts.Take(27));
                    newParts.AddRange(parts.Skip(27).Select(p => p.Split(',')).SelectMany(p => p));

                    if (newParts.Count == 32)
                    {
                        didSomething = true;
                        lines[i] = string.Join(";", newParts);
                        continue;
                    }

                    Console.WriteLine(Environment.NewLine + "      > Error at {0}; {1}", i, parts.Length);
                    Console.ReadLine();
                }

                Console.Write("\r      > {0:n2}%  ", (decimal)i / lines.Count * 100m);
            }
            Console.Write(Environment.NewLine);

            if (didSomething)
            {
                Console.WriteLine("      > Taking backup ...");
                System.IO.File.Copy(file, file + ".bak", true);
                Console.WriteLine("      > Saving ...");
                System.IO.File.WriteAllLines(file, lines, Encoding.Default);
            }

            Console.WriteLine("      > Done.");
        }
    }
}
