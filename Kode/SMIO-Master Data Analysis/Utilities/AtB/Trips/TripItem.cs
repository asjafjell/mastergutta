﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using Utilities.AtB.Interfaces;
using Utilities.AtB.Utilities;
using Utilities.Database;
using Utilities.Extensions;

namespace Utilities.AtB.Trips
{
    public class TripItem : DbLoader, IAiInput
    {
        private static DateTime[] _holidays;
        private Tuple<DateTime, DateTime>[] _vacations;

        [Key]
        public int ID { get; set; }

        //private FullTripItem FullTripItem { get; set; }

        /// <summary>
        /// The day of week for this trip.
        /// </summary>
        [NotMapped]
        public int DayOfWeek
        {
            get { return (int) ActualDeparture.DayOfWeek; }
        }

        /// <summary>
        /// The hour of day for this trip.
        /// </summary>
        [NotMapped]
        public int HourOfDay
        {
            get { return ActualDeparture.Hour; }
        }

        /// <summary>
        /// The hour of day for this trip.
        /// </summary>
        [NotMapped]
        public int MinuteOfDay
        {
            get { return (int) ActualDeparture.TimeOfDay.TotalMinutes; }
        }

        /// <summary>
        /// The month of year for this trip.
        /// </summary>
        [NotMapped]
        public int MonthOfYear
        {
            get { return ActualDeparture.Month; }
        }

        /// <summary>
        /// The actual time of departure from the starting point.
        /// </summary>
        public DateTime ActualDeparture
        {
            get
            {
                if (ActualDeparture_PossiblyTruncated.TimeOfDay.TotalSeconds == 0)
                    return ScheduledDeparture;
                else
                    return ActualDeparture_PossiblyTruncated;
            }
            set { ActualDeparture_PossiblyTruncated = value; }
        }

        [Column("ActualDeparture")]
        private DateTime ActualDeparture_PossiblyTruncated { get; set; }

        /// <summary>
        /// The actual time of arrival to the ending point. 
        /// This value is unknown in the test set.
        /// </summary>
        public DateTime ActualArrival { get; set; }

        /// <summary>
        /// The scheduled time of departure from the starting point.
        /// </summary>
        public DateTime ScheduledDeparture { get; set; }

        /// <summary>
        /// The scheduled time of arrival to the ending point. 
        /// </summary>
        public DateTime ScheduledArrival { get; set; }

        /// <summary>
        /// The actual delay. 
        /// This value is the prediction target, and is missing in the test set.
        /// </summary>
        [NotMapped]
        public int Delay
        {
            get { return (int) ActualArrival.Subtract(ScheduledArrival).TotalSeconds; }
        }

        /// <summary>
        /// Whether or not this trip is in the summer (May-August), where NTNU and other institutions stop working at 15:00 instead of 16:00.
        /// </summary>
        [NotMapped]
        public bool IsSummerTime
        {
            get { return ScheduledDeparture.Date.Month >= 5 && ScheduledDeparture.Date.Month <= 8; }
        }

        /// <summary>
        /// Whether or not this trip is on a public holiday (like May 17th or December 31th).
        /// </summary>
        [NotMapped]
        public bool IsHoliday
        {
            get
            {
                if (ScheduledDeparture.Year < 2013 || ScheduledDeparture.Year > 2014)
                    throw new Exception("IsHoliday is only configured for 2013 and 2014 (not " + ScheduledDeparture.Year + ").");
                if (_holidays == null)
                    _holidays = new[]
                        {
                            new DateTime(2013, 01, 01),
                            new DateTime(2013, 02, 10),
                            new DateTime(2013, 03, 24),
                            new DateTime(2013, 03, 28),
                            new DateTime(2013, 03, 29),
                            new DateTime(2013, 03, 31),
                            new DateTime(2013, 04, 01),
                            new DateTime(2013, 05, 01),
                            new DateTime(2013, 05, 08),
                            new DateTime(2013, 05, 09),
                            new DateTime(2013, 05, 17),
                            new DateTime(2013, 05, 19),
                            new DateTime(2013, 05, 20),
                            new DateTime(2013, 12, 24),
                            new DateTime(2013, 12, 25),
                            new DateTime(2013, 12, 26),
                            new DateTime(2013, 12, 31),
                            new DateTime(2014, 01, 01),
                            new DateTime(2014, 03, 02),
                            new DateTime(2014, 04, 13),
                            new DateTime(2014, 04, 17),
                            new DateTime(2014, 04, 18),
                            new DateTime(2014, 04, 20),
                            new DateTime(2014, 04, 21),
                            new DateTime(2014, 05, 01),
                            new DateTime(2014, 05, 08),
                            new DateTime(2014, 05, 17),
                            new DateTime(2014, 05, 29),
                            new DateTime(2014, 06, 08),
                            new DateTime(2014, 06, 09),
                            new DateTime(2014, 12, 24),
                            new DateTime(2014, 12, 25),
                            new DateTime(2014, 12, 26),
                            new DateTime(2014, 12, 31)
                        };
                return _holidays.Contains(ScheduledDeparture.Date);
            }
        }

        /// <summary>
        /// Whether or not this trip is on a general vacation (like in the middle of summer or the Easter vacation).
        /// </summary>
        [NotMapped]
        public bool IsGeneralVacation
        {
            get
            {
                if (ScheduledDeparture.Year < 2013 || ScheduledDeparture.Year > 2014)
                    throw new Exception("IsGeneralVacation is only configured for 2013 and 2014.");
                if (_vacations == null)
                    _vacations = new[]
                        {
                            new Tuple<DateTime, DateTime>(new DateTime(2013, 01, 01), new DateTime(2013, 01, 01)) //TODO: Fix
                        };
                return _vacations.Any(p => ScheduledDeparture.Date >= p.Item1 && ScheduledDeparture.Date <= p.Item2);
            }
        }

        /// <summary>
        /// The line ID for the bus travelling this trip.
        /// </summary>
        [MaxLength(8)]
        public string LineID { get; set; }

        /// <summary>
        /// The ID of the current trip.
        /// </summary>
        [MaxLength(16)]
        public string TripID { get; set; }

        /// <summary>
        /// The ID of the full trip item this row is based on.
        /// </summary>
        public int FullTripItemID { get; set; }

        /// <summary>
        /// The route ID for this trip.
        /// </summary>
        [MaxLength(8)]
        public string RouteID { get; set; }

        /// <summary>
        /// The vehicle ID for the vehicle travelling this trip.
        /// </summary>
        [MaxLength(16)]
        public string VehicleID { get; set; }

        /// <summary>
        /// The delay at the beginning of this trip (in seconds).
        /// </summary>
        public int InitialDelay { get; set; }

        /// <summary>
        /// Whether or not there is a soccer game starting in less than two hours.
        /// </summary>
        [Column("SoccerInTwo")]
        public bool SoccerGameWithinTwoHoursFromNow { get; set; }

        /// <summary>
        /// Whether or not a soccer game ended less than two hours ago.
        /// </summary>
        [Column("SoccerTwoAgo")]
        public bool SoccerGameLessThanTwoHoursAgo { get; set; }

        /// <summary>
        /// The amount of viewers in the football game, if there is a game.
        /// </summary>
        public int SoccerGameViewers { get; set; }

        /// <summary>
        /// The temperature for the hour this trip started. (If the trip starts at 2013-08-01 15:23, the temperature is from 15:00 to 16:00.)
        /// Measured in celcius degrees.
        /// </summary>
        public decimal Temperature { get; set; }

        /// <summary>
        /// The precipitation for the hour this trip started. (If the trip starts at 2013-08-01 15:23, the precipitation measurement is from 15:00 to 16:00.)
        /// Measured in millimeters.
        /// </summary>
        public decimal Precipitation { get; set; }

        /// <summary>
        /// The wind strength for the hour this trip started. (If the trip starts at 2013-08-01 15:23, the wind measurement is from 15:00 to 16:00.)
        /// Measured in meters/second.
        /// </summary>
        public decimal Wind { get; set; }

        /// <summary>
        /// The humidity for the hour this trip started. (If the trip starts at 2013-08-01 15:23, the humidity measurement is from 15:00 to 16:00.)
        /// Measured as a percentage ([0-100]).
        /// </summary>
        public int Humidity { get; set; }

        /// <summary>
        /// The temperature from yesterday. (If the trip starts at 2013-08-02 15:23, the temperature is from 15:00 to 16:00 on 2013-08-01.)
        /// Measured in celcius degrees.
        /// </summary>
        public decimal TemperatureYesterday { get; set; }

        /// <summary>
        /// The precipitation for yesterday. (If the trip starts at 2013-08-02 15:23, the precipitation measurement is from 15:00 to 16:00 on 2013-08-01.)
        /// Measured in millimeters.
        /// </summary>
        public decimal PrecipitationYesterday { get; set; }

        /// <summary>
        /// The wind strength from yesterday. (If the trip starts at 2013-08-01 15:23, the wind measurement is from 15:00 to 16:00 on 2013-08-01.)
        /// Measured in meters/second.
        /// </summary>
        public decimal WindYesterday { get; set; }

        /// <summary>
        /// The humidity from yesterday. (If the trip starts at 2013-08-01 15:23, the humidity measurement is from 15:00 to 16:00 on 2013-08-01.)
        /// Measured as a percentage ([0-100]).
        /// </summary>
        public int HumidityYesterday { get; set; }

        /// <summary>
        /// The temperature from last week. (If the trip starts at 2013-08-08 15:23, the temperature is from 15:00 to 16:00 on 2013-08-01.)
        /// Measured in celcius degrees.
        /// </summary>
        public decimal TemperatureLastWeek { get; set; }

        /// <summary>
        /// The precipitation for last week. (If the trip starts at 2013-08-08 15:23, the precipitation measurement is from 15:00 to 16:00 on 2013-08-01.)
        /// Measured in millimeters.
        /// </summary>
        public decimal PrecipitationLastWeek { get; set; }

        /// <summary>
        /// The wind strength from last week. (If the trip starts at 2013-08-08 15:23, the wind measurement is from 15:00 to 16:00 on 2013-08-01.)
        /// Measured in meters/second.
        /// </summary>
        public decimal WindLastWeek { get; set; }

        /// <summary>
        /// The humidity from last week. (If the trip starts at 2013-08-08 15:23, the humidity measurement is from 15:00 to 16:00 on 2013-08-01.)
        /// Measured as a percentage ([0-100]).
        /// </summary>
        public int HumidityLastWeek { get; set; }

        /// <summary>
        /// The amount of cell phone tickets sold the previous day.
        /// </summary>
        [Column("CPTYesterday")]
        public int CellPhoneTicketsSoldYesterday { get; set; }

        /// <summary>
        /// The amount of cell phone tickets sold this day last week
        /// </summary>
        [Column("CPTLWeek")]
        public int CellPhoneTicketsSoldThisDayLastWeek { get; set; }

        /// <summary>
        /// The delay of the last bus running this trip.
        /// Measured in seconds
        /// </summary>
        public int DelayLastTrip { get; set; }

        /// <summary>
        /// The average delay of the last five buses running this trip.
        /// Measured in seconds
        /// </summary>
        [Column("AverageLFive")]
        public int AverageDelayLastFiveTrips { get; set; }

        /// <summary>
        /// The standard deviation for the delay of the last five buses running this trip.
        /// Measured in seconds
        /// </summary>
        [Column("StdLastFive")]
        public int StandardDeviationOfDelayForLastFiveTrips { get; set; }

        /// <summary>
        /// The delay for the exact same trip last week.
        /// Measured in seconds
        /// </summary>
        [Column("DelayLWeek")]
        public int DelayForThisTripLastWeek { get; set; }

        /// <summary>
        /// The delay for the five trips preceeding this trip the last week.
        /// Measured in seconds
        /// </summary>
        [Column("AvrDelayLWeek")]
        public int AverageDelayForLastFiveTripsLastWeek { get; set; }

        public static void ResetData(bool removeExisting = false)
        {
            Console.WriteLine("Starting data calculations.");
            var start = DateTime.Now;

            var db = new SMIODataContext();

            if (removeExisting)
                DbHelper.EmptyTable(db, Data.DataFolders.Trips);

            var allTrips = new List<TripItem>();

            var routes = db.FullTripItems.GroupBy(p => p.RouteID);
            var count = 0;
            foreach (var route in routes.Select(p => p.OrderBy(c => c.ScheduledDepartureTime).ToList()))
            {
                var trips = new List<TripItem>();
                foreach (var ft in route)
                {
                    var trip = new TripItem()
                        {
                            ActualDeparture = ft.Date.Add(ft.DepartureTime),
                            ActualArrival = ft.Date.Add(ft.ArrivalTime),
                            ScheduledDeparture = ft.Date.Add(ft.ScheduledDepartureTime),
                            ScheduledArrival = ft.Date.Add(ft.ScheduledArrivalTime),
                            LineID = ft.Line,
                            RouteID = ft.RouteID,
                            VehicleID = ft.Vehicle,
                            InitialDelay = ParseSeconds(ft.DepartureDelay),
                        };

                    trip.DelayLastTrip = FindDelayForTripsBefore(trips, trip.ScheduledDeparture, 1)[0];
                    trip.AverageDelayLastFiveTrips = (int) FindDelayForTripsBefore(trips, trip.ScheduledDeparture, 5).Average();
                    trip.StandardDeviationOfDelayForLastFiveTrips = (int) FindDelayForTripsBefore(trips, trip.ScheduledDeparture, 5).StandardDeviation();
                    trip.DelayForThisTripLastWeek = FindDelayForTrip(trips, trip.ScheduledDeparture.AddDays(-7));
                    trip.AverageDelayForLastFiveTripsLastWeek = (int) FindDelayForTripsBefore(trips, trip.ScheduledDeparture.AddDays(-7), 5).Average();

                    trip.SoccerGameWithinTwoHoursFromNow = false;
                    trip.SoccerGameLessThanTwoHoursAgo = false;
                    trip.SoccerGameViewers = 0;

                    var wo = db.WeatherObjects.FromHour(trip.ScheduledDeparture);
                    trip.Temperature = wo.Temp;
                    trip.Precipitation = wo.Precipitation;
                    trip.Wind = wo.Wind;
                    trip.Humidity = wo.Humidity;

                    wo = db.WeatherObjects.FromHour(trip.ScheduledDeparture.AddDays(-1));
                    trip.TemperatureYesterday = wo.Temp;
                    trip.PrecipitationYesterday = wo.Precipitation;
                    trip.WindYesterday = wo.Wind;
                    trip.HumidityYesterday = wo.Humidity;

                    wo = db.WeatherObjects.FromHour(trip.ScheduledDeparture.AddDays(-7));
                    trip.TemperatureLastWeek = wo.Temp;
                    trip.PrecipitationLastWeek = wo.Precipitation;
                    trip.WindLastWeek = wo.Wind;
                    trip.HumidityLastWeek = wo.Humidity;

                    trip.TripID = ft.Trip;
                    trip.FullTripItemID = ft.ID;

                    trip.CellPhoneTicketsSoldYesterday = db.CellPhoneTicketData.FromDay(trip.ScheduledDeparture.Date.AddDays(-1)).ToArray().Sum(p => p.NumTickets);
                    trip.CellPhoneTicketsSoldThisDayLastWeek = db.CellPhoneTicketData.FromDay(trip.ScheduledDeparture.Date.AddDays(-7)).ToArray().Sum(p => p.NumTickets);

                    trips.Add(trip);

                    Console.Write("\r  > Finished item {0}, time: {1}.", count++, DateTime.Now.Subtract(start).ToString("hh':'mm':'ss'.'fff"));
                }
                allTrips.AddRange(trips);
            }
            Console.Write("\r  > Done. Created {0} items in {1}.\n", count, DateTime.Now.Subtract(start).ToString("hh':'mm':'ss'.'fff"));

            Console.WriteLine("Saving to file.");

            var sql = Data.GetSqlFile(Data.DataFolders.Trips, "trips.sql");
            System.IO.File.WriteAllLines(sql, allTrips.Select(p => string.Format(
                "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[TripItems]([LineID],[RouteID],[VehicleID],[InitialDelay],[SoccerInTwo],[SoccerTwoAgo],[SoccerGameViewers],[Temperature],[Precipitation],[Wind],[Humidity],[TemperatureYesterday],[PrecipitationYesterday],[WindYesterday],[HumidityYesterday],[TemperatureLastWeek],[PrecipitationLastWeek],[WindLastWeek],[HumidityLastWeek],[DelayLastTrip],[AverageLFive],[StdLastFive],[DelayLWeek],[AvrDelayLWeek],[ActualDeparture],[ActualArrival],[ScheduledDeparture],[ScheduledArrival], [CPTYesterday], [CPTLWeek], [FullTripItemID], [TripID]) VALUES('{0}','{1}','{2}',{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21},{22},{23},'{24}','{25}','{26}','{27}', {28}, {29}, {30}, {31}'');",
                p.LineID, p.RouteID, p.VehicleID, p.InitialDelay, Sql(p.SoccerGameWithinTwoHoursFromNow), Sql(p.SoccerGameLessThanTwoHoursAgo), p.SoccerGameViewers, Sql(p.Temperature), Sql(p.Precipitation), Sql(p.Wind), p.Humidity, Sql(p.TemperatureYesterday), Sql(p.PrecipitationYesterday), Sql(p.WindYesterday), p.HumidityYesterday, Sql(p.TemperatureLastWeek), Sql(p.PrecipitationLastWeek), Sql(p.WindLastWeek), p.HumidityLastWeek, p.DelayLastTrip, p.AverageDelayLastFiveTrips, p.StandardDeviationOfDelayForLastFiveTrips, p.DelayForThisTripLastWeek, p.AverageDelayForLastFiveTripsLastWeek, Sql(p.ActualDeparture), Sql(p.ActualArrival), Sql(p.ScheduledDeparture), Sql(p.ScheduledArrival), p.CellPhoneTicketsSoldYesterday, p.CellPhoneTicketsSoldThisDayLastWeek, p.FullTripItemID, p.TripID)), Encoding.Default);

            Console.WriteLine("Saved.");
        }

        /// <summary>
        /// Returns a list of delays for the given amount of trips before the given time.
        /// </summary>
        /// <param name="trips"></param>
        /// <param name="dt"></param>
        /// <param name="take"></param>
        /// <returns></returns>
        protected static int[] FindDelayForTripsBefore(List<TripItem> trips, DateTime dt, int take)
        {
            var delays = new int[take].Select(p => int.MinValue).ToArray();
            for (var i = trips.Count - 1; i > -1; i--)
            {
                if (trips[i].ScheduledDeparture < dt)
                {
                    delays[--take] = trips[i].Delay;
                    if (take <= 0) break;
                }
            }
            return delays;
        }

        /// <summary>
        /// Returns the delay for the trip with the given ScheduledDepartureTime. If there are more (for example if two buses drove at the same time), this method just picks the first (from DB sequence).
        /// </summary>
        /// <param name="trips"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        protected static int FindDelayForTrip(IEnumerable<TripItem> trips, DateTime dt)
        {
            var trip = trips.FirstOrDefault(p => p.ScheduledDeparture == dt) ?? trips.Where(p => p.ScheduledDeparture < dt).OrderBy(p => p.ScheduledDeparture).LastOrDefault();
            return trip == null ? int.MinValue : trip.Delay;
        }

        public double[] ToAnnInput()
        {
            return new object[]
                {
                    DayOfWeek,
//MinuteOfDay,
//MonthOfYear,
//ActualArrival.TimeOfDay.TotalMinutes,
                    HourOfDay,
//ScheduledDeparture.TimeOfDay.TotalMinutes,
//ScheduledArrival.TimeOfDay.TotalMinutes,
//IsSummerTime,
                    IsHoliday,
//IsGeneralVacation,
//Bucketize("line", LineID),
                    Bucketize("route", RouteID),
//Bucketize("vehicle", VehicleID),
                    InitialDelay,
//SoccerGameWithinTwoHoursFromNow,
//SoccerGameLessThanTwoHoursAgo,
//SoccerGameViewers,
                    Temperature,
//Precipitation,
                    Wind,
//Humidity,
                    TemperatureYesterday,
//PrecipitationYesterday,
                    WindYesterday,
//HumidityYesterday,
//TemperatureLastWeek,
                    PrecipitationLastWeek,
                    WindLastWeek,
                    HumidityLastWeek,
//DelayLastTrip,
                    AverageDelayLastFiveTrips,
//StandardDeviationOfDelayForLastFiveTrips,
                    DelayForThisTripLastWeek,
                    AverageDelayForLastFiveTripsLastWeek
                }.ToDoubles();
        }

        public double[] ToAnnOutput()
        {
            return new List<Object>()
                {
                    Delay
                }.ToDoubles();
        }

        public double[] ToAnnData()
        {
            return new List<Object>()
                {
                    DayOfWeek,
                    MinuteOfDay,
                    MonthOfYear,
                    ActualArrival.TimeOfDay.TotalMinutes,
                    HourOfDay,
                    ScheduledDeparture.TimeOfDay.TotalMinutes,
                    ScheduledArrival.TimeOfDay.TotalMinutes,
                    IsSummerTime,
                    IsHoliday,
                    IsGeneralVacation,
                    Bucketize("line", LineID),
                    Bucketize("route", RouteID),
                    Bucketize("vehicle", VehicleID),
                    InitialDelay,
                    SoccerGameWithinTwoHoursFromNow,
                    SoccerGameLessThanTwoHoursAgo,
                    SoccerGameViewers,
                    Temperature,
                    Precipitation,
                    Wind,
                    Humidity,
                    TemperatureYesterday,
                    PrecipitationYesterday,
                    WindYesterday,
                    HumidityYesterday,
                    TemperatureLastWeek,
                    PrecipitationLastWeek,
                    WindLastWeek,
                    HumidityLastWeek,
                    DelayLastTrip,
                    AverageDelayLastFiveTrips,
                    StandardDeviationOfDelayForLastFiveTrips,
                    DelayForThisTripLastWeek,
                    AverageDelayForLastFiveTripsLastWeek,
                    Delay
                }.ToDoubles();
        }

        public string Header()
        {
            return string.Join(",", GetHeaders());
        }

        public static string[] GetHeaders()
        {
            return new[]
                {
                    "DayOfWeek",
                    "MinuteOfDay",
                    "MonthOfYear",
                    "ActualArrival.TimeOfDay.TotalMinutes",
                    "HourOfDay",
                    "ScheduledDeparture.TimeOfDay.TotalMinutes",
                    "ScheduledArrival.TimeOfDay.TotalMinutes",
                    "IsSummerTime",
                    "IsHoliday",
                    "IsGeneralVacation",
                    "LineID",
                    "RouteID",
                    "VehicleID",
                    "InitialDelay",
                    "SoccerGameWithinTwoHoursFromNow",
                    "SoccerGameLessThanTwoHoursAgo",
                    "SoccerGameViewers",
                    "Temperature",
                    "Precipitation",
                    "Wind",
                    "Humidity",
                    "TemperatureYesterday",
                    "PrecipitationYesterday",
                    "WindYesterday",
                    "HumidityYesterday",
                    "TemperatureLastWeek",
                    "PrecipitationLastWeek",
                    "WindLastWeek",
                    "HumidityLastWeek",
                    "DelayLastTrip",
                    "AverageDelayLastFiveTrips",
                    "StandardDeviationOfDelayForLastFiveTrips",
                    "DelayForThisTripLastWeek",
                    "AverageDelayForLastFiveTripsLastWeek",
                    "Delay"
                };
        }

        public static void ToWeka(DateTime start, DateTime end, bool filtered = false)
        {
            var db = new SMIODataContext();
            var path = Data.GetWekaFile(string.Format("Trips_{0:yyyy-MM-dd}_{1:yyyy-MM-dd}{2}.csv", start, end, filtered ? "_filtered" : ""));
            System.IO.File.WriteAllLines(path,
                                         new[] {string.Join(",", GetHeaders())}.
                                             Concat(
                                                 db.Trips.
                                                    Where(p => p.ScheduledDeparture > start && p.ScheduledDeparture <= end && p.LineID == "8").
                                                    ToList().
                                                    Where(p => !filtered || Filter(p)).
                                                    OrderBy(p => p.ScheduledDeparture).
                                                    Select(p => p.ToAnnData()).
                                                    Select(p => string.Join(",", p.Select(d => d.ToString().Replace(",", ".")))))
                                         , Encoding.Default);
            Console.WriteLine("Trips from {0:yyyy-MM-dd} to {1:yyyy-MM-dd} saved to file '{2}'.", start, end, path);
        }

        private static bool Filter(TripItem trip)
        {
            return new[] {trip.InitialDelay, trip.DelayLastTrip, trip.DelayForThisTripLastWeek, trip.AverageDelayForLastFiveTripsLastWeek, trip.AverageDelayLastFiveTrips, trip.Delay}.
                Select(p => p == int.MinValue ? -10000 : p).
                Select(Math.Abs).All(p => p <= 3000);
        }
    }
}
