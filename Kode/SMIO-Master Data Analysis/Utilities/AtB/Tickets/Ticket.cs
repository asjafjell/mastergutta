﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace Utilities.AtB.Tickets
{
    public class Ticket : DbLoader
    {
        public enum UsageTypes
        {
            SchoolUsage,
            SingleTicketUsage,
            PeriodUsage,
            PunchUsage,
            SingleAutoCTASale,
            SingleAutoSVRSale
        }

        [Key]
        public int ID { get; set; }

        public UsageTypes UsageType { get; set; }
        public int Product { get; set; }
        public int CustomerType { get; set; }
        public int NumPassengers { get; set; }
        public DateTime EventDateTime { get; set; }
        public int LineNr { get; set; }
        public int TripNr { get; set; }
        public int Vehicle { get; set; }
        public int FromZone { get; set; }
        public virtual BusStop EntryStop { get; set; }
        public int Operator { get; set; }
        public int ToZone { get; set; }
        public string PaymentType { get; set; }
        //public bool Overgang { get; set; }
        public bool IsInterchange { get; set; }
        public int TransportType { get; set; }
        public string CardNr { get; set; }
        public int SequenceNr { get; set; }
        public string CoreZone { get; set; }
        public string ZoneDistance { get; set; }
        public int Distance { get; set; }
        public decimal Price { get; set; }
        public int GeoValidityType { get; set; }

        public static IEnumerable<Ticket> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).Skip(1).Select(line => line.Split(';')).Select(Load);
        }

        private static Ticket Load(string[] p)
        {
            //if(p[13]!=p[14])
                //throw new Exception("WHAT THE FUCK! Interchange is weird.");
            return new Ticket()
            {
                UsageType = ParseEnum(p[0]),
                Product = ParseInt(p[1]),
                CustomerType = ParseInt(p[2]),
                NumPassengers = ParseInt(p[3]),
                EventDateTime = ParseDateTime(p[4], p[4].Length == 16 ? "dd.MM.yyyy HH:mm" : "yyyy-MM-dd HH:mm:ss.fff"),
                LineNr = ParseInt(p[5]),
                TripNr = ParseInt(p[6]),
                Vehicle = ParseInt(p[7]),
                FromZone = ParseInt(p[8]),
                EntryStop = BusStop.FromCSVFormat(p[9]),
                Operator = ParseInt(p[10]),
                ToZone = ParseInt(p[11]),
                PaymentType = p[12],
                //Overgang = p[13] == "1", //Removed since it's always the same as IsInterchange
                IsInterchange = p[14] == "1",
                TransportType = ParseInt(p[15]),
                CardNr = p[16],
                SequenceNr = ParseInt(p[17]),
                CoreZone = p[18],
                ZoneDistance = p[19],
                Distance = ParseInt(p[20]),
                Price = ParseDecimal(p[21]),
                GeoValidityType = ParseInt(p[22]),
            };
        }

        private static UsageTypes ParseEnum(string s)
        {
            switch (s)
            {
                case "STRPeriodUsageTransaction":
                    return UsageTypes.PeriodUsage;
                case "STRPunchUsageTransaction":
                    return UsageTypes.PunchUsage;
                case "STRSingleTicketUsageTransaction":
                    return UsageTypes.SingleTicketUsage;
                case "STRSchoolUsageTransaction":
                    return UsageTypes.SchoolUsage;
                case "STRSingleAutoCTASaleTransaction":
                    return UsageTypes.SingleAutoCTASale;
                case "STRSingleAutoSVRSaleTransaction":
                    return UsageTypes.SingleAutoSVRSale;
            }

            throw new Exception("Invalid product type: " + s);
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.Ticket);
            var files = System.IO.Directory.GetFiles(path, "*.csv").ToList();

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.Ticket, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = FromFile(file).ToList();

                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[Tickets]([UsageType],[Product],[CustomerType],[NumPassengers],[EventDateTime],[LineNr],[TripNr],[Vehicle],[FromZone],[Operator],[ToZone],[PaymentType],[IsInterchange],[TransportType],[CardNr],[SequenceNr],[CoreZone],[ZoneDistance],[Distance],[Price],[GeoValidityType],[EntryStop_ID]) VALUES({0},{1},{2},{3},'{4}',{5},{6},{7},{8},{9},{10},'{11}',{12},{13},'{14}',{15},'{16}','{17}',{18},{19},{20},{21});",
                    (int)p.UsageType, p.Product, p.CustomerType, p.NumPassengers, Sql(p.EventDateTime), p.LineNr, p.TripNr, p.Vehicle, p.FromZone, p.Operator, p.ToZone, p.PaymentType, Sql(p.IsInterchange), p.TransportType, p.CardNr, p.SequenceNr, p.CoreZone, p.ZoneDistance, p.Distance, Sql(p.Price), p.GeoValidityType, p.EntryStop == null ? "NULL" : p.EntryStop.ID)), Encoding.Default);

                BusStop.Append(items.Where(p => p.EntryStop != null).Select(p => p.EntryStop));
            }
        }
    }
}
