﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace Utilities.AtB.PassageAtVirtualLoops
{
    public class PassageAtVirtualLoopsItem : DbLoader
    {
        [Key]
        public int ID { get; set; }

        public string Line { get; set; }
        public string Shift { get; set; }
        public string Trip { get; set; }
        public string Vehicle { get; set; }
        public string Loop { get; set; }
        public string Link { get; set; }
        public DateTime ActualArrival { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string RPTelegram { get; set; }

        public static IEnumerable<PassageAtVirtualLoopsItem> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).Skip(1).Select(line => line.Split(';')).Select(Load);
        }

        private static PassageAtVirtualLoopsItem Load(string[] p)
        {
            return new PassageAtVirtualLoopsItem()
                {

                    Line = p[0],
                    Shift = p[1],
                    Trip = p[2],
                    Vehicle = p[3],
                    Loop = p[4],
                    Link = p[5],
                    ActualArrival = DateTime.ParseExact(p[6], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                    Latitude = ParseDecimal(p[7]),
                    Longitude = ParseDecimal(p[8]),
                    RPTelegram = p[9]
                };
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.PassageAtVirtualLoops);
            var files = System.IO.Directory.GetFiles(path, "*.csv").ToList();

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.PassageAtVirtualLoops, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = FromFile(file).ToList();

                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[PassageAtVirtualLoopsItems]([Line],[Shift],[Trip],[Vehicle],[Loop],[Link],[ActualArrival],[Latitude],[Longitude],[RPTelegram]) VALUES('{0}','{1}','{2}','{3}','{4}','{5}','{6}',{7},{8},'{9}');",
                    p.Line, p.Shift, p.Trip, p.Vehicle, p.Loop, p.Link, p.ActualArrival.ToString("yyyy-MM-dd HH:mm:ss.fff"), p.Latitude.ToString().Replace(",", "."), p.Longitude.ToString().Replace(",", "."), p.RPTelegram)), Encoding.Default);
            }
        }
    }
}
