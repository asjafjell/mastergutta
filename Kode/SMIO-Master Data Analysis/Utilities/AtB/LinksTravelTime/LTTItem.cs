﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Text;
using System.Linq;
using Utilities.AtB.General;
using Utilities.AtB.Interfaces;
using Utilities.AtB.Utilities;
using System.Data.Entity;
using Utilities.Database;
using Utilities.Extensions;

namespace Utilities.AtB.LinksTravelTime
{
    public class LTTItem : DbLoader, IAiInput
    {
        [Key]
        public int ID { get; set; }
        public string Trip { get; set; }
        public virtual BusStop BusStopFrom { get; set; }
        public virtual BusStop BusStopTo { get; set; }
        public DateTime ActualDeparture { get; set; }
        public DateTime ActualArrival { get; set; }
        public int StopTime { get; set; }
        public int TravelTime { get; set; }
        public int TotalTravelTime { get; set; }
        public int HeadWay { get; set; }

        public static IEnumerable<LTTItem> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).
                          Where(p => Char.IsDigit(p[0])).
                          Select(p => p.Split(';')).
                          Select(p => new LTTItem()
                              {
                                  Trip = p[0],
                                  BusStopFrom = BusStop.FromCSVFormat(p[1]),
                                  BusStopTo = BusStop.FromCSVFormat(p[2]),
                                  ActualDeparture = ParseDateTime(p[3], "dd'/'MM'/'yyyy' 'HH':'mm':'ss"),
                                  ActualArrival = ParseDateTime(p[4], "dd'/'MM'/'yyyy' 'HH':'mm':'ss"),
                                  StopTime = ParseInt(p[5]),
                                  TravelTime = ParseInt(p[6]),
                                  TotalTravelTime = ParseInt(p[7]),
                                  HeadWay = ParseInt(p[8]),
                              });
        }

        public static List<LTTItem> LoadPeriod(DateTime start, DateTime end)
        {
            var files = Data.GetDataFiles(Data.DataFolders.LinkTravelTimes, start, end);

            if (files.Count == 0) throw new Exception("No data.");

            var allItems = new List<LTTItem>();
            allItems.AddRange(FromFile(files.First()).Where(p => p.ActualArrival >= start));
            if (files.Count == 1)
                allItems = allItems.Where(p => p.ActualArrival <= end).ToList();
            else
                for (var i = 0; i < files.Count; i++)
                {
                    if (i == files.Count - 1)
                        allItems.AddRange(FromFile(files[i]).Where(p => p.ActualArrival <= end));
                    else
                        allItems.AddRange(FromFile(files[i]));
                }

            return allItems;
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.LinkTravelTimes);
            var files = System.IO.Directory.GetFiles(path, "*.csv").ToList();

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.LinkTravelTimes, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = FromFile(file).ToList();

                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[LTTItems]([Trip],[ActualDeparture],[ActualArrival],[StopTime],[TravelTime],[TotalTravelTime],[HeadWay],[BusStopFrom_ID],[BusStopTo_ID]) VALUES('{0}','{1}','{2}',{3},{4},{5},{6},{7},{8});",
                    p.Trip, p.ActualDeparture.ToString("yyyy-MM-dd HH:mm:ss.fff"), p.ActualArrival.ToString("yyyy-MM-dd HH:mm:ss.fff"), p.StopTime, p.TravelTime, p.TotalTravelTime, p.HeadWay, p.BusStopFrom.ID, p.BusStopTo.ID)), Encoding.Default);

                BusStop.Append(items.SelectMany(p => new[] {p.BusStopFrom, p.BusStopTo}));
            }
        }


        public double[] ToAnnData()
        {
            return new List<Object>()
            {
                ID,
                Bucketize("Trip", Trip),
                BusStopFrom.ID,
                BusStopTo.ID,
                ActualDeparture.TimeOfDay.TotalMinutes,
                ActualArrival.TimeOfDay.TotalMinutes,
                StopTime,
                TravelTime,
                TotalTravelTime,
                HeadWay
            }.ToDoubles();
        }

        public string Header()
        {
            return "LttId" +
                   "Trip" +
                   "BusStopFromId" +
                   "BusStopToId" +
                   "ActualDepartureTimeOfDayTotalMinutes" +
                   "ActualArrival.TimeOfDay.TotalMinutes" +
                   "StopTime" +
                   "TravelTime" +
                   "TotalTravelTime" +
                   "HeadWay";
        }
    }
}