﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.Utilities;
using Utilities.Database;

namespace Utilities.AtB.PassengerCount
{
    public class PassengerCountItem: DbLoader
    {
        [Key]
        public int ID { get; set; }

        public DateTime DateAndTime { get; set; }
        public BusStop Stop { get; set; }
        public string DriverID { get; set; }
        public string DriverName { get; set; }
        public string Vehicle { get; set; }
        public string Line { get; set; }
        public string Trip { get; set; }
        public int PassengersPresent { get; set; }
        public int TotalUp { get; set; }
        public int TotalDown { get; set; }
        public int Door1Up { get; set; }
        public int Door1Down { get; set; }
        public int Door2Up { get; set; }
        public int Door2Down { get; set; }
        public int Door3Up { get; set; }
        public int Door3Down { get; set; }
        public int Door4Up { get; set; }
        public int Door4Down { get; set; }
        public bool HighCapacity { get; set; }

        public static IEnumerable<PassengerCountItem> FromFile(string file)
        {
            return System.IO.File.ReadAllLines(file, Encoding.Default).Skip(1).Where(p => !string.IsNullOrEmpty(p)).Select(line => line.Split(';')).Select(Load).Where(p => p.Stop != null);
        }

        private static PassengerCountItem Load(string[] p)
        {
            return new PassengerCountItem()
                {
                    DateAndTime = DateTime.ParseExact(p[0], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                    Stop = BusStop.FromCSVFormat(p[2]),
                    DriverID = p[3],
                    DriverName = p[4],
                    Vehicle = p[5],
                    Line = p[6],
                    Trip = p[7],
                    PassengersPresent = ParseInt(p[8]),
                    TotalUp = ParseInt(p[9]),
                    TotalDown = ParseInt(p[10]),
                    Door1Up = ParseInt(p[11]),
                    Door1Down = ParseInt(p[12]),
                    Door2Up = ParseInt(p[13]),
                    Door2Down = ParseInt(p[14]),
                    Door3Up = ParseInt(p[15]),
                    Door3Down = ParseInt(p[16]),
                    Door4Up = ParseInt(p[17]),
                    Door4Down = ParseInt(p[18]),
                    HighCapacity = p[19] != "No",
                };
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.PassengerCount);
            var files = System.IO.Directory.GetFiles(path, "*.csv").ToList();

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.PassengerCount, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = FromFile(file).ToList();

                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[PassengerCountItems]([DateAndTime],[DriverID],[DriverName],[Vehicle],[Line],[Trip],[PassengersPresent],[TotalUp],[TotalDown],[Door1Up],[Door1Down],[Door2Up],[Door2Down],[Door3Up],[Door3Down],[Door4Up],[Door4Down],[HighCapacity],[Stop_ID]) VALUES('{0}','{1}','{2}','{3}','{4}','{5}',{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},'{18}');",
                    p.DateAndTime.ToString("yyyy-MM-dd HH:mm:ss.fff"), p.DriverID, p.DriverName, p.Vehicle, p.Line, p.Trip, p.PassengersPresent, p.TotalUp, p.TotalDown, p.Door1Up, p.Door1Down, p.Door2Up, p.Door2Down, p.Door3Up, p.Door3Down, p.Door4Up, p.Door4Down, p.HighCapacity ? 1 : 0, p.Stop == null ? "" : p.Stop.ID)), Encoding.Default);

                BusStop.Append(items.Select(p => p.Stop));
            }
        }
    }
}
