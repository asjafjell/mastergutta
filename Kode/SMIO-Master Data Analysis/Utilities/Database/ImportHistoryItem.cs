﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Utilities.AtB.Utilities;

namespace Utilities.Database
{
    public class ImportHistoryItem
    {
        [Key]
        public int ID { get; set; }

        public DateTime ImportedAt { get; set; }
        public string File { get; set; }

        public ImportHistoryItem()
        {}

        public ImportHistoryItem(string file)
        {
            ImportedAt = DateTime.Now;
            File = file.Replace(Data.DataPath, "");
        }

        public static void AddSqlFolder(SMIODataContext db, Data.DataFolders folder)
        {
            System.IO.Directory.GetFiles(Data.GetSqlFolder(folder), "*.sql").ToList().ForEach(p => db.ImportHistory.Add(new ImportHistoryItem(p)));
        }

        public static bool FirstTime(SMIODataContext db, Data.DataFolders folder)
        {
            var path = Data.GetSqlFolder(folder).Replace(Data.DataPath, "");
            return !db.ImportHistory.Any(p => p.File.StartsWith(path));
        }

        public static bool IsImported(SMIODataContext db, string file)
        {
            var path = file.Replace(Data.DataPath, "");
            return db.ImportHistory.Any(p => p.File.StartsWith(path));
        }

        public static void Import(SMIODataContext db, string file)
        {
            var item = new ImportHistoryItem(file);
            if (db.ImportHistory.Any(p => p.File == item.File)) return;
            db.ImportHistory.Add(item);
        }

        public static void Clear(SMIODataContext db, Data.DataFolders type)
        {
            var folder = Data.GetSqlFolder(type).Replace(Data.DataPath, "");
            db.ImportHistory.RemoveRange(db.ImportHistory.Where(p => p.File.StartsWith(folder + "\\")));
            db.SaveChanges();
        }
    }
}
