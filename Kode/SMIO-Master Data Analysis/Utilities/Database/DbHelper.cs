﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Utilities.AtB.General;
using Utilities.AtB.Utilities;

namespace Utilities.Database
{
    public static class DbHelper
    {
        public static bool RunSQLFile(string file, bool output = false)
        {
            var cmd = new Process();
            cmd.StartInfo.FileName = Data.SqlCmd;
            cmd.StartInfo.Arguments = "-S.\\SQLExpress -i\"" + file + "\"";
            cmd.StartInfo.UseShellExecute = false;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.Start();

            var error = false;
            while (!cmd.StandardOutput.EndOfStream)
            {
                var line = cmd.StandardOutput.ReadLine();
                if (line == null || String.IsNullOrEmpty(line) || line.ToLower().Contains("1 rows affected")) continue;
                if (output)
                    Console.WriteLine(line);
                error = true;
            }

            return !error;
        }

        public static void CountDB(params Data.DataFolders[] tables)
        {
            var db = new SMIODataContext();
            var f = "{0}: {1:n0}";
            Console.WriteLine("Counting ...");
            if (tables.Length == 0)
                GetTableNames().ToList().ForEach(p => Console.WriteLine(f ,p, GetDBCount(GetDataTypeFromTableName(p), db)));
            else
                tables.ToList().ForEach(p => Console.WriteLine(f, GetTableName(p), GetDBCount(p, db)));
        }

        public static void ValidateSql(Data.DataFolders folder)
        {
            Console.WriteLine("Validating " + folder + " ...");
            var path = Data.GetSqlFolder(folder);
            var files = Directory.GetFiles(path, "*.sql").ToList();

            var db = new SMIODataContext();
            var count = 0;
            foreach (var file in files)
            {
                count += File.ReadAllLines(file, Encoding.Default).Count(p => p.StartsWith("INSERT"));
                Console.Write("\r  > Count: {0:n0}       ", count);
            }
            var dbCount = GetDBCount(folder, db);

            Console.Write("\r  > File count: {0:n0}, DB count: {1:n0}, valid: {2}" + Environment.NewLine, count, dbCount, count == dbCount);
        }

        public static int GetDBCount(Data.DataFolders folder, SMIODataContext db)
        {
            if (!Data.HasDBTable(folder)) return -1;

            switch (folder)
            {
                case Data.DataFolders.PassageAtBusStops:
                    return db.PassageItems.Count();
                case Data.DataFolders.PassageAtVirtualLoops:
                    return db.PassagesAtVirtualLoops.Count();
                case Data.DataFolders.LinkTravelTimes:
                    return db.LTTItems.Count();
                case Data.DataFolders.GPSLog:
                    return db.GPSLogItems.Count();
                case Data.DataFolders.Soccer:
                    return db.SoccerGames.Count();
                case Data.DataFolders.Fulltrips:
                    return db.FullTripItems.Count();
                case Data.DataFolders.Yr:
                    return db.WeatherObjects.Count();
                case Data.DataFolders.Buses:
                    return db.BusStops.Count();
                case Data.DataFolders.PassengerCount:
                    return db.PassengerCounts.Count();
                case Data.DataFolders.Lines:
                    return db.Lines.Count();
                case Data.DataFolders.RealTime:
                    return db.RealTimeItems.Count();
                case Data.DataFolders.Ticket:
                    return db.TicketData.Count();
                case Data.DataFolders.CellPhoneTicket:
                    return db.CellPhoneTicketData.Count();
                case Data.DataFolders.SingleTicket:
                    return db.SingleTickets.Count();
                case Data.DataFolders.Trips:
                    return db.Trips.Count();
                case Data.DataFolders.BusStopPassages:
                    return db.BusStopPassages.Count();
                default:
                    throw new ArgumentOutOfRangeException("folder (" + folder.ToString() + ")");
            }
        }

        public static void EmptyTable(SMIODataContext db, Data.DataFolders folder)
        {
            var name = GetTableName(folder);

            var objCtx = ((System.Data.Entity.Infrastructure.IObjectContextAdapter)db).ObjectContext;
            objCtx.ExecuteStoreCommand(string.Format("DELETE FROM [Utilities.Database.SMIODataContext].[dbo].[{0}]", name));
        }

        private static string GetTableName(Data.DataFolders type)
        {
            if (!Data.HasDBTable(type)) return "[NaT] " + type.ToString();

            switch (type)
            {
                case Data.DataFolders.PassageAtBusStops:
                    return "PassageItems";
                case Data.DataFolders.LinkTravelTimes:
                    return "LTTItems";
                case Data.DataFolders.GPSLog:
                    return "GPSLogItems";
                case Data.DataFolders.Soccer:
                    return "SoccerGames";
                case Data.DataFolders.Yr:
                    return "WeatherObjects";
                case Data.DataFolders.Fulltrips:
                    return "FullTripItems";
                case Data.DataFolders.Buses:
                    return "BusStops";
                case Data.DataFolders.PassageAtVirtualLoops:
                    return "PassageAtVirtualLoopsItems";
                case Data.DataFolders.PassengerCount:
                    return "PassengerCountItems";
                case Data.DataFolders.RealTime:
                    return "RealTimeItems";
                case Data.DataFolders.Ticket:
                    return "Tickets";
                case Data.DataFolders.CellPhoneTicket:
                    return "CellPhoneTickets";
                case Data.DataFolders.SingleTicket:
                    return "SingleTickets";
                case Data.DataFolders.Lines:
                    return "Lines";
                case Data.DataFolders.Trips:
                    return "TripItems";
                case Data.DataFolders.BusStopPassages:
                    return "BusStopPassages";
                default:
                    throw new ArgumentOutOfRangeException("folder");
            }
        }

        public static Data.DataFolders GetDataTypeFromTableName(string name)
        {
            return ((Data.DataFolders[])Enum.GetValues(typeof(Data.DataFolders))).Single(dt => GetTableName(dt).ToLower() == name.ToLower());
        }

        public static string[] GetTableNames()
        {
            return ((Data.DataFolders[])Enum.GetValues(typeof(Data.DataFolders))).Select(GetTableName).ToArray();
        }

        /// <summary>
        /// Runs SQL commands in given folder. Probably never going to be used again, kept for safety.
        /// </summary>
        /// <param name="folder"></param>
        private static void RunSQLCommands(Data.DataFolders folder)
        {
            Console.WriteLine("Running sql commands ...");

            var path = Path.Combine(Data.GetDataFolder(folder), "sql");
            var files = Directory.GetFiles(path, "*.sql").Where(p => Path.GetFileNameWithoutExtension(p).Contains("-") == false && Path.GetFileNameWithoutExtension(p).Contains("_") == false).OrderBy(p => Int32.Parse(Path.GetFileNameWithoutExtension(p)));

            var times = new List<double>();
            var count = 0;
            var lineCount = 0;

            foreach (var file in files)
            {
                if (times.Count == 0)
                    Console.Write("\r  > Running '{0}'.", Path.GetFileName(file));
                else
                    Console.Write("\r  > Running '{0}'. Average time: {1:n1} ms, files left: {2}, time remaining: {3:n1} minutes.                    ", Path.GetFileName(file), times.Average(), files.Count() - count - 2, (times.Average() * (files.Count() - count++ - 1)) / 60000f);
                var start = DateTime.Now;

                lineCount += File.ReadAllLines(file).Count(p => p.StartsWith("INSERT"));

                var cmd = new Process();
                cmd.StartInfo.FileName = Data.SqlCmd;
                cmd.StartInfo.Arguments = "-S.\\SQLExpress -i\"" + file + "\"";
                cmd.StartInfo.UseShellExecute = false;
                cmd.StartInfo.RedirectStandardOutput = true;
                cmd.StartInfo.CreateNoWindow = true;
                cmd.Start();

                var error = false;
                while (!cmd.StandardOutput.EndOfStream)
                {
                    var line = cmd.StandardOutput.ReadLine();
                    if (line == null || String.IsNullOrEmpty(line) || line.ToLower().Contains("1 rows affected")) continue;
                    Console.WriteLine(line);
                    error = true;
                }

                if (!error)
                    try
                    {
                        File.Delete(file);
                    }
                    catch (Exception ex)
                    {
                        Thread.Sleep(10000);
                        try
                        {
                            File.Delete(file);
                        }
                        catch (Exception ex2)
                        {
                            Thread.Sleep(10000);
                            try
                            {
                                File.Delete(file);
                            }
                            catch (Exception ex3)
                            {
                                //Console.WriteLine("  > Failed to delete " + System.IO.Path.GetFileName(file));
                            }
                        }
                    }

                var ms = DateTime.Now.Subtract(start).TotalMilliseconds;
                times.Add(ms);

                if (times.Count > 10) times = times.Skip(times.Count - 10).ToList();

                /*var dbCount = GetDBCount();
                if (lineCount != dbCount)
                {
                    Console.WriteLine("Line diff: " + lineCount + "/" + dbCount);
                    Console.ReadLine();
                }*/
            }

            Console.Write(Environment.NewLine + "  > Done." + Environment.NewLine);

        }

        public static List<string> SplitFiles(string path, string[] files, bool deleteExisting = true, bool insertTransactions = true, bool numberOnlyNames = true)
        {
            Directory.CreateDirectory(path);
            var res = new List<string>();
            
            if (deleteExisting)
                Directory.GetFiles(path, "*.*").ToList().ForEach(File.Delete);

            var size = Data.User == Data.Users.Simen ? 2000 : 10000;
            var fileNum = 0;
            foreach (var file in files)
            {
                Console.WriteLine("  > Splitting " + Path.GetFileName(file));
                var lines = File.ReadAllLines(file, Encoding.Default);
                Console.WriteLine("      - Read.");
                int parts = 1;
                while (lines.Any())
                {
                    var fsize = Math.Min(size, lines.Count());
                    var tosave = lines.Take(fsize).ToList();
                    if (insertTransactions)
                    {
                        tosave.Insert(0, "BEGIN TRAN T1;");
                        tosave.Insert(0, "SET xact_abort ON");
                        tosave.Add("COMMIT TRAN T1;");
                    }

                    var name = numberOnlyNames ? Path.Combine(path, fileNum++.ToString() + ".sql") : Path.Combine(path, Path.GetFileNameWithoutExtension(file) + "_" + parts + Path.GetExtension(file));
                    File.WriteAllLines(name, tosave, Encoding.Default);
                    res.Add(name);

                    Console.Write("\r      - Wrote {0} files.     ", parts++);
                    lines = lines.Skip(fsize).ToArray();
                }
                Console.Write(Environment.NewLine);
                Console.WriteLine("      - Done.");
            }

            return res;
        }

        public static void Reset(params Data.DataFolders[] types)
        {
            Console.WriteLine("Are you sure you want to reset the following tables?");
            Console.WriteLine("  > " + string.Join(", ", types.Select(p=>p.ToString())));
            var inp = Console.ReadLine();
            if (inp != "yes" && inp != "y")
            {
                Console.WriteLine("Aborted.");
                return;
            }

            var db = new SMIODataContext();
            foreach (var type in types)
            {
                ImportHistoryItem.Clear(db, type);
                if (type == Data.DataFolders.Buses)
                {
                    BusStop.ResetBusStops();
                }
                else
                {
                    EmptyTable(db, type);
                }
                Console.WriteLine("  > Reset " + type);
            }

            Console.WriteLine("Done.");
        }
    }
}
