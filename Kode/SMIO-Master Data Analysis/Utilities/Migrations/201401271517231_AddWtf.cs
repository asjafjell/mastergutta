namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWtf : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.FullTripItems", "Shift", c => c.String());
            AlterColumn("dbo.FullTripItems", "Line", c => c.String());
            AlterColumn("dbo.FullTripItems", "RouteID", c => c.String());
            AlterColumn("dbo.FullTripItems", "Vehicle", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.FullTripItems", "Vehicle", c => c.Int(nullable: false));
            AlterColumn("dbo.FullTripItems", "RouteID", c => c.Int(nullable: false));
            AlterColumn("dbo.FullTripItems", "Line", c => c.Int(nullable: false));
            AlterColumn("dbo.FullTripItems", "Shift", c => c.Int(nullable: false));
        }
    }
}
