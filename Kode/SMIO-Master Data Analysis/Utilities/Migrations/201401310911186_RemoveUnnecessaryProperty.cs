namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemoveUnnecessaryProperty : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.PassengerCountItems", "SkipThis");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PassengerCountItems", "SkipThis", c => c.String());
        }
    }
}
