namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBusStopPassages2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStopPassages", "BusStopID", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusStopPassages", "BusStopID");
        }
    }
}
