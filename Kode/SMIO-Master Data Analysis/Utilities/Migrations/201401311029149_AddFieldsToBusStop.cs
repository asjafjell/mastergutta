namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFieldsToBusStop : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStops", "RealTimeID", c => c.String());
            AddColumn("dbo.BusStops", "X", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.BusStops", "Y", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BusStops", "Y");
            DropColumn("dbo.BusStops", "X");
            DropColumn("dbo.BusStops", "RealTimeID");
        }
    }
}
