namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPassengerCount : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PassengerCountItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        DateAndTime = c.DateTime(nullable: false),
                        SkipThis = c.String(),
                        DriverID = c.String(),
                        DriverName = c.String(),
                        Vehicle = c.String(),
                        Line = c.String(),
                        Trip = c.String(),
                        PassengersPresent = c.Int(nullable: false),
                        TotalUp = c.Int(nullable: false),
                        TotalDown = c.Int(nullable: false),
                        Door1Up = c.Int(nullable: false),
                        Door1Down = c.Int(nullable: false),
                        Door2Up = c.Int(nullable: false),
                        Door2Down = c.Int(nullable: false),
                        Door3Up = c.Int(nullable: false),
                        Door3Down = c.Int(nullable: false),
                        Door4Up = c.Int(nullable: false),
                        Door4Down = c.Int(nullable: false),
                        HighCapacity = c.Int(nullable: false),
                        Stop_ID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BusStops", t => t.Stop_ID)
                .Index(t => t.Stop_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PassengerCountItems", "Stop_ID", "dbo.BusStops");
            DropIndex("dbo.PassengerCountItems", new[] { "Stop_ID" });
            DropTable("dbo.PassengerCountItems");
        }
    }
}
