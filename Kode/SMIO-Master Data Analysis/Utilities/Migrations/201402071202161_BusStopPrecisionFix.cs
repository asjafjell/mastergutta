namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BusStopPrecisionFix : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStops", "Y_", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.BusStops", "Y");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusStops", "Y", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.BusStops", "Y_");
        }
    }
}
