namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImportHistory : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImportHistoryItems",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        ImportedAt = c.DateTime(nullable: false),
                        File = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ImportHistoryItems");
        }
    }
}
