namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLinesFinal : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Lines",
                c => new
                    {
                        ID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.LineBusStops",
                c => new
                    {
                        Line_ID = c.String(nullable: false, maxLength: 128),
                        BusStop_ID = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.Line_ID, t.BusStop_ID })
                .ForeignKey("dbo.Lines", t => t.Line_ID, cascadeDelete: true)
                .ForeignKey("dbo.BusStops", t => t.BusStop_ID, cascadeDelete: true)
                .Index(t => t.Line_ID)
                .Index(t => t.BusStop_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LineBusStops", "BusStop_ID", "dbo.BusStops");
            DropForeignKey("dbo.LineBusStops", "Line_ID", "dbo.Lines");
            DropIndex("dbo.LineBusStops", new[] { "BusStop_ID" });
            DropIndex("dbo.LineBusStops", new[] { "Line_ID" });
            DropTable("dbo.LineBusStops");
            DropTable("dbo.Lines");
        }
    }
}
