namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSingleTickets : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SingleTickets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Company = c.Int(nullable: false),
                        ToZone = c.Int(nullable: false),
                        ZoneShortName = c.String(),
                        Product = c.Int(nullable: false),
                        CustomerProfile = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        FromStop_ID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BusStops", t => t.FromStop_ID)
                .Index(t => t.FromStop_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SingleTickets", "FromStop_ID", "dbo.BusStops");
            DropIndex("dbo.SingleTickets", new[] { "FromStop_ID" });
            DropTable("dbo.SingleTickets");
        }
    }
}
