namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeRTData2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.RealTimeItems", "StopID", c => c.String());
            AddColumn("dbo.RealTimeItems", "QueryTime", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.RealTimeItems", "QueryTime");
            DropColumn("dbo.RealTimeItems", "StopID");
        }
    }
}
