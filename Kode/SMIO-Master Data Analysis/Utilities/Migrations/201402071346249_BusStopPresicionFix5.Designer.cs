// <auto-generated />
namespace Utilities.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.0.2-21211")]
    public sealed partial class BusStopPresicionFix5 : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(BusStopPresicionFix5));
        
        string IMigrationMetadata.Id
        {
            get { return "201402071346249_BusStopPresicionFix5"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
