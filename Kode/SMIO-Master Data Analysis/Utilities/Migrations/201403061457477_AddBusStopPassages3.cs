namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBusStopPassages3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStopPassages", "BusStopCode", c => c.String());
            DropColumn("dbo.BusStopPassages", "TimeOfDayMinute");
            DropColumn("dbo.BusStopPassages", "BusStopID");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusStopPassages", "BusStopID", c => c.Int(nullable: false));
            AddColumn("dbo.BusStopPassages", "TimeOfDayMinute", c => c.Int(nullable: false));
            DropColumn("dbo.BusStopPassages", "BusStopCode");
        }
    }
}
