namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class VirtualLoopsPrecisionFix2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PassageAtVirtualLoopsItems", "Latitude", c => c.Decimal(nullable: false, precision: 18, scale: 8));
            AddColumn("dbo.PassageAtVirtualLoopsItems", "Longitude", c => c.Decimal(nullable: false, precision: 18, scale: 8));
            DropColumn("dbo.PassageAtVirtualLoopsItems", "Latitude_");
            DropColumn("dbo.PassageAtVirtualLoopsItems", "Longitude_");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PassageAtVirtualLoopsItems", "Longitude_", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.PassageAtVirtualLoopsItems", "Latitude_", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.PassageAtVirtualLoopsItems", "Longitude");
            DropColumn("dbo.PassageAtVirtualLoopsItems", "Latitude");
        }
    }
}
