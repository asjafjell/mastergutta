namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class BusStopPrecisionFix3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BusStops", "X_", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.BusStops", "X");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BusStops", "X", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.BusStops", "X_");
        }
    }
}
