namespace Utilities.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTickets : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Tickets",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        UsageType = c.Int(nullable: false),
                        Product = c.Int(nullable: false),
                        CustomerType = c.Int(nullable: false),
                        NumPassengers = c.Int(nullable: false),
                        EventDateTime = c.DateTime(nullable: false),
                        LineNr = c.Int(nullable: false),
                        TripNr = c.Int(nullable: false),
                        Vehicle = c.Int(nullable: false),
                        FromZone = c.Int(nullable: false),
                        Operator = c.Int(nullable: false),
                        ToZone = c.Int(nullable: false),
                        PaymentType = c.String(),
                        Overgang = c.Int(nullable: false),
                        IsInterchange = c.Boolean(nullable: false),
                        TransportType = c.Boolean(nullable: false),
                        CardNr = c.String(),
                        SequenceNr = c.Int(nullable: false),
                        CoreZone = c.String(),
                        SoneDistance = c.String(),
                        Distance = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                        GeoValidityType = c.Int(nullable: false),
                        EntryStop_ID = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.ID)
                .ForeignKey("dbo.BusStops", t => t.EntryStop_ID)
                .Index(t => t.EntryStop_ID);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Tickets", "EntryStop_ID", "dbo.BusStops");
            DropIndex("dbo.Tickets", new[] { "EntryStop_ID" });
            DropTable("dbo.Tickets");
        }
    }
}
