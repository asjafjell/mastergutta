﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Xml.Linq;
using AForge.Genetic;
using Utilities.AtB.Interfaces;
using Utilities.AtB.Utilities;
using Utilities.Database;
using Utilities.Extensions;

namespace Utilities.Yr
{
    public class WeatherObject : IAiInput
    {
        [Key]
        public int ID { get; set; } 
        public DateTime Time { get; set; }
        public string Description { get; set; }
        public decimal Temp { get; set; }
        public decimal TempMax { get; set; }
        public decimal TempMin { get; set; }
        public decimal Precipitation { get; set; }
        public decimal Wind { get; set; }
        public decimal WindExtreme { get; set; }
        public int Humidity { get; set; }

        public override string ToString()
        {
            return "'" + string.Join("', '", new object[] {Time.ToString(DateFormat), Description, Temp, TempMax, TempMin, Precipitation, Wind, WindExtreme, Humidity}) + "'";
        }

        private const string DateFormat = "yyyy-MM-dd HH:mm"; 

        public static string GetHeaders()
        {
            return "'" + string.Join("', '", new object[] {"Date", "Description", "Temp", "TempMax", "TempMin", "Precipitation", "Wind", "WindExtreme", "Humidity"}) + "'";
        }

        public static IEnumerable<WeatherObject> ReadFile(string file)
        {
            var numberFormat = new CultureInfo("fr-FR");
            return System.IO.File.ReadAllLines(file, Encoding.Default).Where(p => p.StartsWith("'2")).Select(line => line.Substring(1, line.Length - 2).Split("', '")).Select(parts => new WeatherObject()
                {
                    Time = DateTime.ParseExact(parts[0], DateFormat, CultureInfo.InvariantCulture),
                    Description = parts[1],
                    Temp = decimal.Parse(parts[2], numberFormat),
                    TempMax = decimal.Parse(parts[3], numberFormat),
                    TempMin = decimal.Parse(parts[4], numberFormat),
                    Precipitation = decimal.Parse(parts[5], numberFormat),
                    Wind = decimal.Parse(parts[6], numberFormat),
                    WindExtreme = decimal.Parse(parts[7], numberFormat),
                    Humidity = int.Parse(parts[8])
                });
        }

        public static void GenerateSqlFiles()
        {
            var path = Data.GetDataFolder(Data.DataFolders.Yr);
            var files = System.IO.Directory.GetFiles(path, "*.csv").ToList();

            Func<decimal, string> n = d => d.ToString().Replace(",", ".");

            foreach (var file in files)
            {
                var sql = Data.GetSqlFile(Data.DataFolders.Yr, System.IO.Path.GetFileNameWithoutExtension(file) + ".sql");
                if (System.IO.File.Exists(sql)) continue;
                Console.WriteLine("  > Generating SQL from " + System.IO.Path.GetFileNameWithoutExtension(file));
                var items = ReadFile(file).ToList();
                System.IO.File.WriteAllLines(sql, items.Select(p => string.Format(
                    "INSERT INTO [Utilities.Database.SMIODataContext].[dbo].[WeatherObjects]([Time],[Description],[Temp],[TempMax],[TempMin],[Precipitation],[Wind],[WindExtreme],[Humidity]) VALUES('{0}','{1}',{2},{3},{4},{5},{6},{7},{8});",
                    p.Time.ToString("yyyy-MM-dd HH:mm:ss.fff"), p.Description, n(p.Temp), n(p.TempMax), n(p.TempMin), n(p.Precipitation), n(p.Wind), n(p.WindExtreme), n(p.Humidity))), Encoding.Default);

                items.Clear();
            }
        }

        /// <summary>
        /// Detects values with -999 (which means that they were corrupted at Yr), and finds new values for those using interpolation. Does work directly on the database table.
        /// </summary>
        public static void InterpolateWeather()
        {
            var db = new SMIODataContext();
            var objects = db.WeatherObjects.OrderBy(p => p.Time).ToList();

            var lastTempMin = -1m;
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].TempMin == -999)
                {
                    var next = -999m;
                    var j = i;
                    while (next == -999m)
                        next = objects[++j].TempMin;
                    var fix = (next + lastTempMin) / 2m;
                    var id = objects[i].ID;
                    db.WeatherObjects.Single(p => p.ID == id).TempMin = fix;
                    Console.WriteLine("Set TempMin at {0}/{1} to {2}", i, id, objects[i].TempMin);
                }

                lastTempMin = objects[i].TempMin;
            }

            var lastTempMax = -1m;
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].TempMax == -999)
                {
                    var next = -999m;
                    var j = i;
                    while (next == -999m)
                        next = objects[++j].TempMax;
                    var fix = (next + lastTempMax) / 2m;
                    var id = objects[i].ID;
                    db.WeatherObjects.Single(p => p.ID == id).TempMax = fix;
                    Console.WriteLine("Set TempMax at {0}/{1} to {2}", i, id, objects[i].TempMax);
                }

                lastTempMax = objects[i].TempMax;
            }

            var lastPrecipitation = -1m;
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].Precipitation == -999)
                {
                    var next = -999m;
                    var j = i;
                    while (next == -999m)
                        next = objects[++j].Precipitation;
                    var fix = (next + lastPrecipitation) / 2m;
                    var id = objects[i].ID;
                    db.WeatherObjects.Single(p => p.ID == id).Precipitation = fix;
                    Console.WriteLine("Set Precipitation at {0}/{1} to {2}", i, id, objects[i].Precipitation);
                }

                lastPrecipitation = objects[i].Precipitation;
            }

            var lastWind = -1m;
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].Wind == -999)
                {
                    var next = -999m;
                    var j = i;
                    while (next == -999m)
                        next = objects[++j].Wind;
                    var fix = (next + lastWind) / 2m;
                    var id = objects[i].ID;
                    db.WeatherObjects.Single(p => p.ID == id).Wind = fix;
                    Console.WriteLine("Set Wind at {0}/{1} to {2}", i, id, objects[i].Wind);
                }

                lastWind = objects[i].Wind;
            }

            var lastWindExtreme = -1m;
            for (int i = 0; i < objects.Count; i++)
            {
                if (objects[i].WindExtreme == -999)
                {
                    var next = -999m;
                    var j = i;
                    while (next == -999m)
                        next = objects[++j].WindExtreme;
                    var fix = (next + lastWindExtreme) / 2m;
                    var id = objects[i].ID;
                    db.WeatherObjects.Single(p => p.ID == id).WindExtreme = fix;
                    Console.WriteLine("Set WindExtreme at {0}/{1} to {2}", i, id, objects[i].WindExtreme);
                }

                lastWindExtreme = objects[i].WindExtreme;
            }

            db.SaveChanges();

            Console.WriteLine("Fixed weather.");

            Console.ReadLine();
        }

        public static void FetchAndWriteToFile(DateTime start, DateTime end)
        {
            var file = Data.GetDataFile(Data.DataFolders.Yr, "yr_" + start.ToString("yyyy-MM-dd") + "_" + end.ToString("yyyy-MM-dd") + ".csv");

            var dt = DateTime.Now;
            var results = FetchWeatherData(start, end).Select(p => p.ToString()).ToList();
            results.Insert(0, WeatherObject.GetHeaders());

            System.IO.File.WriteAllLines(file, results, Encoding.Default);

            Console.WriteLine("Fetched weather data in " + DateTime.Now.Subtract(dt).TotalMilliseconds);
            Console.ReadLine();
        }

        private static IEnumerable<WeatherObject> FetchWeatherData(DateTime start, DateTime end)
        {
            var date = start.AddDays(-1);
            var results = new List<WeatherObject>();
            var dates = new List<DateTime>();
            while (date < end)
                dates.Add(date = date.AddDays(1));

            const int perThread = 50;

            ServicePointManager.DefaultConnectionLimit = perThread;

            var threads = new List<Thread>();
            for (var i = 0; i < dates.Count; i += perThread)
            {
                var myDates = dates.Skip(i).Take(perThread).ToArray();
                var thread = new Thread(() =>
                {
                    foreach (var day in myDates)
                    {
                        var myResults = FetchWeatherData(day).ToList();
                        lock (results)
                        {
                            results.AddRange(myResults);
                        }
                        Console.WriteLine("Fetched " + day.ToString("yyyy-MM-dd"));
                    }
                });
                threads.Add(thread);
                thread.Start();
            }

            while (threads.Any(p => p.IsAlive))
                Thread.Sleep(500);

            return results.OrderBy(p => p.Time);
        }

        private static IEnumerable<WeatherObject> FetchWeatherData(DateTime date)
        {
            const string url = "http://www.yr.no/sted/Norge/S%C3%B8r-Tr%C3%B8ndelag/Trondheim/Trondheim/almanakk.html?dato={0}";
            const string dateFormat = "yyyy-MM-dd";

            date = date.Date;
            var wc = new WebClient { Encoding = Encoding.UTF8 };
            var html = wc.DownloadString(string.Format(url, date.ToString(dateFormat)));

            if (!html.Contains("Observasjoner for Trondheim"))
                throw new Exception("Invalid weather data");

            var parts = html.Split("<table class=\"yr-table yr-table-hourly yr-popup-area\">")[1].Split("</tbody>")[0].Split("<tbody>")[1];
            var xml = XElement.Parse("<root>" + parts.Replace("<th ", "<td ").Replace("</th>", "</td>") + "</root>");

            CultureInfo numberFormat = new CultureInfo("fr-FR");
            var hours = xml.Elements("tr").
                            Select(p => p.Elements("td").ToArray()).
                            Select(p => new
                            {
                                Time = p[0].Value.Split("kl ")[1].Split("</strong>")[0],
                                Description = p[1].Value == "-" ? "-" : p[1].ToString().Split(" alt=\"")[1].Split('\"')[0].Trim(),
                                Temp = p[2].Value == "-" ? "-999,0" : p[2].Value.Replace("°", ""),
                                TempMax = p[3].Value == "-" ? "-999,0" : p[3].Value.Replace("°", ""),
                                TempMin = p[4].Value == "-" ? "-999,0" : p[4].Value.Replace("°", ""),
                                Precipitation = p[5].Value == "-" ? "-999,0" : p[5].Value.Replace(" mm", ""),
                                Wind = p[6].Value == "-" ? "-999,0" : p[6].Value.Split(", ")[1].Split(" ")[0],
                                WindExtreme = p[7].Value == "-" ? "-999,0" : p[7].Value.Replace(" m/s", ""),
                                Humidity = p[8].Value == "-" ? "-999,0" : p[8].Value.Replace(" %", "")

                            }).
                            Select(p => new WeatherObject()
                            {
                                Time = date.AddHours(int.Parse(p.Time)),
                                Description = p.Description,
                                Temp = decimal.Parse(p.Temp, numberFormat),
                                TempMax = decimal.Parse(p.TempMax, numberFormat),
                                TempMin = decimal.Parse(p.TempMin, numberFormat),
                                Precipitation = decimal.Parse(p.Precipitation, numberFormat),
                                Wind = decimal.Parse(p.Wind, numberFormat),
                                WindExtreme = decimal.Parse(p.WindExtreme, numberFormat),
                                Humidity = int.Parse(p.Humidity)
                            });

            return hours.ToList();
        }

        public double[] ToAnnData()
        {
            var doubles = new List<object>
            {
                ID,
                Time.TimeOfDay.Seconds,
                DbLoader.Bucketize("Description", Description),
                Temp,
                TempMax,
                TempMin,
                Precipitation,
                Wind,
                WindExtreme,
                Humidity
            }.ToDoubles();

            return doubles;
        }

        public string Header()
        {
            return "WeatherID," +
                   "Time," +
                   "Description," +
                   "Temp," +
                   "TempMax," +
                   "TempMin," +
                   "Precipitation," +
                   "Wind," +
                   "WindExtreme," +
                   "Humidity";
        }
    }
}